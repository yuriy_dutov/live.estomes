﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="batch_upload.aspx.vb" Inherits="batch_upload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>

</head>
<body>
    <form id="form1" runat="server">
        <% Session.Timeout = 900%>
        <div class="style1">
            <center style="text-align: center">
                <b>Agent Manager - Manage Batches</b><br />
                <br />
                <asp:HiddenField ID="hdnUploadedFile" runat="server" />
                <asp:HiddenField ID="txtFirstName" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="txtLastName" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="txtEmail" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="txtPrimaryPhone" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="txtCitizen" runat="server" Value="Y"></asp:HiddenField>
                <asp:HiddenField ID="txtInterest" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="txtGraduated" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="txtSourceID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="txtAddress" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="txtCity" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="txtState" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="txtZip" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="txtOptInDate" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="txtOptInTime" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="txtIP" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="txtURL" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="txtLeadID" runat="server"></asp:HiddenField>

                <asp:Wizard ID="Wizard1" runat="server" ActiveStepIndex="0" BackColor="#EFF3FB" BorderColor="#B5C7DE"
                    BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em">
                    <NavigationStyle VerticalAlign="Top" HorizontalAlign="Center" Width="500px" Height="300px" />
                    <StepStyle VerticalAlign="Top" CssClass="steppadding" Font-Size="0.8em" ForeColor="#333333" />
                    <NavigationStyle VerticalAlign="Bottom" HorizontalAlign="Right" />
                    <HeaderTemplate>
                        Step
            <%=Wizard1.ActiveStepIndex + 1%>
            of
            <%=Wizard1.WizardSteps.Count%>
                    </HeaderTemplate>
                    <HeaderStyle BackColor="AliceBlue" ForeColor="White" />
                    <WizardSteps>
                        <asp:WizardStep ID="WizardStep1" runat="server" Title="Get CSV File">
                            <br />
                            Load CSV File:<br />
                            <br />
                            <asp:FileUpload ID="fupExcelFile" runat="server" />
                        </asp:WizardStep>
                        <%-- <asp:WizardStep ID="WizardStep2" runat="server" Title="Choose Excel Sheet">
                            <br />
                            Choose Sheet to Load:<br />
                            <br />
                            <asp:DropDownList ID="ddlExcelSheets" runat="server">
                            </asp:DropDownList>
                        </asp:WizardStep>--%>
                        <asp:WizardStep ID="WizardStep4" runat="server" Title="Map Columns">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" />


                            <table>
                                <tr>
                                    <td>Select Destination to Upload:</td>
                                    <td>
                                        <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="dsPostName" AppendDataBoundItems="true" DataTextField="post_name" DataValueField="post_name">
                                            <asp:ListItem Text="(Select)" Value=""></asp:ListItem>
                                            <asp:ListItem Text="post_database" Value="post_database"></asp:ListItem>
                                           
                                        </asp:DropDownList>

                                        <asp:SqlDataSource ID="dsPostName" runat="server" ConnectionString="<%$ ConnectionStrings:SchoolConnectionString3 %>" SelectCommand="SELECT [post_name] FROM [post_info] ORDER BY [post_name]"></asp:SqlDataSource>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Sub ID:</td>
                                    <td>
                                        <asp:TextBox ID="txtSubID" runat="server"></asp:TextBox></td>
                                </tr>
                            </table>
                            <br />
                            <br />
                            Map Columns Between Spreadsheet and Table:<br />


                            <asp:GridView ID="gvColumns" runat="server" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:BoundField DataField="ColumnName" HeaderText="Excel Sheet Column" />
                                    <asp:BoundField DataField="DataType" HeaderText="Data Type" />
                                    <asp:TemplateField HeaderText="Mapped Column">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlDestinationColumn" runat="server">
                                                <asp:ListItem Text="Not Mapped" Value=""></asp:ListItem>
                                                <asp:ListItem>FirstName</asp:ListItem>
                                                <asp:ListItem>LastName</asp:ListItem>
                                                <asp:ListItem>Email</asp:ListItem>
                                                <asp:ListItem>Address</asp:ListItem>
                                                <asp:ListItem>City</asp:ListItem>
                                                <asp:ListItem>State</asp:ListItem>
                                                <asp:ListItem>Zip</asp:ListItem>
                                                <asp:ListItem>PrimaryPhone</asp:ListItem>
                                                <asp:ListItem>OptInDate</asp:ListItem>
                                                <asp:ListItem>OptInTime</asp:ListItem>
                                                <asp:ListItem>URL</asp:ListItem>
                                                <asp:ListItem>IP</asp:ListItem>
                                                <asp:ListItem>Interest</asp:ListItem>
                                                <asp:ListItem>Graduated</asp:ListItem>
                                                <asp:ListItem>SourceID</asp:ListItem>
                                                <asp:ListItem>LeadID</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                            <br />



                            <br />
                        </asp:WizardStep>
                        <asp:WizardStep ID="WizardStep5" runat="server" Title="Upload Data">
                            <br />
                            Final Check:<br />
                            <asp:GridView ID="gvColumnMap" runat="server">
                            </asp:GridView>
                            <br />
                            <asp:Label ID="lblFinalQuery" runat="server" Text="Label"></asp:Label>
                            <asp:Button ID="btnUpload" runat="server" Text="Upload" />
                            <br />
                            <asp:Label ID="lblAlreadyAdded" runat="server" ForeColor="Red"></asp:Label>


                        </asp:WizardStep>
                    </WizardSteps>
                    <SideBarButtonStyle BackColor="#507CD1" Font-Names="Verdana" ForeColor="White" />
                    <NavigationButtonStyle BackColor="White" BorderColor="#507CD1" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284E98" />
                    <SideBarStyle BackColor="#507CD1" Font-Size="0.9em" VerticalAlign="Top" CssClass="steppadding" />
                    <HeaderStyle BackColor="#284E98" BorderColor="#EFF3FB" BorderStyle="Solid" BorderWidth="2px"
                        Font-Bold="True" Font-Size="0.9em" ForeColor="White" HorizontalAlign="Center" />
                </asp:Wizard>

            </center>
        </div>
    </form>
</body>
</html>
