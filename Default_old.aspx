﻿<%@ Page Language="VB" AutoEventWireup="false" Debug="true" CodeFile="Default_old.aspx.vb" ValidateRequest="false"
    Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    First Name
                </td>
                <td>
                    <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Last Name
                </td>
                <td>
                    <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Email
                </td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Primary Phone
                </td>
                <td>
                    <asp:TextBox ID="txtPrimaryPhone" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Citizen
                </td>
                <td>
                    <asp:TextBox ID="txtCitizen" runat="server" Text="Y"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Field of Interest
                </td>
                <td>
                    <asp:TextBox ID="txtInterest" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Had HS or GED
                </td>
                <td>
                    <asp:TextBox ID="txtGraduated" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Source ID
                </td>
                <td>
                    <asp:TextBox ID="txtSourceID" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Address
                </td>
                <td>
                    <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    City
                </td>
                <td>
                    <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    State
                </td>
                <td>
                    <asp:TextBox ID="txtState" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Zip
                </td>
                <td>
                    <asp:TextBox ID="txtZip" runat="server"></asp:TextBox>
                </td>
            </tr>

 <tr>
                <td>
                    Opt In Date
                </td>
                <td>
                    <asp:TextBox ID="txtOptInDate" runat="server"></asp:TextBox>
                </td>
            </tr>

 <tr>
                <td>
                    Opt In Time
                </td>
                <td>
                    <asp:TextBox ID="txtOptInTime" runat="server"></asp:TextBox>
                </td>
            </tr>

 <tr>
                <td>
                    IP address
                </td>
                <td>
                    <asp:TextBox ID="txtIP" runat="server"></asp:TextBox>
                </td>
            </tr>






            <tr>
                <td>
                    URL</td>
                <td>
                    <asp:TextBox ID="txtURL" runat="server"></asp:TextBox>
                </td>
            </tr>






            <tr>
                <td>
                    Lead ID</td>
                <td>
                    <asp:TextBox ID="txtLeadID" runat="server"></asp:TextBox>
                </td>
            </tr>






            <tr>
                <td>
                    Test
                </td>
                <td>
                    <asp:DropDownList ID="ddlTest" runat="server">
                        <asp:ListItem Selected="True">Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnPost" runat="server" Text="Post" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
