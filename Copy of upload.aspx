﻿<%@ Page Language="VB" AutoEventWireup="false" Debug="true" CodeFile="Copy of upload.aspx.vb"
    Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    Select destination:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server">
                        <asp:ListItem Text="(Select)" Value=""></asp:ListItem>
                        <asp:ListItem>Post_leadresell</asp:ListItem>
                        <asp:ListItem>Post_leadresell2</asp:ListItem>
                        <asp:ListItem>Post_leadresell3</asp:ListItem><asp:ListItem>Post_leadresell4</asp:ListItem>
                        <asp:ListItem>Post_LR_Data</asp:ListItem>
                        <asp:ListItem>Post_ed_soup</asp:ListItem>
                        <asp:ListItem>Post_adspire</asp:ListItem>
                        <asp:ListItem>post_axiom</asp:ListItem>
                        <asp:ListItem>post_1NW</asp:ListItem>
                        <asp:ListItem>post_market</asp:ListItem>
                        <asp:ListItem>post_Accelerex</asp:ListItem>
                        <asp:ListItem>post_edsoup_mob</asp:ListItem>
                        <asp:ListItem>post_Duplicate</asp:ListItem>
                        <asp:ListItem>post_PerfectPitch</asp:ListItem>
                        <asp:ListItem>post_OBWT</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Sub ID:
                </td>
                <td>
                    <asp:TextBox ID="txtSubID" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    CSV File:
                </td>
                <td>
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Button ID="btnPost" runat="server" Text="Post" />
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="txtFirstName" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="txtLastName" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="txtEmail" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="txtPrimaryPhone" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="txtCitizen" runat="server" Value="Y"></asp:HiddenField>
        <asp:HiddenField ID="txtInterest" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="txtGraduated" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="txtSourceID" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="txtAddress" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="txtCity" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="txtState" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="txtZip" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="txtOptInDate" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="txtOptInTime" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="txtIP" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="txtURL" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="txtLeadID" runat="server"></asp:HiddenField>
    </div>
    </form>
</body>
</html>
