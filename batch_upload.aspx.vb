﻿Option Compare Text
Imports System.Data.OleDb
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports Common

Partial Class batch_upload
    Inherits System.Web.UI.Page
    Dim dt As DataTable


    Protected Sub Wizard1_NextButtonClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.WizardNavigationEventArgs) Handles Wizard1.NextButtonClick
        If Wizard1.ActiveStepIndex = 0 Then
            Try
                If IO.File.Exists(Server.MapPath("docs/" & fupExcelFile.FileName)) Then
                    IO.File.Delete(Server.MapPath("docs/" & fupExcelFile.FileName))
                End If
                fupExcelFile.SaveAs(Server.MapPath("docs/" & fupExcelFile.FileName))

            Catch ex As Exception
                Response.Write(ex.Message)
                Response.End()
            End Try
            hdnUploadedFile.Value = fupExcelFile.FileName

            GetExcelSheetNames(fupExcelFile.FileName)


        End If


    End Sub

    Protected Sub GetExcelSheetNames(ByVal excelFile As String)

        Dim dt As New DataTable

        Dim filetype As String = excelFile.Substring(excelFile.LastIndexOf(".") + 1).ToLower

        Select Case filetype
            Case "xls"
                Dim cn As New OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Server.MapPath("docs/" & excelFile) & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1"";")
                cn.Open()
                Dim reply As New OleDb.OleDbDataAdapter("select * from [sheet1$]", cn)
                reply.Fill(dt)
                cn.Close()
            Case "xslx"
                Dim cn As New OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Server.MapPath("docs/" & excelFile) & ";Extended Properties=""Excel 12.0 Xml;HDR=YES"";")
                cn.Open()
                Dim reply As New OleDb.OleDbDataAdapter("select * from [sheet1$]", cn)
                reply.Fill(dt)
                cn.Close()
            Case "csv", "txt"

                'Response.Write("select * from [" & excelFile & "]")
                'Response.End()

                Dim cn As New OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Server.MapPath("docs/") & ";Extended Properties=""text;HDR=Yes;FMT=Delimited"";")
                cn.Open()
                Dim reply As New OleDb.OleDbDataAdapter("select * from [" & excelFile & "]", cn)
                reply.Fill(dt)
                cn.Close()

        End Select


        Dim dt_col As New DataTable
        dt_col.Columns.Add("ColumnName")
        dt_col.Columns.Add("DataType")



        For Each dc As DataColumn In dt.Columns
            Dim dr As DataRow = dt_col.NewRow
            dr.Item("ColumnName") = dc.ColumnName
            dr.Item("DataType") = dc.DataType
            dt_col.Rows.Add(dr)
        Next

        gvColumns.DataSource = dt_col 'dt_speeds
        gvColumns.DataBind()


    End Sub


    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click




        Using filestr As New FileStream(Server.MapPath("docs/") + "\schema.ini", FileMode.Create, FileAccess.Write)
            Using writer As New StreamWriter(filestr)
                writer.WriteLine("[" + hdnUploadedFile.Value + "]")
                writer.WriteLine("ColNameHeader=True")
                writer.WriteLine("Format=CSVDelimited")
                writer.WriteLine("DateTimeFormat=dd-MMM-yy")
                writer.WriteLine("Col1=A Text Width 500")
                writer.WriteLine("Col2=B Text Width 500")
                writer.WriteLine("Col3=C Text Width 500")
                writer.WriteLine("Col4=D Text Width 500")
                writer.WriteLine("Col5=E Text Width 500")
                writer.WriteLine("Col6=F Text Width 500")
                writer.WriteLine("Col7=G Text Width 500")
                writer.WriteLine("Col8=H Text Width 500")
                writer.WriteLine("Col9=I Text Width 500")
                writer.WriteLine("Col10=J Text Width 500")
                writer.WriteLine("Col11=K Text Width 500")
                writer.WriteLine("Col12=L Text Width 500")
                writer.Close()
                writer.Dispose()
            End Using
            filestr.Close()
            filestr.Dispose()
        End Using




        Dim excel_dt As New DataTable

        Dim cn As New OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Server.MapPath("docs/") & ";Extended Properties=""text;HDR=Yes;FMT=Delimited;IMEX=1"";")
        cn.Open()
        Dim reply As New OleDb.OleDbDataAdapter("select * from [" & hdnUploadedFile.Value & "]", cn)
        reply.Fill(excel_dt)
        cn.Close()

        Dim num_loaded As Integer = 0
        Dim num_good As Integer = 0
        Dim num_bad As Integer = 0



        Dim already_added As String = ""
        txtSourceID.Value = txtSubID.Text

        For Each dr As DataRow In excel_dt.Rows

            Dim col_count As Integer = 0
            For Each dr2 As GridViewRow In gvColumns.Rows
                Dim ddl As DropDownList = dr2.Cells(2).Controls(1)

                If ddl.SelectedValue <> "Not Mapped" Then

                    Dim temp_val As String = dr.Item(col_count).ToString.Replace("'", "''")
                    Select Case ddl.SelectedValue
                        Case "FirstName"
                            txtFirstName.Value = temp_val
                        Case "Address"
                            txtAddress.Value = temp_val
                        Case "Citizen"
                            txtCitizen.Value = temp_val
                        Case "City"
                            txtCity.Value = temp_val
                        Case "Email"
                            txtEmail.Value = temp_val
                        Case "Graduated"
                            txtGraduated.Value = temp_val
                        Case "Interest"
                            txtInterest.Value = temp_val
                        Case "IP"
                            txtIP.Value = temp_val
                        Case "LastName"
                            txtLastName.Value = temp_val
                        Case "LeadID"
                            txtLeadID.Value = temp_val
                        Case "OptInDate"
                            txtOptInDate.Value = temp_val
                        Case "OptInTime"
                            txtOptInTime.Value = temp_val
                        Case "PrimaryPhone"
                            txtPrimaryPhone.Value = temp_val
                        Case "SourceID"
                            txtSourceID.Value = temp_val
                        Case "State"
                            txtState.Value = temp_val
                        Case "URL"
                            txtURL.Value = temp_val
                        Case "Zip"
                            txtZip.Value = temp_val

                    End Select

                End If

                col_count += 1

            Next



            Try
                If try_submit(DropDownList1.SelectedValue, False, False) Then
                    num_good += 1
                Else
                    num_bad += 1
                End If

                ' Post_Email(False, True)

            Catch ex As Exception
                Response.Write("Failed: " & txtFirstName.Value & " " & txtLastName.Value & " " & ex.Message & "<br>")
            End Try

            num_loaded += 1



        Next
        lblAlreadyAdded.Text = "(Update Complete) - " & num_loaded & " uploaded - Success: " & num_good & "/Fail: " & num_bad
        cn.Close()
        cn.Dispose()


    End Sub




    Dim isDupe As Boolean = False


    Protected Function try_submit(routine As String, Optional isDupe As Boolean = False, Optional first_pass As Boolean = False) As Boolean


        'If routine = "" Then
        '    Return False
        'End If

        'new try_submit
        If Not isDupe Then
            Dim post_dt As DataTable = GetSchoolTable("select * from post_info where post_name = '" & routine & "'")
            If post_dt.Rows.Count > 0 Then



                Dim this_uri As Uri
                Dim data As String = ""


                this_uri = New Uri(post_dt.Rows(0).Item("post_URL").ToString)
                If post_dt.Rows(0).Item("fixed_string").ToString <> "" Then
                    data &= post_dt.Rows(0).Item("fixed_string").ToString
                End If
                If post_dt.Rows(0).Item("FirstName").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("FirstName").ToString & "=" & txtFirstName.Value
                End If
                If post_dt.Rows(0).Item("LastName").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("LastName").ToString & "=" & txtLastName.Value
                End If
                If post_dt.Rows(0).Item("Email").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Email").ToString & "=" & txtEmail.Value
                End If
                If post_dt.Rows(0).Item("Phone").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Phone").ToString & "=" & txtPrimaryPhone.Value
                End If
                If post_dt.Rows(0).Item("Zip").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Zip").ToString & "=" & txtZip.Value
                End If
                If post_dt.Rows(0).Item("Address").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Address").ToString & "=" & txtAddress.Value
                End If
                If post_dt.Rows(0).Item("City").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("City").ToString & "=" & txtCity.Value
                End If
                If post_dt.Rows(0).Item("State").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("State").ToString & "=" & txtState.Value
                End If
                If post_dt.Rows(0).Item("IP").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("IP").ToString & "=" & txtIP.Value
                End If
                If post_dt.Rows(0).Item("Citizen").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Citizen").ToString & "=" & txtCitizen.Value
                End If
                If post_dt.Rows(0).Item("Graduated").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Graduated").ToString & "=" & txtGraduated.Value
                End If
                If post_dt.Rows(0).Item("Interest").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Interest").ToString & "=" & txtInterest.Value
                End If
                If post_dt.Rows(0).Item("LeadID").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("LeadID").ToString & "=" & txtLeadID.Value
                End If
                If post_dt.Rows(0).Item("OptinDate").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("OptinDate").ToString & "=" & txtOptInDate.Value
                End If
                If post_dt.Rows(0).Item("OptInTime").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("OptInTime").ToString & "=" & txtOptInTime.Value
                End If
                If post_dt.Rows(0).Item("SourceID").ToString <> "" Then

                    Dim sources() As String = post_dt.Rows(0).Item("SourceID").ToString.Split("|")
                    For Each source In sources
                        data &= "&" & source & "=" & txtSourceID.Value
                    Next

                    'data &= "&" & post_dt.Rows(0).Item("SourceID") & "=" & txtSourceID.value
                End If
                If post_dt.Rows(0).Item("URL").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("URL").ToString & "=" & txtURL.Value
                End If
                If post_dt.Rows(0).Item("Today").ToString <> "" Then

                    Dim sources() As String = post_dt.Rows(0).Item("Today").ToString.Split("|")
                    For Each source In sources
                        data &= "&" & source & "=" & Today.ToShortDateString
                    Next


                    'data &= "&" & post_dt.Rows(0).Item("Today") & "=" & Today.ToShortDateString
                End If



                Dim web_request As HttpWebRequest

                If post_dt.Rows(0).Item("method").ToString = "post" Then

                    web_request = HttpWebRequest.Create(this_uri)
                    web_request.Method = WebRequestMethods.Http.Post
                    web_request.ContentLength = System.Text.Encoding.UTF8.GetByteCount(data)
                    web_request.ContentType = "application/x-www-form-urlencoded"
                    Dim writer As New StreamWriter(web_request.GetRequestStream)
                    writer.Write(data, 0, System.Text.Encoding.UTF8.GetByteCount(data))
                    writer.Close()
                Else
                    this_uri = New Uri(post_dt.Rows(0).Item("post_URL") & "?" & data)
                    web_request = HttpWebRequest.Create(this_uri)
                    web_request.Method = WebRequestMethods.Http.Get

                End If



                'web_request.ContentLength = data.Length



                Dim web_response As HttpWebResponse = web_request.GetResponse()
                Dim reader As New StreamReader(web_response.GetResponseStream())
                Dim tmp As String = reader.ReadToEnd()
                web_response.Close()



                Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
                cn.Open()
                Dim reply As New SqlCommand("AddMFP", cn)
                reply.CommandType = System.Data.CommandType.StoredProcedure
                reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
                reply.Parameters.AddWithValue("last_name", txtLastName.Value)
                reply.Parameters.AddWithValue("email", txtEmail.Value)
                reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
                reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
                reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
                reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

                reply.Parameters.AddWithValue("address", txtAddress.Value)
                reply.Parameters.AddWithValue("city", txtCity.Value)
                reply.Parameters.AddWithValue("state", txtState.Value)
                reply.Parameters.AddWithValue("zip", txtZip.Value)

                reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
                reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
                reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

                reply.Parameters.AddWithValue("show_result", True)
                If tmp.IndexOf(post_dt.Rows(0).Item("rejected").ToString) > -1 Then 'Fail
                    reply.Parameters.AddWithValue("post_result", "FAIL")
                Else
                    reply.Parameters.AddWithValue("post_result", "SUCCESS")
                End If

                reply.Parameters.AddWithValue("lead_id", txtLeadID.Value)

                reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
                reply.Parameters.AddWithValue("submitted_data", data)
                reply.Parameters.AddWithValue("submitted_site", post_dt.Rows(0).Item("post_URL").ToString)
                reply.Parameters.AddWithValue("result", tmp)
                reply.Parameters.AddWithValue("isDupe", isDupe)
                reply.Parameters.AddWithValue("first_pass", first_pass)
                reply.Parameters.AddWithValue("post_function", post_dt.Rows(0).Item("post_name").ToString)
                reply.Parameters.AddWithValue("url", txtURL.Value)
                If Request("vendor_id") IsNot Nothing Then
                    reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
                Else
                    reply.Parameters.AddWithValue("vendor_id", "0")
                End If


                reply.Parameters.AddWithValue("query_string", Request.Url.Query)

                'If routine = "Post_leadsresell_YCA" Then
                '    'email_error(cn.State.ToString)
                'End If

                reply.ExecuteNonQuery()
                cn.Close()

                If tmp.IndexOf(post_dt.Rows(0).Item("rejected").ToString) > -1 Then 'Fail
                    Return False
                Else
                    Return True
                End If
            Else
                Select Case routine
                    Case "post_Duplicate"
                        If Post_Duplicate(True, first_pass) Then Return True
                    Case "Post_database"
                        If Post_database(True, first_pass) Then Return True
                    Case "Post_Email"
                        If Post_Email(True, first_pass) Then Return True
                End Select
                Return False
            End If
        Else
            Select Case routine

                Case "post_Duplicate"
                    If Post_Duplicate(True, first_pass) Then Return True

            End Select
        End If


        Return False
    End Function


    'Protected Function try_submit2(routine As String, Optional isDupe As Boolean = False, Optional first_pass As Boolean = False) As Boolean
    '    Select Case routine
    '        Case "Post_leadresell"
    '            If Post_leadresell(True, first_pass) Then Return True
    '        Case "Post_leadresell2"
    '            If Post_leadresell2(True, first_pass) Then Return True
    '        Case "Post_leadresell3"
    '            If Post_leadresell3(True, first_pass) Then Return True
    '        Case "Post_leadresell3"
    '            If Post_leadresell3(True, first_pass) Then Return True
    '        Case "Post_leadreselljg"
    '            If Post_leadreselljg(True, first_pass) Then Return True
    '        Case "Post_leadresell4"
    '            If Post_leadresell4(True, first_pass) Then Return True
    '        Case "Post_leadresell5"
    '            If Post_leadresell5(True, first_pass) Then Return True

    '        Case "post_leadresell_SPHN"
    '            If Post_leadresell_SPHN(True, first_pass) Then Return True
    '        Case "Post_leadresell_FSA"
    '            If Post_leadresell_FSA(True, first_pass) Then Return True
    '        Case "Post_leadresell_MAU"
    '            If Post_leadresell_MAU(True, first_pass) Then Return True
    '        Case "Post_leadresell_CWU"
    '            If Post_leadresell_CWU(True, first_pass) Then Return True

    '        Case "Post_leadsresell_YCAR"
    '            If Post_leadsresell_YCAR(True, first_pass) Then Return True
    '        Case "Post_leadsresell_LJSR"
    '            If Post_leadsresell_LJSR(True, first_pass) Then Return True
    '        Case "Post_leadsresell_EDS2"
    '            If Post_leadsresell_EDS2(True, first_pass) Then Return True






    '        Case "post_leadresell_PR"
    '            If post_leadresell_PR(True, first_pass) Then Return True
    '        Case "post_leadresell_UA"
    '            If post_leadresell_UA(True, first_pass) Then Return True
    '        Case "post_NW"
    '            If post_NW(True, first_pass) Then Return True
    '        Case "Post_LR_Data"
    '            If Post_LR_Data(True, first_pass) Then Return True
    '        Case "Post_ed_soup"
    '            If post_ed_soup(True, first_pass) Then Return True
    '        Case "post_PerfectPitch"
    '            If post_PerfectPitch(True, first_pass) Then Return True
    '        Case "Post_adspire"
    '            If post_adspire(True, first_pass) Then Return True
    '        Case "post_axiom"
    '            If post_axiom(True, first_pass) Then Return True
    '        Case "post_1NW"
    '            If post_1NW(True, first_pass) Then Return True
    '        Case "post_market"
    '            If post_market(True, first_pass) Then Return True
    '        Case "post_Accelerex"
    '            If post_Accelerex(True, first_pass) Then Return True

    '        Case "post_Duplicate"
    '            If Post_Duplicate(True, first_pass) Then Return True

    '        Case "post_edsoup_mob"
    '            If post_edsoup_mob(True, first_pass) Then Return True
    '        Case "post_OBWT"
    '            If post_OBWT(True, first_pass) Then Return True
    '        Case "post_h2h"
    '            If post_h2h(True, first_pass) Then Return True
    '        Case "post_SMS_A"
    '            If post_SMS_A(True, first_pass) Then Return True
    '        Case "post_SMS_B"
    '            If post_SMS_B(True, first_pass) Then Return True

    '        Case "post_SMS_MIB"
    '            If post_SMS_MIB(True, first_pass) Then Return True
    '        Case "post_SMS_HIM"
    '            If post_SMS_HIM(True, first_pass) Then Return True
    '        Case "post_SMS_FHN"
    '            If post_SMS_FHN(True, first_pass) Then Return True
    '        Case "post_SMS_UFR"
    '            If post_SMS_UFR(True, first_pass) Then Return True

    '        Case "Post_ES"
    '            If Post_ES(True, first_pass) Then Return True
    '        Case "Post_leadresellHFAF"
    '            If Post_leadresellHFAF(True, first_pass) Then Return True

    '        Case "Post_leadresell_YCA"
    '            If Post_leadresell_YCA(True, first_pass) Then Return True

    '        Case "Post_OAM"
    '            If Post_OAM(True, first_pass) Then Return True
    '        Case "Post_OAM2"
    '            If Post_OAM2(True, first_pass) Then Return True

    '        Case "Post_database"
    '            If Post_database(True, first_pass) Then Return True
    '        Case "Post_Email"
    '            If Post_Email(True, first_pass) Then Return True

    '        Case "post_USN_FFR"
    '            If post_USN_FFR(True, first_pass) Then Return True

    '        Case "post_USN_FHN"
    '            If post_USN_FHN(True, first_pass) Then Return True
    '    End Select

    '    Return False
    'End Function

    Protected Sub Post_Data()
        Exit Sub

        Dim centerID As String = ""
        If Request("centerID") IsNot Nothing Then
            If Request("centerID") = "2" Then
                'Repost_Data()
                'Exit Sub
            End If
        End If

        Dim this_uri As Uri

        this_uri = New Uri("http://ps.myfootpath.com/v2/transmission.php")

        Dim data As String
        If Request("vendor_id") IsNot Nothing Then
            Select Case Request("vendor_id")
                Case "1" 'Estomes
                    data = "afid=167&afkey=3136bc30e8b4be820e16abace7c28f1e&campaign_id=169&request_type_id=16"
                Case "2" 'Tymax 
                    data = "afid=115&afkey=7ac92e12a4330e9f62ab7b0694005161&campaign_id=124&request_type_id=16"
                Case Else
                    data = "afid=167&afkey=3136bc30e8b4be820e16abace7c28f1e&campaign_id=169&request_type_id=16"
            End Select
        Else
            data = "afid=167&afkey=3136bc30e8b4be820e16abace7c28f1e&campaign_id=169&request_type_id=16"
        End If

        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&primary_phone=" & txtPrimaryPhone.Value
        data &= "&citizen=" & txtCitizen.Value
        data &= "&field_of_interest=" & txtInterest.Value
        data &= "&has_hs_or_ged=" & txtGraduated.Value
        data &= "&source_id=" & txtSourceID.Value

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        ' Response.Write(tmp)



        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)


        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp <> "success" Then
            ' Repost_Data()
        End If

        Response.End()
    End Sub

    Protected Function Post_Duplicate(show_result As Boolean, first_pass As Boolean) As Boolean

        Dim this_uri As String = "Duplicate"

        Dim data As String = ""

        Dim tmp As String = "Duplicate"


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp = "SUCCESS" Then
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        Else
            reply.Parameters.AddWithValue("post_result", "FAIL")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("url", txtURL.Value)

        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()


        If tmp = "SUCCESS" Then
            Return True
        Else
            Return False
        End If


    End Function



    Protected Sub Repost_Data()

        Dim data As String

        data = "userpass=bw430fhww5"

        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email_address=" & txtEmail.Value
        data &= "&phone_num=" & txtPrimaryPhone.Value
        data &= "&address_one=&address_two=&address_three=&city=&state=&postal_code=&auto_leadid=&comments="
        data &= "&vendor_id=" & txtSourceID.Value

        Dim this_uri As Uri
        this_uri = New Uri("http://dialer205.jdmphone.com/estromes.php?" & data)




        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        'web_request.ContentLength = data.Length
        'web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        Response.Write("<br>Repost:" & tmp)

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()
        Response.End()
    End Sub


      Protected Sub Repost_Live()
        Dim this_uri As Uri

        this_uri = New Uri("http://live.estomes.com")



        Dim data As String = "vendor_id=2&sub_id=LR_PNIL"

        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&primary_phone=" & txtPrimaryPhone.Value
        data &= "&citizen=" & txtCitizen.Value
        data &= "&field_of_interest=" & txtInterest.Value
        data &= "&has_hs_or_ged=" & txtGraduated.Value
        ' data &= "&source_id=" & Request("source_id").ToString


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()

    End Sub

  
    Protected Function Post_Email(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri

        Dim data As String

        'http://submission.lvlivefeed2.com/c2c_dpost.php?from=<email>&first_name=<firstname>&last_name=<lastname>&zip=<zip>&ipaddress=<ipaddress>&sitename=<site>&listname=<listname>

        this_uri = New Uri("http://submission.lvlivefeed2.com/c2c_dpost.php")
        data = "listname=estomes" '&CampaignId=eStomes
        data &= "&from=" & txtEmail.Value
        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&zip=" & txtZip.Value
        data &= "&ipaddress=" & txtIP.Value
        data &= "&sitename=" & txtURL.Value


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        'Response.Write(this_uri.AbsoluteUri & "?" & data)

        Dim tmp As String
        Try
            Dim web_response As HttpWebResponse = web_request.GetResponse()
            Dim reader As New StreamReader(web_response.GetResponseStream())
            tmp = reader.ReadToEnd()
            web_response.Close()
        Catch ex As WebException
            tmp = ex.Response.Headers.ToString
        End Try






        If show_result Then
            'Response.Write(tmp)
            'Response.End()

        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf(Chr(34) & "IsValid" & Chr(34) & ":true") = -1 Then 'Fail" Then
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_Email")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf(Chr(34) & "IsValid" & Chr(34) & ":true") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function Post_database(show_result As Boolean, first_pass As Boolean) As Boolean

        Dim data As String = ""




        Dim tmp As String = ""


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("show_result", show_result)

        reply.Parameters.AddWithValue("post_result", "SUCCESS")

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", "N/A")
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_database")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf(Chr(34) & "IsValid" & Chr(34) & ":true") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function


    Protected Sub Page_PreRenderComplete(sender As Object, e As System.EventArgs) Handles Me.PreRenderComplete

    End Sub


End Class

