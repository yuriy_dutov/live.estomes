﻿Imports Common
Imports System.Data

Partial Class function_designer
    Inherits System.Web.UI.Page

    Protected Sub GridView1_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "Clone" Then
            UpdateSchoolTable("INSERT INTO [post_info] ([FirstName], [LastName], [Email], [Phone], [Zip], [Address], [City], [State], [IP], [Citizen], [Graduated], [Interest], [LeadID], [OptInDate], [OptInTime], [SourceID], [post_URL], [fixed_string], [post_name], [active], Rejected) select [FirstName], [LastName], [Email], [Phone], [Zip], [Address], [City], [State], [IP], [Citizen], [Graduated], [Interest], [LeadID], [OptInDate], [OptInTime], [SourceID], [post_URL], [fixed_string], [post_name], [active], Rejected from post_info where id = " & GridView1.DataKeys(e.CommandArgument).Value)
            'GridView1.DataBind()
            Dim new_post As DataTable = GetSchoolTable("select max(id) from post_info")
            If new_post.Rows.Count > 0 Then
                Response.Redirect("function_edit.aspx?ID=" & new_post.Rows(0).Item(0))
            End If
        End If


    End Sub

    Protected Sub GridView1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView1.SelectedIndexChanged
        Response.Redirect("function_edit.aspx?ID=" & GridView1.DataKeys(GridView1.SelectedIndex).Value)
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd.Click
        dsPost.Insert()
        Dim new_post As DataTable = GetSchoolTable("select max(id) from post_info")
        If new_post.Rows.Count > 0 Then
            Response.Redirect("function_edit.aspx?ID=" & new_post.Rows(0).Item(0))

        End If
    End Sub
End Class
