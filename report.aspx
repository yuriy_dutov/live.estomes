﻿<%@ Page Language="VB" AutoEventWireup="false" EnableEventValidation="false" CodeFile="report.aspx.vb"
    Inherits="report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            Start Date:<asp:TextBox ID="StartDate" runat="server">

            </asp:TextBox>
            <asp:CalendarExtender TargetControlID="StartDate" ID="CalendarExtender1" runat="server">
            </asp:CalendarExtender>
            End Date:
        <asp:TextBox ID="EndDate" runat="server">

        </asp:TextBox>
            <asp:CalendarExtender TargetControlID="EndDate" ID="CalendarExtender2" runat="server">
            </asp:CalendarExtender>
            Sub-id:
        <asp:DropDownList ID="ddlSource" DataTextField="sub_id" DataValueField="sub_id" AppendDataBoundItems="True"
            runat="server">
            <asp:ListItem Text="(Select)" Value=""></asp:ListItem>
        </asp:DropDownList>
            <asp:Button ID="btnGo" runat="server" Text="Go" /><br />
            <asp:Button ID="btnExport" runat="server" Text="Export to Excel" />
            <div>
                <asp:GridView ID="GridView1" EnableViewState="false" runat="server" AutoGenerateColumns="False" DataSourceID="dsExportList"
                    AllowSorting="True" BackColor="White" BorderColor="#999999" BorderStyle="None"
                    BorderWidth="1px" CellPadding="3" GridLines="Vertical">
                    <AlternatingRowStyle BackColor="#DCDCDC" />
                    <Columns>
                        <asp:BoundField DataField="first_name" HeaderText="First Name" SortExpression="first_name" />
                        <asp:BoundField DataField="last_name" HeaderText="Last Name" SortExpression="last_name" />
                        <asp:BoundField DataField="email" HeaderText="Email" SortExpression="email" />
                        <asp:BoundField DataField="primary_phone" HeaderText="Phone Number" SortExpression="primary_phone" />
                        <asp:BoundField DataField="address" HeaderText="Address" SortExpression="address" />
                        <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
                        <asp:BoundField DataField="State" HeaderText="State" SortExpression="State" />
                        <asp:BoundField DataField="Zip" HeaderText="Zip" SortExpression="Zip" />
                        <asp:BoundField DataField="sub_id" HeaderText="Sub ID" SortExpression="sub_id" />
                        <asp:BoundField DataField="submitted_date" HeaderText="Date" SortExpression="submitted_date" />
                        <asp:BoundField DataField="show_result" HeaderText="Visible" SortExpression="show_result" />
                        <asp:BoundField DataField="URL" HeaderText="URL" SortExpression="URL" />
                        <asp:BoundField DataField="post_result" HeaderText="Post Result" SortExpression="post_result" />
                        <asp:BoundField DataField="force_reject" HeaderText="Forced Reject" SortExpression="force_reject" />
                        <asp:BoundField DataField="result" HeaderText="Result" ItemStyle-Wrap="true" SortExpression="result" />
                        <asp:BoundField DataField="post_function" HeaderText="Posted To" ItemStyle-Wrap="true"
                            SortExpression="post_function" />
                        <asp:BoundField DataField="ipaddress" HeaderText="IP Address" ItemStyle-Wrap="true"
                            SortExpression="ipaddress" />
                        <%--<asp:BoundField DataField="opt_in" HeaderText="Original Opt In Date"
                        SortExpression="opt_in" />--%>
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#000065" />
                </asp:GridView>
                <asp:SqlDataSource ID="dsExportList" runat="server" ConnectionString="<%$ ConnectionStrings:MFPConnectionString %>"
                    SelectCommand="SELECT * FROM  MyFootpathAffiliates with (nolock)
                        WHERE     (submitted_date BETWEEN @start_date AND @end_date + ' 11:59pm' ) ">
                    <%--SelectCommand="SELECT *, case when sub_id like '%PNIL%' and isDupe = 1 then opt_in_date else '' end as opt_in FROM  MyFootpathAffiliates m 
                        left join estomes_data.dbo.NIP n on m.email = n.Email or m.primary_phone = n.phone
                        WHERE     (submitted_date BETWEEN @start_date AND @end_date + ' 11:59pm' )--%>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="hdnStartDate" Name="start_date" />
                        <asp:ControlParameter ControlID="hdnEndDate" Name="end_date" />
                        <asp:Parameter DefaultValue="Northeast" Name="Region" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:HiddenField ID="hdnStartDate" runat="server" />
                <asp:HiddenField ID="hdnEndDate" runat="server" />
            </div>
        </div>
    </form>
</body>
</html>
