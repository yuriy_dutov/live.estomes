﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class reporting_data_report
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SchoolConnectionString").ConnectionString)
            cn.Open()
            Dim reply As New SqlDataAdapter("select distinct owner from vicidial_list with (nolock)  order by owner", cn)
            Dim dt As New DataTable
            reply.Fill(dt)
            ddlSource.DataTextField = "owner"
            ddlSource.DataValueField = "owner"
            ddlSource.DataSource = dt
            ddlSource.DataBind()
        End If

    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As System.EventArgs) Handles btnUpdate.Click
        Dim sql As String = "select count(*) as [Number Entries], convert (varchar(10), convert (varchar, entry_date, 101), 101) as [Entry Date], owner as [Data Source] from dbo.vicidial_list with (nolock) where 1=1"
        If txtStart.Text <> "" Then
            sql &= " and entry_date >= '" & txtStart.Text & "' "
        End If

        If txtEnd.Text <> "" Then
            sql &= " and entry_date <= '" & txtEnd.Text & "' "
        End If

        If ddlSource.SelectedValue <> "" Then
            sql &= " and owner = '" & ddlSource.SelectedValue & "' "
        End If

        sql &= " group by convert (varchar(10), convert (varchar, entry_date, 101), 101), owner "
        sql &= " order by convert (varchar(10), convert (varchar, entry_date, 101), 101), owner "

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SchoolConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlDataAdapter(sql, cn)
        Dim dt As New DataTable
        reply.Fill(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()

    End Sub
End Class
