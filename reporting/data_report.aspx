﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="data_report.aspx.vb" Inherits="reporting_data_report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        Start Date:
        <asp:TextBox ID="txtStart" runat="server"></asp:TextBox>
        <asp:CalendarExtender TargetControlID="txtStart" ID="CalendarExtender1" runat="server">
        </asp:CalendarExtender>
        End Date :
        <asp:TextBox ID="txtEnd" runat="server"></asp:TextBox>
        <asp:CalendarExtender TargetControlID="txtEnd" ID="CalendarExtender2" runat="server">
        </asp:CalendarExtender>
        Data Source:
        <asp:DropDownList ID="ddlSource" runat="server">
        </asp:DropDownList>
        <asp:Button ID="btnUpdate" runat="server" Text="Go" />
        <br />
        <asp:GridView ID="GridView1" runat="server">
        </asp:GridView>
    </div>
    </form>
</body>
</html>
