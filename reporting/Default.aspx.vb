﻿
Partial Class reporting_Default
    Inherits System.Web.UI.Page

    Protected Sub btnGo_Click(sender As Object, e As System.EventArgs) Handles btnGo.Click
        dsSubmitted.DataBind()
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Response.ContentType = "application/vnd.ms-excel"
    End Sub
End Class
