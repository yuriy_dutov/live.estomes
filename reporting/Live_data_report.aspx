﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Live_data_report.aspx.vb"
    Inherits="reporting_data_report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div>
            Start Date:
        <asp:TextBox ID="txtStart" runat="server"></asp:TextBox>
            <asp:CalendarExtender TargetControlID="txtStart" ID="CalendarExtender1" runat="server">
            </asp:CalendarExtender>
            End Date :
        <asp:TextBox ID="txtEnd" runat="server"></asp:TextBox>
            <asp:CalendarExtender TargetControlID="txtEnd" ID="CalendarExtender2" runat="server">
            </asp:CalendarExtender>
            Data Source:
        <asp:DropDownList ID="ddlSource" runat="server" AppendDataBoundItems="true">
            <asp:ListItem Value="" Text="(Select)"></asp:ListItem>
        </asp:DropDownList>
            Vendor:
        <asp:DropDownList ID="ddlVendor" DataSourceID="dsVendors" AppendDataBoundItems="true" DataTextField="post_name" DataValueField="post_url" runat="server">
            <asp:ListItem Value="" Text="(Select)"></asp:ListItem>
             <asp:ListItem Value="N/A" Text="post_database"></asp:ListItem>
        </asp:DropDownList>

            First Pass:
            <asp:DropDownList ID="ddlFirst" runat="server">
                <asp:ListItem Text="All" Value=""></asp:ListItem>
                <asp:ListItem Text="Checked" Value="1"></asp:ListItem>
                <asp:ListItem Text="Not Checked" Value="0"></asp:ListItem>
            </asp:DropDownList>
            Force Reject:
            <asp:DropDownList ID="ddlForce" runat="server">
                                <asp:ListItem Text="All" Value=""></asp:ListItem>
                <asp:ListItem Text="Checked" Value="1"></asp:ListItem>
                <asp:ListItem Text="Not Checked" Value="0"></asp:ListItem>

            </asp:DropDownList>


            <asp:SqlDataSource ID="dsVendors" runat="server" ConnectionString="<%$ ConnectionStrings:SchoolConnectionString3 %>"
                SelectCommand="select  post_name, post_url from post_info order by post_name"></asp:SqlDataSource>
            <asp:Button ID="btnUpdate" runat="server" Text="Go" />
            <br />
            <asp:GridView ID="GridView1" EnableViewState="false" runat="server">
            </asp:GridView>
        </div>
    </form>
</body>
</html>
