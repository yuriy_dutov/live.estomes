﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Net
Imports System.IO

Partial Class reporting_data_report
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SchoolConnectionString").ConnectionString)
            cn.Open()
            Dim reply As New SqlDataAdapter("select distinct sub_id from MyFootpathAffiliates order by sub_id", cn)
            Dim dt As New DataTable
            reply.Fill(dt)
            ddlSource.DataTextField = "sub_id"
            ddlSource.DataValueField = "sub_id"
            ddlSource.DataSource = dt
            ddlSource.DataBind()
        End If

    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As System.EventArgs) Handles btnUpdate.Click
        Dim sql As String = "select convert (varchar(10), convert (varchar, submitted_date, 101), 101) as [Entry Date], post_function  as 'Vendor ID', count(*) as [Number Entries], " & _
            "convert(decimal(10,2), count(*) * revenue) as revenue,  convert(decimal(10,2), count(*) * cost * (case when force_reject = 1 then 0 else 1 end) * (case when first_pass = 0 then 0 else 1 end))   as cost, " & _
            "convert(decimal(10,2), count(*) * (revenue - cost  * (case when force_reject = 1 then 0 else 1 end) * (case when first_pass = 0 then 0 else 1 end) )) as profit,   " & _
            "sub_id as [Data Source], sum(case when post_result= 'SUCCESS' then 1 else 0 end) as [Success],sum(case when post_result= 'FAIL' then 1 else 0 end) as [Fail], " & _
            "convert(varchar(10),Convert(int,convert(float,sum(case when post_result= 'FAIL' then 1 else 0 end))/Count(*) * 100)) + '%'  as [% Fail], isDupe,  " & _
            "first_pass, force_reject   from dbo.MyFootpathAffiliates left join post_info on post_function = post_name where 1=1"
        If txtStart.Text <> "" Then
            sql &= " and submitted_date >= '" & txtStart.Text & "' "
        End If

        If txtEnd.Text <> "" Then
            sql &= " and submitted_date <= '" & txtEnd.Text & "' "
        End If

        If ddlSource.SelectedValue <> "" Then
            sql &= " and sub_id = '" & ddlSource.SelectedValue & "' "
        End If

        If ddlVendor.SelectedValue <> "" Then
            sql &= " and submitted_site = '" & ddlVendor.SelectedValue & "' "
        End If


        Select Case ddlFirst.SelectedValue
            Case "1"
                sql &= " and first_pass = 1 "
            Case "0"
                sql &= " and ((first_pass = 0) or (first_pass is null)) "
        End Select


        Select Case ddlForce.SelectedValue
            Case "1"
                sql &= " and force_reject = 1 "
            Case "0"
                sql &= " and ((force_reject = 0) or (force_reject is null)) "
        End Select
       

        sql &= " group by convert (varchar(10), convert (varchar, submitted_date, 101), 101), sub_id, post_function, isDupe, first_pass, force_reject, revenue, cost "
        sql &= " order by convert (varchar(10), convert (varchar, submitted_date, 101), 101), sub_id, post_function, isDupe, first_pass, force_reject, revenue, cost "

        Response.Write("<!--" & sql & "-->")

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SchoolConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlDataAdapter(sql, cn)
        reply.SelectCommand.CommandTimeout = 900
        Dim dt As New DataTable
        reply.Fill(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()

    End Sub

    Protected Sub GridView1_DataBound(sender As Object, e As System.EventArgs) Handles GridView1.DataBound

        Try
            GridView1.FooterRow.Visible = True
            GridView1.FooterRow.Font.Bold = True
            GridView1.FooterRow.Cells(2).Text = "0"
            GridView1.FooterRow.Cells(4).Text = "0"
            GridView1.FooterRow.Cells(5).Text = "0"


            For Each gvr As GridViewRow In GridView1.Rows
                If gvr.RowType = DataControlRowType.DataRow Then
                    GridView1.FooterRow.Cells(2).Text = Val(GridView1.FooterRow.Cells(2).Text) + Val(gvr.Cells(2).Text)
                    GridView1.FooterRow.Cells(3).Text = Val(GridView1.FooterRow.Cells(3).Text) + Val(gvr.Cells(3).Text)
                    GridView1.FooterRow.Cells(4).Text = Val(GridView1.FooterRow.Cells(4).Text) + Val(gvr.Cells(4).Text)
                    GridView1.FooterRow.Cells(5).Text = Val(GridView1.FooterRow.Cells(5).Text) + Val(gvr.Cells(5).Text)

                    GridView1.FooterRow.Cells(7).Text = Val(GridView1.FooterRow.Cells(7).Text) + Val(gvr.Cells(7).Text)
                    GridView1.FooterRow.Cells(8).Text = Val(GridView1.FooterRow.Cells(8).Text) + Val(gvr.Cells(8).Text)

                End If
            Next

            GridView1.FooterRow.Cells(9).Text = CInt(Val(GridView1.FooterRow.Cells(8).Text) / Val(GridView1.FooterRow.Cells(2).Text) * 100) & "%"

        Catch ex As Exception

        End Try


    End Sub

    Protected Sub GridView1_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand

        Dim ddl As DropDownList
        For Each gvr As GridViewRow In GridView1.Rows
            If gvr.Cells(1).Text = e.CommandArgument Then
                ddl = gvr.FindControl("ddlVendorResubmit")
            End If
        Next


      
        Dim this_uri As Uri

        this_uri = New Uri("http://live.estomes.com")


        Dim dt As New DataTable
        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()

        Dim sql As String = "select * from MyFootpathAffiliates where post_result = 'FAIL' "

        If txtStart.Text <> "" Then
            Sql &= " and submitted_date >= '" & txtStart.Text & "' "
        End If

        If txtEnd.Text <> "" Then
            Sql &= " and submitted_date <= '" & txtEnd.Text & "' "
        End If

        If ddlSource.SelectedValue <> "" Then
            Sql &= " and sub_id = '" & ddlSource.SelectedValue & "' "
        End If

        Dim reply As New SqlDataAdapter(sql, cn)
        reply.Fill(dt)

        For Each dr As DataRow In dt.Rows
            Dim data As String = "vendor_id=" & ddl.SelectedValue & "&sub_id=" & dr("sub_id").ToString

            data &= "&first_name=" & dr("first_name").ToString
            data &= "&last_name=" & dr("last_name").ToString
            data &= "&email=" & dr("email").ToString
            data &= "&primary_phone=" & dr("primary_phone").ToString
            data &= "&citizen=" & dr("citizen").ToString
            data &= "&field_of_interest=" & dr("field_of_interest").ToString
            data &= "&has_hs_or_ged=" & dr("has_hs_or_ged").ToString
            ' data &= "&source_id=" & dr("source_id").ToString

            Response.Write("http://live.estomes.com/?" & data & "<br>")

            'Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
            'web_request.Method = WebRequestMethods.Http.Post
            'web_request.ContentLength = data.Length
            'web_request.ContentType = "application/x-www-form-urlencoded"
            'Dim writer As New StreamWriter(web_request.GetRequestStream)
            'writer.Write(data)
            'writer.Close()

            'Dim web_response As HttpWebResponse = web_request.GetResponse()
            'Dim reader As New StreamReader(web_response.GetResponseStream())
            'Dim tmp As String = reader.ReadToEnd()
            'web_response.Close()
        Next


        Response.End()



    End Sub



End Class
