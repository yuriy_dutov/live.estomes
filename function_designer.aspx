﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="function_designer.aspx.vb" Inherits="function_designer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h2>Function Designer</h2>
            <h2>
                <asp:Button ID="btnAdd" runat="server" Text="Add Post" />
            </h2>

            <asp:GridView ID="GridView1" Font-Size="X-Small" runat="server" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id" DataSourceID="dsPost" ForeColor="#333333" GridLines="None">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:CommandField ShowSelectButton="True" />
                    <asp:ButtonField ButtonType="Link" Text="Clone" CommandName="Clone" />
                    <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                    <asp:BoundField DataField="post_name" HeaderText="post_name" SortExpression="post_name" />
                    <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />
                    <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" />
                    <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                    <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
                    <asp:BoundField DataField="Zip" HeaderText="Zip" SortExpression="Zip" />
                    <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
                    <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
                    <asp:BoundField DataField="State" HeaderText="State" SortExpression="State" />
                    <asp:BoundField DataField="IP" HeaderText="IP" SortExpression="IP" />
                    <asp:BoundField DataField="Citizen" HeaderText="Citizen" SortExpression="Citizen" />
                    <asp:BoundField DataField="Graduated" HeaderText="Graduated" SortExpression="Graduated" />
                    <asp:BoundField DataField="Interest" HeaderText="Interest" SortExpression="Interest" />
                    <asp:BoundField DataField="LeadID" HeaderText="LeadID" SortExpression="LeadID" />
                    <asp:BoundField DataField="OptInDate" HeaderText="OptInDate" SortExpression="OptInDate" />
                    <asp:BoundField DataField="OptInTime" HeaderText="OptInTime" SortExpression="OptInTime" />
                    <asp:BoundField DataField="SourceID" HeaderText="SourceID" SortExpression="SourceID" />
                    <asp:BoundField DataField="post_URL" HeaderText="post_URL" SortExpression="post_URL" />
                    <asp:BoundField DataField="fixed_string" HeaderText="fixed_string" SortExpression="fixed_string" />

                    <asp:CheckBoxField DataField="active" HeaderText="active" SortExpression="active" />
                </Columns>
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <SortedAscendingCellStyle BackColor="#FDF5AC" />
                <SortedAscendingHeaderStyle BackColor="#4D0000" />
                <SortedDescendingCellStyle BackColor="#FCF6C0" />
                <SortedDescendingHeaderStyle BackColor="#820000" />
            </asp:GridView>
            <asp:SqlDataSource ID="dsPost" ConnectionString="<%$ ConnectionStrings:SchoolConnectionString3 %>" runat="server"
                DeleteCommand="DELETE FROM [post_info] WHERE [id] = @id" InsertCommand="INSERT INTO [post_info] ([FirstName], [LastName], [Email], [Phone], [Zip], 
                [Address], [City], [State], [IP], [Citizen], [Graduated], [Interest], [LeadID], [OptInDate], [OptInTime], [SourceID], [post_URL], 
                [fixed_string], [post_name], [active]) VALUES (@FirstName, @LastName, @Email, @Phone, @Zip, @Address, @City, @State, @IP, @Citizen, @Graduated,
                 @Interest, @LeadID, @OptInDate, @OptInTime, @SourceID, @post_URL, @fixed_string, @post_name, 1)"
                SelectCommand="SELECT * FROM [post_info]"
                UpdateCommand="UPDATE [post_info] SET [FirstName] = @FirstName, [LastName] = @LastName, [Email] = @Email, [Phone] = @Phone, [Zip] = @Zip, [Address] = @Address, [City] = @City, 
                [State] = @State, [IP] = @IP, [Citizen] = @Citizen, [Graduated] = @Graduated, [Interest] = @Interest, [LeadID] = @LeadID, [OptInDate] = @OptInDate, 
                [OptInTime] = @OptInTime, [SourceID] = @SourceID, [post_URL] = @post_URL, [fixed_string] = @fixed_string, [post_name] = @post_name, [active] = @active WHERE [id] = @id">
                <DeleteParameters>
                    <asp:Parameter Name="id" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="FirstName" Type="String" />
                    <asp:Parameter Name="LastName" Type="String" />
                    <asp:Parameter Name="Email" Type="String" />
                    <asp:Parameter Name="Phone" Type="String" />
                    <asp:Parameter Name="Zip" Type="String" />
                    <asp:Parameter Name="Address" Type="String" />
                    <asp:Parameter Name="City" Type="String" />
                    <asp:Parameter Name="State" Type="String" />
                    <asp:Parameter Name="IP" Type="String" />
                    <asp:Parameter Name="Citizen" Type="String" />
                    <asp:Parameter Name="Graduated" Type="String" />
                    <asp:Parameter Name="Interest" Type="String" />
                    <asp:Parameter Name="LeadID" Type="String" />
                    <asp:Parameter Name="OptInDate" Type="String" />
                    <asp:Parameter Name="OptInTime" Type="String" />
                    <asp:Parameter Name="SourceID" Type="String" />
                    <asp:Parameter Name="post_URL" Type="String" />
                    <asp:Parameter Name="fixed_string" Type="String" />
                    <asp:Parameter Name="post_name" Type="String" />
                    <asp:Parameter Name="active" Type="Boolean" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="FirstName" Type="String" />
                    <asp:Parameter Name="LastName" Type="String" />
                    <asp:Parameter Name="Email" Type="String" />
                    <asp:Parameter Name="Phone" Type="String" />
                    <asp:Parameter Name="Zip" Type="String" />
                    <asp:Parameter Name="Address" Type="String" />
                    <asp:Parameter Name="City" Type="String" />
                    <asp:Parameter Name="State" Type="String" />
                    <asp:Parameter Name="IP" Type="String" />
                    <asp:Parameter Name="Citizen" Type="String" />
                    <asp:Parameter Name="Graduated" Type="String" />
                    <asp:Parameter Name="Interest" Type="String" />
                    <asp:Parameter Name="LeadID" Type="String" />
                    <asp:Parameter Name="OptInDate" Type="String" />
                    <asp:Parameter Name="OptInTime" Type="String" />
                    <asp:Parameter Name="SourceID" Type="String" />
                    <asp:Parameter Name="post_URL" Type="String" />
                    <asp:Parameter Name="fixed_string" Type="String" />
                    <asp:Parameter Name="post_name" Type="String" />
                    <asp:Parameter Name="active" Type="Boolean" />
                    <asp:Parameter Name="id" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>

        </div>
    </form>
</body>
</html>
