﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="routing_admin.aspx.vb" Inherits="routing_admin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Button ID="btnAdd" runat="server" Text="Add" />
        <asp:GridView ID="gvRouting" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            CellPadding="4" DataKeyNames="id" DataSourceID="dsRouting" ForeColor="#333333"
            GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:CommandField ShowEditButton="True" />
                <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True"
                    SortExpression="id" />
                <asp:BoundField DataField="sub_id" HeaderText="Sub ID" SortExpression="sub_id" />
                <asp:TemplateField HeaderText="Route To" SortExpression="send_to">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList1" runat="server" SelectedValue='<%# Bind("send_to") %>'
                            AppendDataBoundItems="true" DataSourceID="dsPostNames" DataTextField="post_name"
                            DataValueField="post_name">
                            <asp:ListItem Text="(Select)" Value=""></asp:ListItem>
                            <asp:ListItem>Post_database</asp:ListItem>
                            <asp:ListItem>Post_Email</asp:ListItem>
                            <asp:ListItem>post_Duplicate</asp:ListItem>
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("send_to") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Fail To" SortExpression="fail_to">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList2" runat="server" SelectedValue='<%# Bind("fail_to") %>'
                            AppendDataBoundItems="true" DataSourceID="dsPostNames" DataTextField="post_name"
                            DataValueField="post_name">
                            <asp:ListItem Text="(Select)" Value=""></asp:ListItem>
                            <asp:ListItem>Post_database</asp:ListItem>
                            <asp:ListItem>Post_Email</asp:ListItem>
                            <asp:ListItem>post_Duplicate</asp:ListItem>
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("fail_to") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CheckBoxField DataField="post_email" HeaderText="Post Email" />
                <asp:TemplateField HeaderText="Second Fail To" SortExpression="fail_to">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList3" runat="server" SelectedValue='<%# Bind("second_fail") %>'
                            AppendDataBoundItems="true" DataSourceID="dsPostNames" DataTextField="post_name"
                            DataValueField="post_name">
                            <asp:ListItem Text="(Select)" Value=""></asp:ListItem>
                            <asp:ListItem>Post_database</asp:ListItem>
                            <asp:ListItem>Post_Email</asp:ListItem>
                            <asp:ListItem>post_Duplicate</asp:ListItem>
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("second_fail") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="prefix" SortExpression="prefix" HeaderText="Prefix" />
                <asp:BoundField DataField="daily_cap" SortExpression="daily_cap" HeaderText="Daily Cap (TOTAL SENT TO BOTH)" />
                <asp:TemplateField HeaderText="Secondary Post During Cap" SortExpression="daily_roll_to">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList4" runat="server" SelectedValue='<%# Bind("daily_roll_to") %>'
                            AppendDataBoundItems="true" DataSourceID="dsPostNames" DataTextField="post_name"
                            DataValueField="post_name">
                            <asp:ListItem Text="(Select)" Value=""></asp:ListItem>
                            <asp:ListItem>Post_database</asp:ListItem>
                            <asp:ListItem>Post_Email</asp:ListItem>
                            <asp:ListItem>post_Duplicate</asp:ListItem>
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("daily_roll_to") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowDeleteButton="True" />
            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>
        <asp:SqlDataSource ID="dsRouting" runat="server" ConnectionString="<%$ ConnectionStrings:EstomesConnectionString %>"
            DeleteCommand="DELETE FROM [Routing_estomes] WHERE [id] = @id" InsertCommand="INSERT INTO [Routing_estomes] ( [sub_id], [send_to]) VALUES ( @sub_id, @send_to)"
            SelectCommand="SELECT * FROM [Routing_estomes]" UpdateCommand="UPDATE [Routing_estomes] SET post_email=@post_email, daily_roll_to=@daily_roll_to, daily_cap=@daily_cap, second_fail=@second_fail, prefix=@prefix, fail_to=@fail_to,[sub_id] = @sub_id, [send_to] = @send_to WHERE [id] = @id">
            <DeleteParameters>
                <asp:Parameter Name="id" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="sub_id" Type="String" />
                <asp:Parameter Name="send_to" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="sub_id" Type="String" />
                <asp:Parameter Name="send_to" Type="String" />
                <asp:Parameter Name="fail_to" Type="String" />
                <asp:Parameter Name="second_fail" Type="String" />
                <asp:Parameter Name="prefix" Type="String" />
                <asp:Parameter Name="daily_roll_to" Type="String" />
                <asp:Parameter Name="id" Type="Int32" />
                <asp:Parameter Name="daily_cap" Type="Int32" />
                <asp:Parameter Name="post_email" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsPostNames" runat="server" ConnectionString="<%$ ConnectionStrings:MFPConnectionString %>"
            SelectCommand="select distinct post_name from post_info where active = 1"></asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
