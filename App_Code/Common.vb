﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports system.net.mail

Public Class Common

    Public Shared Function GetTable(sql As String) As DataTable
        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("EstomesConnectionString").ConnectionString)
        cn.Open()

        Dim reply As New SqlDataAdapter(sql, cn)
        reply.SelectCommand.CommandTimeout = 300
        Dim dt As New DataTable
        reply.Fill(dt)
        cn.Close()
        cn.Dispose()
        Return dt

    End Function

    Public Shared Sub UpdateTable(sql As String)
        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("EstomesConnectionString").ConnectionString)
        cn.Open()

        Dim reply As New SqlCommand(sql, cn)
        reply.CommandTimeout = 300
        reply.ExecuteNonQuery()
        cn.Close()
        cn.Dispose()

    End Sub

    Public Shared Function GetSchoolTable(sql As String) As DataTable
        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()

        Dim reply As New SqlDataAdapter(sql, cn)
        reply.SelectCommand.CommandTimeout = 300
        Dim dt As New DataTable
        reply.Fill(dt)
        cn.Close()
        cn.Dispose()
        Return dt

    End Function

    Public Shared Sub UpdateSchoolTable(sql As String)
        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()

        Dim reply As New SqlCommand(sql, cn)
        reply.CommandTimeout = 300
        reply.ExecuteNonQuery()
        cn.Close()
        cn.Dispose()

    End Sub


    Public Shared Sub Email_Error(error_text As String, Optional also_email As String = "")
        ' Send notification e-mail
        Dim Email As MailMessage = New MailMessage("stace@datadrivendevelopment.com", "stace@datadrivendevelopment.com")

        If also_email <> "" Then
            Dim add_emails() As String = also_email.Split(";")
            For Each add_email In add_emails
                Email.To.Add(New MailAddress(add_email))
            Next
        End If

        'Email.To.Add(New MailAddress("stace@datadrivendevelopment.com"))
        Email.IsBodyHtml = False
        Email.Subject = "WEB SITE ERROR"
        Email.Body = "Source Page: " & System.Web.HttpContext.Current.Request.Path & Chr(13)
        Email.Body &= "Referrer: " & System.Web.HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & Chr(13)
        Email.Body &= "Name: " & System.Web.HttpContext.Current.User.Identity.Name & Chr(13)
        Email.Body &= "Error Information" & Chr(13) & Chr(13)
        'Email.Body &= "Name:   " & ShowAttribute("name") & Chr(13)
        'Email.Body &= "Corp ID: " & ShowAttribute("corpid") & Chr(13)
        'Email.Body &= "Manager ID: " & ShowAttribute("reportsto") & Chr(13)
        Email.Body &= "Browser: " & System.Web.HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT") & Chr(13)
        'Email.Body &= "Session: " & System.Web.HttpContext.Current.Session.Count.ToString & Chr(13)

        'Email.Body &= context.ToString() & Chr(13)
        'For i As Integer = 0 To context.Session.Count - 1
        'Email.Body &= context.Session(i).ToString() & Chr(13)
        'Next
        Try
            Email.Body &= "QueryString: " & Chr(13)
            Dim qs_string() As String = System.Web.HttpContext.Current.Request.QueryString.AllKeys
            For Each qs_str As String In qs_string
                Email.Body &= qs_str & " - " & System.Web.HttpContext.Current.Request.QueryString(qs_str).ToString & Chr(13)

            Next
        Catch ex As Exception

        End Try

        'For i As Integer = 0 To System.Web.HttpContext.Current.Request.QueryString.Count - 1
        '    Email.Body &= System.Web.HttpContext.Current.Request.QueryString(i).ToString() + " - " & System.Web.HttpContext.Current.Request.QueryString(i).ToString() & Chr(13)
        'Next
        Email.Body &= "Date and time: " & Now().ToString & Chr(13)
        Email.Body &= "Page: " & System.Web.HttpContext.Current.Request.ServerVariables("URL") & Chr(13)
        Email.Body &= "Remote Address: " & System.Web.HttpContext.Current.Request.ServerVariables("REMOTE_ADDR") & Chr(13)
        Email.Body &= "HTTP Referer: " & System.Web.HttpContext.Current.Request.ServerVariables("HTTP_REFERER") & Chr(13) & Chr(13)

        Email.Body &= error_text
        Email.Priority = MailPriority.High
        Dim sc As SmtpClient = New SmtpClient("64.111.27.71")
        sc.Send(Email)
    End Sub




End Class
