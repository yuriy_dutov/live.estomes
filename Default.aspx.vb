﻿Option Compare Text

Imports System.Net
Imports System.IO
Imports System.Data.SqlClient
Imports System.Data
Imports Common

Partial Class _Default
    Inherits System.Web.UI.Page

    Dim isDupe As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls

        If Not IsPostBack Then
            If Request("first_name") IsNot Nothing Then
                If Request("first_name") IsNot Nothing Then
                    txtFirstName.Text = Request("first_name")
                End If
                If Request("last_name") IsNot Nothing Then
                    txtLastName.Text = Request("last_name")
                End If
                If Request("email") IsNot Nothing Then
                    txtEmail.Text = Request("email")
                End If
                If Request("primary_phone") IsNot Nothing Then
                    txtPrimaryPhone.Text = Request("primary_phone").Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "")
                End If
                If Request("citizen") IsNot Nothing Then
                    txtCitizen.Text = Request("citizen")
                Else
                    txtCitizen.Text = "Y"
                End If
                If Request("field_of_interest") IsNot Nothing Then
                    txtInterest.Text = Request("field_of_interest")
                End If
                If Request("has_hs_or_ged") IsNot Nothing Then
                    txtGraduated.Text = Request("has_hs_or_ged")
                End If
                If Request("sub_id") IsNot Nothing Then
                    txtSourceID.Text = Request("sub_id")
                End If

                If Request("address") IsNot Nothing Then
                    txtAddress.Text = Request("address")
                End If
                If Request("city") IsNot Nothing Then
                    txtCity.Text = Request("city")
                End If
                If Request("state") IsNot Nothing Then
                    txtState.Text = Request("state")
                End If
                If Request("zip") IsNot Nothing Then
                    txtZip.Text = Request("zip")
                End If
                If Request("optindate") IsNot Nothing Then
                    txtOptInDate.Text = Request("optindate")
                End If


                If Request("optintime") IsNot Nothing Then
                    txtOptInTime.Text = Request("optintime")
                End If

                If Request("lead_id") IsNot Nothing Then
                    txtLeadID.Text = Request("lead_id")
                End If

                If Request("ipaddress") IsNot Nothing Then
                    txtIP.Text = Request("ipaddress")
                End If

                If Request("url") IsNot Nothing Then
                    txtURL.Text = Request("url")
                End If

                If Request("test") IsNot Nothing Then
                    ddlTest.SelectedValue = "Yes"
                Else
                    ddlTest.SelectedValue = "No"
                End If

                Dim emergencyMode = Boolean.Parse(System.Configuration.ConfigurationManager.AppSettings("emergencyMode"))

                If emergencyMode = True Then

                    Dim conStr As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SchoolConnectionString1").ConnectionString)
                    conStr.Open()

                    Dim cmd As New SqlCommand()
                    cmd.CommandType = CommandType.Text
                    cmd.CommandText = "INSERT INTO template_lead_info (FirstName, LastName, Email, Phone, Citizen, Interest, Graduated, SourceID, Address, City, State, Zip, OptInDate, OptInTime, LeadID, IP, URL, test)" & vbCrlf &
                                      "values(@FirstName, @LastName, @Email, @PrimaryPhone, @Citizen, @Interest, @Graduated, @SourceID, @Address, @City, @State, @Zip, @OptInDate, @OptInTime, @LeadID, @IP, @URL, @Test)"
                    cmd.Parameters.AddWithValue("FirstName",txtFirstName.Text)
                    cmd.Parameters.AddWithValue("LastName",txtLastName.Text)
                    cmd.Parameters.AddWithValue("Email",txtEmail.Text)
                    cmd.Parameters.AddWithValue("PrimaryPhone",txtPrimaryPhone.Text)
                    cmd.Parameters.AddWithValue("Citizen",txtCitizen.Text)
                    cmd.Parameters.AddWithValue("Interest",txtInterest.Text)
                    cmd.Parameters.AddWithValue("Graduated",txtGraduated.Text)
                    cmd.Parameters.AddWithValue("SourceID",txtSourceID.Text)
                    cmd.Parameters.AddWithValue("Address",txtAddress.Text)
                    cmd.Parameters.AddWithValue("City",txtCity.Text)
                    cmd.Parameters.AddWithValue("State",txtState.Text)
                    cmd.Parameters.AddWithValue("Zip",txtZip.Text)
                    cmd.Parameters.AddWithValue("OptInDate",txtOptInDate.Text)
                    cmd.Parameters.AddWithValue("OptInTime",txtOptInTime.Text)
                    cmd.Parameters.AddWithValue("LeadID",txtLeadID.Text)
                    cmd.Parameters.AddWithValue("IP",txtIP.Text)
                    cmd.Parameters.AddWithValue("URL",txtURL.Text)
                    cmd.Parameters.AddWithValue("Test",ddlTest.Text)

                    cmd.Connection = conStr
                    cmd.ExecuteNonQuery()

                    conStr.Close()
                    Return

                End If
                    
                Dim check_dt As New DataTable


                Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("EstomesConnectionString").ConnectionString)
                cn.Open()

                If Request("sub_id") = "PNIL" Then

                    Dim reply3 As New SqlDataAdapter("select * from unique_email_phone with (nolock) where (phone = @phone or email = @email) and tablename in ('OY2','NIP') ", cn)
                    reply3.SelectCommand.Parameters.AddWithValue("phone", txtPrimaryPhone.Text)
                    reply3.SelectCommand.Parameters.AddWithValue("email", txtEmail.Text)
                    reply3.Fill(check_dt)

                    If check_dt.Rows.Count > 0 Then
                        isDupe = True

                    End If

                    Dim ins As New SqlCommand("insert into unique_email_phone (phone, email, tablename, opt_in_date, uploaded) select @phone, @email, 'NIP', GetDate(), Getdate()", cn)
                    ins.Parameters.AddWithValue("phone", txtPrimaryPhone.Text)
                    ins.Parameters.AddWithValue("email", txtEmail.Text)
                    ins.ExecuteNonQuery()

                End If

                If Request("sub_id") = "CMS" Then

                    Dim reply3 As New SqlDataAdapter("select * from cms_email_phone with (nolock) where (phone = @phone or email = @email)", cn)
                    reply3.SelectCommand.Parameters.AddWithValue("phone", txtPrimaryPhone.Text)
                    reply3.SelectCommand.Parameters.AddWithValue("email", txtEmail.Text)
                    reply3.Fill(check_dt)

                    If check_dt.Rows.Count > 0 Then
                        isDupe = True

                    End If

                    Dim ins As New SqlCommand("insert into cms_email_phone (phone, email, tablename, opt_in_date, uploaded) select @phone, @email, 'NA', GetDate(), Getdate()", cn)
                    ins.Parameters.AddWithValue("phone", txtPrimaryPhone.Text)
                    ins.Parameters.AddWithValue("email", txtEmail.Text)
                    ins.ExecuteNonQuery()

                End If


                If Request("vendor_id") IsNot Nothing Then

                    Dim dt As New DataTable

                    Dim reply As New SqlDataAdapter("select * from Vendor_Routing with(nolock) where vendor_id = @vendor_id", cn)
                    reply.SelectCommand.Parameters.AddWithValue("vendor_id", Request("vendor_id"))

                    reply.Fill(dt)
                    If Not try_submit(dt.Rows(0).Item("send_to"), isDupe, True) Then
                        Response.Write("FAIL - Duplicate Lead")
                        txtSourceID.Text = dt.Rows(0).Item("prefix").ToString & "_" & txtSourceID.Text
                        try_submit(dt.Rows(0).Item("fail_to").ToString)
                    Else
                        Response.Write("Successful Post")
                    End If

                    cn.Close()
                    cn.Dispose()
                    Response.End()


                End If

                If Request("sub_id") IsNot Nothing Then

                    Dim dt As New DataTable

                    Dim reply As New SqlDataAdapter("select * from Routing_estomes with(nolock) where sub_id = @sub_id", cn)
                    reply.SelectCommand.Parameters.AddWithValue("sub_id", Request("sub_id"))

                    reply.Fill(dt)

                    'Get daily cap
                    'select count(*) from MyFootpathAffiliates where sub_id = 'PNIL' and submitted_date > convert(varchar(10),GetDate(),101)

                    Dim cn2 As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
                    cn2.Open()
                    Dim daily_dt As New DataTable
                    Dim reply2 As New SqlDataAdapter("select count(*) from MyFootpathAffiliates where sub_id = @sub_id and submitted_date > convert(varchar(10),GetDate(),101)", cn2)
                    reply2.SelectCommand.Parameters.AddWithValue("sub_id", Request("sub_id"))
                    reply2.Fill(daily_dt)
                    cn2.Close()
                    cn2.Dispose()


                    If isDupe Then
                        try_submit(dt.Rows(0).Item("fail_to").ToString)
                        Response.Write("FAIL - Duplicate Lead")
                        cn.Close()
                        cn.Dispose()
                        Response.End()
                    End If


                    Dim daily_count As Integer = 0
                    If daily_dt.Rows.Count > 0 Then
                        daily_count = daily_dt.Rows(0).Item(0)
                    End If



                    If dt.Rows.Count > 0 Then

                        If ((dt.Rows(0).Item("post_email").ToString = "True") Or (dt.Rows(0).Item("daily_roll_to").ToString = "True")) Then
                            Post_Email(False, False)
                        End If

                        If daily_count <> 0 And ((daily_count < dt.Rows(0).Item("daily_cap")) And (daily_count Mod 2 = 0)) Then
                            If Not try_submit(dt.Rows(0).Item("daily_roll_to").ToString, isDupe, True) Then
                                Response.Write("FAIL - Duplicate Lead")
                                If Not try_submit(dt.Rows(0).Item("send_to").ToString, isDupe) Then
                                    txtSourceID.Text = dt.Rows(0).Item("prefix").ToString & "_" & txtSourceID.Text
                                    If Not try_submit(dt.Rows(0).Item("fail_to").ToString) Then
                                        txtSourceID.Text = dt.Rows(0).Item("prefix").ToString & "_" & txtSourceID.Text
                                        If try_submit(dt.Rows(0).Item("second_fail").ToString) Then
                                            cn.Close()
                                            cn.Dispose()
                                            Response.End()
                                        End If
                                    Else
                                        cn.Close()
                                        cn.Dispose()
                                        Response.End()
                                    End If

                                End If
                            Else
                                cn.Close()
                                cn.Dispose()
                                Response.Write("Successful Post")
                                Response.End()
                            End If
                        Else
                            If Not try_submit(dt.Rows(0).Item("send_to").ToString, isDupe, True) Then
                                Response.Write("FAIL - Duplicate Lead")
                                If Not try_submit(dt.Rows(0).Item("daily_roll_to").ToString, isDupe) Then

                                    txtSourceID.Text = dt.Rows(0).Item("prefix").ToString & "_" & txtSourceID.Text
                                    If Not try_submit(dt.Rows(0).Item("fail_to").ToString) Then

                                        txtSourceID.Text = dt.Rows(0).Item("prefix").ToString & "_" & txtSourceID.Text
                                        If try_submit(dt.Rows(0).Item("second_fail").ToString) Then
                                            cn.Close()
                                            cn.Dispose()
                                            Response.End()
                                        End If
                                    Else
                                        cn.Close()
                                        cn.Dispose()
                                        Response.End()
                                    End If

                                End If
                            Else
                                cn.Close()
                                cn.Dispose()
                                Response.Write("Successful Post")
                                Response.End()
                            End If
                        End If

                    End If

                End If


                cn.Close()
                cn.Dispose()
                Response.End()


            End If
        End If
    End Sub

    Protected Function try_submit(routine As String, Optional isDupe As Boolean = False, Optional first_pass As Boolean = False, Optional force_reject As Boolean = False) As Boolean



        'If routine = "" Then
        '    Return False
        'End If

        'new try_submit
        If Not isDupe Then
            Dim post_dt As DataTable = GetSchoolTable("select * from post_info where post_name = '" & routine & "'")
            If post_dt.Rows.Count > 0 Then

                Dim random_rate As Random = New Random()

                Dim rand_check As Integer = random_rate.Next(1, 100)

                If Not isdbnull(post_dt.Rows(0).Item("reject")) Then
                    If (post_dt.Rows(0).Item("reject") = "1") And (post_dt.Rows(0).Item("reject_rate") > rand_check) Then
                        force_reject = True
                    End If
                End If

                Dim this_uri As Uri
                Dim data As String = ""


                this_uri = New Uri(post_dt.Rows(0).Item("post_URL"))
                If post_dt.Rows(0).Item("fixed_string").ToString <> "" Then
                    data &= post_dt.Rows(0).Item("fixed_string")
                End If
                If post_dt.Rows(0).Item("FirstName").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("FirstName") & "=" & txtFirstName.Text
                End If
                If post_dt.Rows(0).Item("LastName").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("LastName") & "=" & txtLastName.Text
                End If
                If post_dt.Rows(0).Item("Email").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Email") & "=" & txtEmail.Text
                End If
                If post_dt.Rows(0).Item("Phone").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Phone") & "=" & txtPrimaryPhone.Text
                End If
                If post_dt.Rows(0).Item("Zip").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Zip") & "=" & txtZip.Text
                End If
                If post_dt.Rows(0).Item("Address").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Address") & "=" & txtAddress.Text
                End If
                If post_dt.Rows(0).Item("City").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("City") & "=" & txtCity.Text
                End If
                If post_dt.Rows(0).Item("State").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("State") & "=" & txtState.Text
                End If
                If post_dt.Rows(0).Item("IP").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("IP") & "=" & txtIP.Text
                End If
                If post_dt.Rows(0).Item("Citizen").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Citizen") & "=" & txtCitizen.Text
                End If
                If post_dt.Rows(0).Item("Graduated").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Graduated") & "=" & txtGraduated.Text
                End If
                If post_dt.Rows(0).Item("Interest").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Interest") & "=" & txtInterest.Text
                End If
                If post_dt.Rows(0).Item("LeadID").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("LeadID") & "=" & txtLeadID.Text
                End If
                If post_dt.Rows(0).Item("OptinDate").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("OptinDate") & "=" & txtOptInDate.Text
                End If
                If post_dt.Rows(0).Item("OptInTime").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("OptInTime") & "=" & txtOptInTime.Text
                End If
                If post_dt.Rows(0).Item("SourceID").ToString <> "" Then

                    Dim sources() As String = post_dt.Rows(0).Item("SourceID").ToString.Split("|")
                    For Each source In sources
                        data &= "&" & source & "=" & txtSourceID.Text
                    Next

                    'data &= "&" & post_dt.Rows(0).Item("SourceID") & "=" & txtSourceID.Text
                End If
                If post_dt.Rows(0).Item("URL").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("URL") & "=" & txtURL.Text
                End If
                If post_dt.Rows(0).Item("Today").ToString <> "" Then

                    Dim sources() As String = post_dt.Rows(0).Item("Today").ToString.Split("|")
                    For Each source In sources
                        data &= "&" & source & "=" & Today.ToShortDateString
                    Next


                    'data &= "&" & post_dt.Rows(0).Item("Today") & "=" & Today.ToShortDateString
                End If



                Dim web_request As HttpWebRequest

                If post_dt.Rows(0).Item("method").ToString = "post" Or post_dt.Rows(0).Item("method").ToString = "" Then

                    web_request = HttpWebRequest.Create(this_uri)
                    web_request.Method = WebRequestMethods.Http.Post
                    web_request.ContentLength = System.Text.Encoding.UTF8.GetByteCount(data)
                    web_request.ContentType = "application/x-www-form-urlencoded"
                    Dim writer As New StreamWriter(web_request.GetRequestStream)
                    writer.Write(data, 0, System.Text.Encoding.UTF8.GetByteCount(data))
                    writer.Close()
                Else
                    this_uri = New Uri(post_dt.Rows(0).Item("post_URL") & "?" & data)
                    web_request = HttpWebRequest.Create(this_uri)
                    web_request.Method = WebRequestMethods.Http.Get

                End If



                'web_request.ContentLength = data.Length



                Dim web_response As HttpWebResponse = web_request.GetResponse()
                Dim reader As New StreamReader(web_response.GetResponseStream())
                Dim tmp As String = reader.ReadToEnd()
                web_response.Close()



                Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
                cn.Open()
                Dim reply As New SqlCommand("AddMFP", cn)
                reply.CommandType = System.Data.CommandType.StoredProcedure
                reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
                reply.Parameters.AddWithValue("last_name", txtLastName.Text)
                reply.Parameters.AddWithValue("email", txtEmail.Text)
                reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
                reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
                reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
                reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

                reply.Parameters.AddWithValue("address", txtAddress.Text)
                reply.Parameters.AddWithValue("city", txtCity.Text)
                reply.Parameters.AddWithValue("state", txtState.Text)
                reply.Parameters.AddWithValue("zip", txtZip.Text)

                reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
                reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
                reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

                reply.Parameters.AddWithValue("show_result", True)
                If tmp.IndexOf(post_dt.Rows(0).Item("rejected").ToString) > -1 Then 'Fail
                    reply.Parameters.AddWithValue("post_result", "FAIL")
                Else
                    reply.Parameters.AddWithValue("post_result", "SUCCESS")
                End If

                reply.Parameters.AddWithValue("lead_id", txtLeadID.Text)

                reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
                reply.Parameters.AddWithValue("submitted_data", data)
                reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
                reply.Parameters.AddWithValue("result", tmp)
                reply.Parameters.AddWithValue("isDupe", isDupe)
                reply.Parameters.AddWithValue("first_pass", first_pass)
                reply.Parameters.AddWithValue("post_function", post_dt.Rows(0).Item("post_name").ToString)
                reply.Parameters.AddWithValue("url", txtURL.Text)

                reply.Parameters.AddWithValue("force_reject", force_reject)

                If Request("vendor_id") IsNot Nothing Then
                    reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
                Else
                    reply.Parameters.AddWithValue("vendor_id", "0")
                End If


                reply.Parameters.AddWithValue("query_string", Request.Url.Query)

                'If routine = "Post_leadsresell_YCA" Then
                '    'email_error(cn.State.ToString)
                'End If
                Try
                    reply.ExecuteNonQuery()
                Catch ex As Exception
                    reply.ExecuteNonQuery()
                End Try

                cn.Close()

                If tmp.IndexOf(post_dt.Rows(0).Item("rejected").ToString) > -1 Or force_reject Then 'Fail
                    Return False
                Else
                    Return True
                End If
            Else
                Select Case routine
                    Case "post_Duplicate"
                        If Post_Duplicate(True, first_pass) Then Return True
                    Case "Post_database"
                        If Post_database(True, first_pass) Then Return True
                    Case "Post_Email"
                        If Post_Email(True, first_pass) Then Return True
                End Select
                Return False
            End If
        Else
            Select Case routine

                Case "post_Duplicate"
                    If Post_Duplicate(True, first_pass) Then Return True

            End Select
        End If


        Return False
    End Function



    Protected Function Post_Duplicate(show_result As Boolean, first_pass As Boolean) As Boolean

        Dim this_uri As String = "Duplicate"

        Dim data As String = ""

        Dim tmp As String = "Duplicate"


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp = "SUCCESS" Then
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        Else
            reply.Parameters.AddWithValue("post_result", "FAIL")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("url", txtURL.Text)

        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()


        If tmp = "SUCCESS" Then
            Return True
        Else
            Return False
        End If


    End Function

    Protected Sub Repost_Data()

        Dim data As String

        data = "userpass=bw430fhww5"

        data &= "&first_name=" & txtFirstName.Text
        data &= "&last_name=" & txtLastName.Text
        data &= "&email_address=" & txtEmail.Text
        data &= "&phone_num=" & txtPrimaryPhone.Text
        data &= "&address_one=&address_two=&address_three=&city=&state=&postal_code=&auto_leadid=&comments="
        data &= "&vendor_id=" & txtSourceID.Text

        Dim this_uri As Uri
        this_uri = New Uri("http://dialer205.jdmphone.com/estromes.php?" & data)

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        'web_request.ContentLength = data.Length
        'web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        Response.Write("<br>Repost:" & tmp)

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()
        Response.End()
    End Sub

    Protected Sub Repost_Live()
        Dim this_uri As Uri

        this_uri = New Uri("http://live.estomes.com")

        Dim data As String = "vendor_id=2&sub_id=LR_PNIL"

        data &= "&first_name=" & txtFirstName.Text
        data &= "&last_name=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&primary_phone=" & txtPrimaryPhone.Text
        data &= "&citizen=" & txtCitizen.Text
        data &= "&field_of_interest=" & txtInterest.Text
        data &= "&has_hs_or_ged=" & txtGraduated.Text
        ' data &= "&source_id=" & Request("source_id").ToString

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()

    End Sub



    Protected Function Post_database(show_result As Boolean, first_pass As Boolean) As Boolean

        Dim data As String = ""

        Dim tmp As String = ""

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)


        reply.Parameters.AddWithValue("show_result", show_result)
        reply.Parameters.AddWithValue("post_result", "SUCCESS")

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", "N/A")
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_database")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf(Chr(34) & "IsValid" & Chr(34) & ":true") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function



    Protected Function Post_Email(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri

        Dim data As String

        'http://submission.lvlivefeed2.com/c2c_dpost.php?from=<email>&first_name=<firstname>&last_name=<lastname>&zip=<zip>&ipaddress=<ipaddress>&sitename=<site>&listname=<listname>


        data = "listname=estomes" '&CampaignId=eStomes
        data &= "&from=" & txtEmail.Text
        data &= "&first_name=" & txtFirstName.Text
        data &= "&last_name=" & txtLastName.Text
        data &= "&zip=" & txtZip.Text
        data &= "&ipaddress=" & txtIP.Text
        data &= "&sitename=" & txtURL.Text

        this_uri = New Uri("http://submission.lvlivefeed2.com/c2c_dpost.php?" & data)
        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        'web_request.ContentLength = data.Length
        'web_request.ContentType = "application/x-www-form-urlencoded"
        'Dim writer As New StreamWriter(web_request.GetRequestStream)
        'writer.Write(data.ToString)
        'writer.Close()

        'Response.Write(this_uri.AbsoluteUri & "?" & data)

        Dim tmp As String
        Try
            Dim web_response As HttpWebResponse = web_request.GetResponse()
            Dim reader As New StreamReader(web_response.GetResponseStream())
            tmp = reader.ReadToEnd()
            web_response.Close()
        Catch ex As WebException
            tmp = ex.Response.Headers.ToString
        End Try






        If show_result Then
            'Response.Write(tmp)
            'Response.End()

        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("1") = -1 Then 'Fail" Then
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_Email")
        reply.Parameters.AddWithValue("url", txtURL.Text)

        reply.Parameters.AddWithValue("query_string", Request.Url.Query)

        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("1") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function


End Class


