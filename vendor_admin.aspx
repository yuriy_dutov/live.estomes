﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="vendor_admin.aspx.vb" Inherits="routing_admin" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Button ID="btnAdd" runat="server" Text="Add" />
        <asp:GridView ID="gvRouting" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            CellPadding="4" DataKeyNames="id" DataSourceID="dsRouting" ForeColor="#333333"
            GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:CommandField ShowEditButton="True" />
                <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True"
                    SortExpression="id" />
                <asp:BoundField DataField="vendor_id" HeaderText="Vendor ID" SortExpression="vendor_id" />
                <asp:TemplateField HeaderText="Route To" SortExpression="send_to">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList1" runat="server" SelectedValue='<%# Bind("send_to") %>'>
                            <asp:ListItem Text="(Select)" Value=""></asp:ListItem>
                            <asp:ListItem>Post_database</asp:ListItem>
                            <asp:ListItem>Post_Email</asp:ListItem>
                            <asp:ListItem>Post_leadresell</asp:ListItem>
                            <asp:ListItem>Post_leadresell2</asp:ListItem>
                            <asp:ListItem>Post_leadresell3</asp:ListItem>
                            <asp:ListItem>Post_leadreselljg</asp:ListItem>
                            <asp:ListItem>Post_leadresell4</asp:ListItem>
                            <asp:ListItem>Post_leadresell5</asp:ListItem>
                            <asp:ListItem>post_leadresell_PR</asp:ListItem>
                            <asp:ListItem>post_leadresell_UA</asp:ListItem>
                            <asp:ListItem>Post_leadresellHFAF</asp:ListItem>
                            <asp:ListItem>Post_leadresell_YCA</asp:ListItem>
                            <asp:ListItem>post_leadresell_SPHN</asp:ListItem>
                            <asp:ListItem>post_leadresell_MAU</asp:ListItem>
                            <asp:ListItem>post_leadresell_FSA</asp:ListItem>
                            <asp:ListItem>post_leadresell_CWU</asp:ListItem>
                            <asp:ListItem>Post_leadsresell_YCAR</asp:ListItem>
                            <asp:ListItem>Post_leadsresell_LJSR</asp:ListItem>
                            <asp:ListItem>Post_leadsresell_EDS2</asp:ListItem>
                            <asp:ListItem>post_NW</asp:ListItem>
                            <asp:ListItem>Post_LR_Data</asp:ListItem>
                            <asp:ListItem>Post_ed_soup</asp:ListItem>
                            <asp:ListItem>Post_adspire</asp:ListItem>
                            <asp:ListItem>post_axiom</asp:ListItem>
                            <asp:ListItem>post_1NW</asp:ListItem>
                            <asp:ListItem>post_market</asp:ListItem>
                            <asp:ListItem>post_Accelerex</asp:ListItem>
                            <asp:ListItem>post_edsoup_mob</asp:ListItem>
                            <asp:ListItem>post_Duplicate</asp:ListItem>
                            <asp:ListItem>post_PerfectPitch</asp:ListItem>
                            <asp:ListItem>post_OBWT</asp:ListItem>
                            <asp:ListItem>post_h2h</asp:ListItem>
                            <asp:ListItem>post_SMS_A</asp:ListItem>
                            <asp:ListItem>post_SMS_B</asp:ListItem>
                            <asp:ListItem>post_SMS_MIB</asp:ListItem>
                            <asp:ListItem>post_SMS_HIM</asp:ListItem>
                            <asp:ListItem>post_SMS_FHN</asp:ListItem>
                            <asp:ListItem>post_SMS_UFR</asp:ListItem>
                            <asp:ListItem>Post_ES</asp:ListItem>
                            <asp:ListItem>Post_OAM</asp:ListItem>
                            <asp:ListItem>Post_OAM2</asp:ListItem>
                            <asp:ListItem>post_USN_FFR</asp:ListItem>
                            <asp:ListItem>post_USN_FHN</asp:ListItem>
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("send_to") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Fail To" SortExpression="fail_to">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList2" runat="server" SelectedValue='<%# Bind("fail_to") %>'>
                            <asp:ListItem Text="(Select)" Value=""></asp:ListItem>
                            <asp:ListItem>Post_database</asp:ListItem>
                            <asp:ListItem>Post_Email</asp:ListItem>
                            <asp:ListItem>Post_leadresell</asp:ListItem>
                            <asp:ListItem>Post_leadresell2</asp:ListItem>
                            <asp:ListItem>Post_leadresell3</asp:ListItem>
                            <asp:ListItem>Post_leadreselljg</asp:ListItem>
                            <asp:ListItem>Post_leadresell4</asp:ListItem>
                            <asp:ListItem>Post_leadresell5</asp:ListItem>
                            <asp:ListItem>post_leadresell_PR</asp:ListItem>
                            <asp:ListItem>post_leadresell_UA</asp:ListItem>
                            <asp:ListItem>Post_leadresellHFAF</asp:ListItem>
                            <asp:ListItem>Post_leadresell_YCA</asp:ListItem>
                            <asp:ListItem>post_leadresell_SPHN</asp:ListItem>
                            <asp:ListItem>post_leadresell_MAU</asp:ListItem>
                            <asp:ListItem>post_leadresell_FSA</asp:ListItem>
                            <asp:ListItem>post_leadresell_CWU</asp:ListItem>
                            <asp:ListItem>Post_leadsresell_YCAR</asp:ListItem>
                            <asp:ListItem>Post_leadsresell_LJSR</asp:ListItem>
                            <asp:ListItem>Post_leadsresell_EDS2</asp:ListItem>
                            <asp:ListItem>post_NW</asp:ListItem>
                            <asp:ListItem>Post_LR_Data</asp:ListItem>
                            <asp:ListItem>Post_ed_soup</asp:ListItem>
                            <asp:ListItem>Post_adspire</asp:ListItem>
                            <asp:ListItem>post_axiom</asp:ListItem>
                            <asp:ListItem>post_axiom</asp:ListItem>
                            <asp:ListItem>post_1NW</asp:ListItem>
                            <asp:ListItem>post_market</asp:ListItem>
                            <asp:ListItem>post_Accelerex</asp:ListItem>
                            <asp:ListItem>post_edsoup_mob</asp:ListItem>
                            <asp:ListItem>post_Duplicate</asp:ListItem>
                            <asp:ListItem>post_PerfectPitch</asp:ListItem>
                            <asp:ListItem>post_OBWT</asp:ListItem>
                            <asp:ListItem>post_h2h</asp:ListItem>
                            <asp:ListItem>post_SMS_A</asp:ListItem>
                            <asp:ListItem>post_SMS_B</asp:ListItem>
                            <asp:ListItem>post_SMS_MIB</asp:ListItem>
                            <asp:ListItem>post_SMS_HIM</asp:ListItem>
                            <asp:ListItem>post_SMS_FHN</asp:ListItem>
                            <asp:ListItem>post_SMS_UFR</asp:ListItem>
                            <asp:ListItem>Post_ES</asp:ListItem>
                            <asp:ListItem>Post_OAM</asp:ListItem>
                            <asp:ListItem>Post_OAM2</asp:ListItem>
                            <asp:ListItem>post_USN_FFR</asp:ListItem>
                            <asp:ListItem>post_USN_FHN</asp:ListItem>
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("fail_to") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="prefix" SortExpression="prefix" HeaderText="Prefix" />
                <asp:CommandField ShowDeleteButton="True" />
            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>
        <asp:SqlDataSource ID="dsRouting" runat="server" ConnectionString="<%$ ConnectionStrings:EstomesConnectionString %>"
            DeleteCommand="DELETE FROM [Vendor_Routing] WHERE [id] = @id" InsertCommand="INSERT INTO [Vendor_Routing] ([vendor_id], [send_to], [fail_to], [prefix]) VALUES (@vendor_id, @send_to, @fail_to, @prefix)"
            SelectCommand="SELECT * FROM [Vendor_Routing]" UpdateCommand="UPDATE [Vendor_Routing] SET [vendor_id] = @vendor_id, [send_to] = @send_to, [fail_to] = @fail_to, [prefix] = @prefix WHERE [id] = @id">
            <DeleteParameters>
                <asp:Parameter Name="id" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="vendor_id" Type="String" />
                <asp:Parameter Name="send_to" Type="String" />
                <asp:Parameter Name="fail_to" Type="String" />
                <asp:Parameter Name="prefix" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="vendor_id" Type="String" />
                <asp:Parameter Name="send_to" Type="String" />
                <asp:Parameter Name="fail_to" Type="String" />
                <asp:Parameter Name="prefix" Type="String" />
                <asp:Parameter Name="id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
