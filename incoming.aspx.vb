﻿Imports System.Data.SqlClient
Imports System.Net
Imports System.IO
Imports System.Data

Partial Class incoming
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'first_name
        'last_name
        'email
        'zip
        'DateTime
        'IP
        'optin

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SchoolConnectionString").ConnectionString)
        cn.Open()


        Dim reply2 As New SqlDataAdapter("select count(*) from vicidial_list with (nolock)  where phone_number = '" & Request("number1").ToString & "'", cn)
        Dim dupe_dt As New DataTable
        reply2.Fill(dupe_dt)
        If dupe_dt.Rows(0).Item(0) > 0 Then
            'This is a number already in the system
            cn.Close()
            Response.Write("REJECT - Duplicate phone number")
            Exit Sub
        End If


        Dim reply As New SqlCommand("insert into vicidial_list (first_name, last_name, email, postal_code, phone_number, entry_date, source_id,owner) values (@first_name, @last_name, @email, @postal_code, @phone_number, GetDate(), @source_id,@owner)", cn)
        reply.Parameters.AddWithValue("first_name", Request("first_name").ToString)
        reply.Parameters.AddWithValue("last_name", Request("last_name").ToString)
        reply.Parameters.AddWithValue("email", Request("email").ToString)
        reply.Parameters.AddWithValue("postal_code", Request("zip").ToString)
        reply.Parameters.AddWithValue("phone_number", Request("number1").ToString)
        reply.Parameters.AddWithValue("entry_date", Request("DateTime").ToString)
        reply.Parameters.AddWithValue("source_id", Request("IP").ToString)
        reply.Parameters.AddWithValue("owner", Request("list_id").ToString)
        reply.ExecuteNonQuery()


        cn.Close()

        'Repost to Five9
        'https://api.five9.com:8443/web2campaign/AddToList?

        Dim this_uri As Uri
        this_uri = New Uri("https://api.five9.com/web2campaign/AddToList")


        'https://api.five9.com:8443/web2campaign/AddToList?F9domain=acme inc&F9list=webleads
        '&number1=9255551212&first_name=john&last_name=smith&F9retResults=1
        '&F9retURL=http://www.five9.com/errorHandling.html

        Dim data As String = ""

        data &= "email=" & Request("email").ToString
        data &= "&first_name=" & Request("first_name").ToString
        data &= "&last_name=" & Request("last_name").ToString
        data &= "&number1=" & Request("number1").ToString
        data &= "&number2=" & Request("number2").ToString
        data &= "&number3=" & Request("number3").ToString
        data &= "&F9list=" & Request("list_id").ToString
        data &= "&city=" & Request("city").ToString
        data &= "&state=" & Request("state").ToString
        data &= "&zip=" & Request("zip").ToString
        data &= "&street=" & Request("street").ToString
        'data &= "&ip=" & Request("ip").ToString
        'data &= "&datetime=" & Request("datetime").ToString
        data &= "&F9domain=LeadRingers"

        'Response.Write(data)
        'Response.End()

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Timeout = 120000
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()


        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()

        Response.Write("SUCCESS")
        Response.End()

        'Response.Write(tmp)



    End Sub
End Class
