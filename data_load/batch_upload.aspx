﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="batch_upload.aspx.vb" Inherits="batch_upload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
   
</head> 
<body>
    <form id="form1" runat="server">
    <% session.timeout=900 %>
    <div class="style1">
        <center style="text-align: center">
        <b>Agent Manager - Manage Batches</b><br /><br />
            <asp:HiddenField ID="hdnUploadedFile" runat="server" />
        
        <asp:Wizard ID="Wizard1" runat="server" ActiveStepIndex="0" BackColor="#EFF3FB" BorderColor="#B5C7DE"
        BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" >
        <NavigationStyle VerticalAlign="Top"  HorizontalAlign="Center" Width="500px" Height="300px" />
        <StepStyle VerticalAlign="Top" CssClass="steppadding"  Font-Size="0.8em" ForeColor="#333333" />
        <NavigationStyle VerticalAlign="Bottom" HorizontalAlign="Right" />
        <HeaderTemplate>
            Step
            <%=Wizard1.ActiveStepIndex + 1%>
            of
            <%=Wizard1.WizardSteps.Count%></HeaderTemplate>
        <HeaderStyle BackColor="AliceBlue" ForeColor="White" />
        <WizardSteps>
            <asp:WizardStep ID="WizardStep1" runat="server" Title="Get Excel File">
                <br />
                Load Excel File:<br />
                <br />
                <asp:FileUpload ID="fupExcelFile" runat="server" />
                <br />
                <br />
                Source: 
                <asp:TextBox ID="txtListID" runat="server"></asp:TextBox>
                or 
                <br />
                <asp:DropDownList ID="DropDownList1" DataSourceID="SqlDataSource1" DataTextField="owner" DataValueField="owner" runat="server">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:SchoolConnectionString %>" 
                    SelectCommand="select distinct owner from vicidial_list where owner is not null order by owner"></asp:SqlDataSource>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStep2" runat="server" Title="Choose Excel Sheet">
                <br />
                Choose Sheet to Load:<br />
                <br />
                <asp:DropDownList ID="ddlExcelSheets" runat="server">
                </asp:DropDownList>
            </asp:WizardStep>
          
            <asp:WizardStep ID="WizardStep4" runat="server" Title="Map Columns">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                Map Columns Between Spreadsheet and Table:<br />
                <asp:GridView ID="gvColumns" runat="server" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField DataField="ColumnName" HeaderText="Excel Sheet Column" />
                        <asp:BoundField DataField="ColumnSize" HeaderText="Size" />
                        <asp:BoundField DataField="DataType" HeaderText="Data Type" />
                        <asp:TemplateField HeaderText="Mapped Column">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlDestinationColumn" runat="server">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <br />
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStep5" runat="server" Title="Upload Data">
                <br />
                Final Check:<br />
                <asp:GridView ID="gvColumnMap" runat="server">
                </asp:GridView>
                <br />
                <asp:Label ID="lblFinalQuery" runat="server" Text="Label"></asp:Label>
                <asp:Button ID="btnUpload" runat="server" Text="Upload" />
                <br />
                <asp:Label ID="lblAlreadyAdded" runat="server" ForeColor="Red"></asp:Label>
            </asp:WizardStep>
        </WizardSteps>
        <SideBarButtonStyle BackColor="#507CD1" Font-Names="Verdana" ForeColor="White"  />
        <NavigationButtonStyle BackColor="White" BorderColor="#507CD1" BorderStyle="Solid"
            BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284E98" />
        <SideBarStyle BackColor="#507CD1" Font-Size="0.9em" VerticalAlign="Top" CssClass="steppadding"  />
        <HeaderStyle BackColor="#284E98" BorderColor="#EFF3FB" BorderStyle="Solid" BorderWidth="2px"
            Font-Bold="True" Font-Size="0.9em" ForeColor="White" HorizontalAlign="Center" />
    </asp:Wizard>
      
        </center>
    </div>
    </form>
</body>
</html>
