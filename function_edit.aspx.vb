﻿Imports System.Data
Imports Common

Partial Class function_edit
    Inherits System.Web.UI.Page

    Protected Sub FormView1_DataBound(sender As Object, e As EventArgs) Handles FormView1.DataBound
        Dim drv As DataRowView = FormView1.DataItem

        Dim full_string As String = drv("post_URL") & "?" & drv("fixed_string")
        If drv("FirstName").ToString <> "" Then
            full_string &= "&" & drv("FirstName") & "=" & "Test"
        End If
        If drv("LastName").ToString <> "" Then
            full_string &= "&" & drv("LastName") & "=" & "Tester"
        End If
        If drv("Email").ToString <> "" Then
            full_string &= "&" & drv("Email") & "=" & "test@test.com"
        End If
        If drv("Phone").ToString <> "" Then
            full_string &= "&" & drv("Phone") & "=" & "7195551212"
        End If
        If drv("Zip").ToString <> "" Then
            full_string &= "&" & drv("Zip") & "=" & "90210"
        End If
        If drv("Address").ToString <> "" Then
            full_string &= "&" & drv("Address") & "=" & "Address"
        End If
        If drv("City").ToString <> "" Then
            full_string &= "&" & drv("City") & "=" & "City"
        End If
        If drv("State").ToString <> "" Then
            full_string &= "&" & drv("State") & "=" & "State"
        End If
        If drv("IP").ToString <> "" Then
            full_string &= "&" & drv("IP") & "=" & "IP"
        End If

        If drv("Citizen").ToString <> "" Then
            full_string &= "&" & drv("Citizen") & "=" & "Citizen"
        End If
        If drv("Graduated").ToString <> "" Then
            full_string &= "&" & drv("Graduated") & "=" & "Graduated"
        End If
        If drv("Interest").ToString <> "" Then
            full_string &= "&" & drv("Interest") & "=" & "Interest"
        End If
        If drv("LeadID").ToString <> "" Then
            full_string &= "&" & drv("LeadID") & "=" & "LeadID"
        End If
        If drv("OptInDate").ToString <> "" Then
            full_string &= "&" & drv("OptInDate") & "=" & "OptInDate"
        End If
        If drv("OptInTime").ToString <> "" Then
            full_string &= "&" & drv("OptInTime") & "=" & "OptInTime"
        End If
        If drv("SourceID").ToString <> "" Then
            full_string &= "&" & drv("SourceID") & "=" & "SourceID"
        End If
        If drv("IP").ToString <> "" Then
            full_string &= "&" & drv("IP") & "=" & "IP"
        End If

        hlSample.NavigateUrl = full_string
        hlSample.Text = full_string
        hlSample.Target = "_blank"

    End Sub
End Class
