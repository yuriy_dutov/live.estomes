﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Copy of Default.aspx.vb" Inherits="_1_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="margin-left: auto; margin-right: auto; width: 900px; padding-left: auto;
    padding-left: auto; margin-top: 0px; padding-top: 0px;">
    <form id="form1" runat="server">
    <div>
        <table style="width: 884px; height: 556px; background-image: url(images/page1_background.jpg);
            padding: 0px; margin: 0px;">
            <tr>
                <td style="width: 570px; height: 57px;">
                    &nbsp;
                </td>
                <td style="width: 260px; height: 57px;">
                    &nbsp;
                </td>
                <td style="width: 54px;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="left" valign="top">
                    <table style="font-family: Arial; font-size: small;">
                        <tr>
                            <td colspan="2" style="color: #b80108; font-size: xx-large;">
                                &nbsp;Speak With an<br />
                                Advisor Today!
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                What is you area of interest?
                                <br />
                                <asp:DropDownList ID="txtInterest" Width="100%" runat="server">
                                    <asp:ListItem Text="(Select)" Value=""></asp:ListItem>
                                    <asp:ListItem>Psychology And Counseling</asp:ListItem>
                                    <asp:ListItem>Healthcare: Other/General</asp:ListItem>
                                    <asp:ListItem>Healthcare: Medical Assisting And Administration</asp:ListItem>
                                    <asp:ListItem>Culinary</asp:ListItem>
                                    <asp:ListItem>Business</asp:ListItem>
                                    <asp:ListItem>Visual Arts And Design</asp:ListItem>
                                    <asp:ListItem>Information Technology</asp:ListItem>
                                    <asp:ListItem>Healthcare: Nursing</asp:ListItem>
                                    <asp:ListItem>Education And Teaching</asp:ListItem>
                                    <asp:ListItem>Criminal Justice</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                First Name:
                            </td>
                            <td>
                                Last Name:
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtFirstName" Width="120" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtLastName" Width="120" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Phone:
                            </td>
                            <td>
                                Email:
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtPrimaryPhone" Width="120" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtEmail" Width="120" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Are you a US Citizen?
                                <br />
                                <asp:DropDownList ID="txtCitizen" Width="100%" runat="server">
                                    <asp:ListItem>(Select)</asp:ListItem>
                                    <asp:ListItem Value="Y">Yes</asp:ListItem>
                                    <asp:ListItem Value="N">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <nobr>Do you have a High School Diploma/GED?</nobr>
                                <br />
                                <asp:DropDownList ID="txtGraduated" Width="100%" runat="server">
                                    <asp:ListItem>(Select)</asp:ListItem>
                                    <asp:ListItem Value="Y">Yes</asp:ListItem>
                                    <asp:ListItem Value="N">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="color: Gray; font-size: x-small;">
                                By submitting my information, I give consent to contact me.
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:ImageButton ID="ImageButton1" ImageUrl="~/1/images/page1_submit.jpg" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
