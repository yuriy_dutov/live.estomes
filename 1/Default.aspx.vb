﻿
Imports System.Net
Imports System.IO
Imports System.Data.SqlClient

Partial Class _1_Default
    Inherits System.Web.UI.Page

    Protected Sub Post_Data()

        'http://api.leadsresell.com/post_leads.aspx?post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=25705&fname=test&lname=test&zip=12354&email=test@test.com&dob=1982-05-03T00:00:00&phone1=1234567890&phone2=1234567890&program=test&edulevel=test&gradyear=test&student=test&ip=23.23.23.23&url=test&regdate=2012-05-03T11:07:48&timezone=test&remoteid=test&sub=te st&is18=test&address=test&city=test&state=NY&gender=M

        Dim this_uri As Uri
        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")

        Dim data As String = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=25705"

        If Request("vendor_id") IsNot Nothing Then
            Select Case Request("vendor_id")
                Case "1" 'Estomes
                    this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
                    data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=25705"
                    data &= "&fname=" & txtFirstName.Text
                    data &= "&lname=" & txtLastName.Text
                    data &= "&email=" & txtEmail.Text
                    data &= "&phone1=" & txtPrimaryPhone.Text
                    data &= "&zip=&dob=&phone2=&program=&edulevel=&gradyear=&student=&ip=&url=&regdate=&timezone=&remoteid=sub=&is18=&address=&city=&state=&gender="
                    data &= "&test=1"
                    'fname=test&lname=test&zip=12354&email=test@test.com&dob=1982-05-03T00:00:00&phone1=1234567890&phone2=1234567890
                    '&program=test&edulevel=test&gradyear=test&student=test&ip=23.23.23.23&url=test&regdate=2012-05-03T11:07:48&timezone=test&remoteid=test&sub=te st&is18=test&address=test&city=test&state=NY&gender=M


                Case "2" 'Tymax 
                    data = "afid=115&afkey=7ac92e12a4330e9f62ab7b0694005161&campaign_id=124&request_type_id=16"
                Case Else
                    data = "afid=167&afkey=3136bc30e8b4be820e16abace7c28f1e&campaign_id=169&request_type_id=16"
            End Select
        Else
            data = "afid=167&afkey=3136bc30e8b4be820e16abace7c28f1e&campaign_id=169&request_type_id=16"
        End If

        data &= "&first_name=" & txtFirstName.Text
        data &= "&last_name=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&primary_phone=" & txtPrimaryPhone.Text
        data &= "&citizen=" '& txtCitizen.Text
        data &= "&field_of_interest=" '& txtInterest.Text
        data &= "&has_hs_or_ged=" '& txtGraduated.Text
        data &= "&source_id=" & "TEST"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        'Response.Write(tmp)

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", "") ' txtCitizen.SelectedValue)
        reply.Parameters.AddWithValue("field_of_interest", "") 'txtInterest.SelectedValue)
        reply.Parameters.AddWithValue("has_hs_or_ged", "") 'txtGraduated.SelectedValue)
        reply.Parameters.AddWithValue("sub_id", "TEST")
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "1")
        End If

        reply.ExecuteNonQuery()
        cn.Close()

        If tmp <> "success" Then
            RegisterStartupScript("Result", "<script>alert('Thank you! An education advisor will contact you shortly.');</script>")
            Repost_Data()
        Else
            RegisterStartupScript("Result", "<script>alert('I am sorry, but the information has not been posted.  Reason: " & tmp & "');</script>")
        End If


    End Sub

    Protected Sub Post_Data_MFP()

        'http://api.leadsresell.com/post_leads.aspx?post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=25705&fname=test&lname=test&zip=12354&email=test@test.com&dob=1982-05-03T00:00:00&phone1=1234567890&phone2=1234567890&program=test&edulevel=test&gradyear=test&student=test&ip=23.23.23.23&url=test&regdate=2012-05-03T11:07:48&timezone=test&remoteid=test&sub=te st&is18=test&address=test&city=test&state=NY&gender=M

        Dim this_uri As Uri
        this_uri = New Uri("http://test-ps.myfootpath.com/v2/transmission.php")

        'this_uri = New Uri("http://ps.myfootpath.com/v2/transmission.php")
        Dim data As String = "afid=167&afkey=3136bc30e8b4be820e16abace7c28f1e&campaign_id=169&request_type_id=16"

        If Request("vendor_id") IsNot Nothing Then
            Select Case Request("vendor_id")
                Case "1" 'Estomes
                    data = "afid=167&afkey=3136bc30e8b4be820e16abace7c28f1e&campaign_id=169&request_type_id=16"
                Case "2" 'Tymax 
                    data = "afid=115&afkey=7ac92e12a4330e9f62ab7b0694005161&campaign_id=124&request_type_id=16"
                Case Else
                    data = "afid=167&afkey=3136bc30e8b4be820e16abace7c28f1e&campaign_id=169&request_type_id=16"
            End Select
        Else
            data = "afid=167&afkey=3136bc30e8b4be820e16abace7c28f1e&campaign_id=169&request_type_id=16"
        End If

        data &= "&first_name=" & txtFirstName.Text
        data &= "&last_name=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&primary_phone=" & txtPrimaryPhone.Text
        data &= "&citizen=" '& txtCitizen.Text
        data &= "&field_of_interest=" '& txtInterest.Text
        data &= "&has_hs_or_ged=" '& txtGraduated.Text
        data &= "&source_id=" & "TEST"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        'Response.Write(tmp)

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", "") ' txtCitizen.SelectedValue)
        reply.Parameters.AddWithValue("field_of_interest", "") 'txtInterest.SelectedValue)
        reply.Parameters.AddWithValue("has_hs_or_ged", "") 'txtGraduated.SelectedValue)
        reply.Parameters.AddWithValue("sub_id", "TEST")
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "1")
        End If

        reply.ExecuteNonQuery()
        cn.Close()

        If tmp <> "success" Then
            RegisterStartupScript("Result", "<script>alert('Thank you! An education advisor will contact you shortly.');</script>")
            Repost_Data()
        Else
            RegisterStartupScript("Result", "<script>alert('I am sorry, but the information has not been posted.  Reason: " & tmp & "');</script>")
        End If


    End Sub

   

    Protected Sub Repost_Data()

        Dim data As String

        data = "userpass=bw430fhww5"

        data &= "&first_name=" & txtFirstName.Text
        data &= "&last_name=" & txtLastName.Text
        data &= "&email_address=" & txtEmail.Text
        data &= "&phone_num=" & txtPrimaryPhone.Text
        data &= "&address_one=&address_two=&address_three=&city=&state=&postal_code=&auto_leadid=&comments="
        data &= "&vendor_id=" & "TEST"

        Dim this_uri As Uri
        this_uri = New Uri("http://dialer205.jdmphone.com/estromes.php?" & data)




        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        'web_request.ContentLength = data.Length
        'web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        Response.Write("<br>Repost:" & tmp)

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", "") 'txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", "") 'txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", "") 'txtGraduated.Text)
        reply.Parameters.AddWithValue("sub_id", "TEST")
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "1")
        End If
        reply.ExecuteNonQuery()
        cn.Close()


    End Sub

    Protected Sub ImageButton1_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Post_Data()
    End Sub
End Class
