﻿
Imports System.Data.OleDb
Imports System.Data
Imports System.Data.SqlClient

Partial Class batch_upload
    Inherits System.Web.UI.Page
    Dim dt As DataTable


    Protected Sub Wizard1_NextButtonClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.WizardNavigationEventArgs) Handles Wizard1.NextButtonClick
        If Wizard1.ActiveStepIndex = 0 Then
            Try
                If IO.File.Exists(Server.MapPath("docs/" & fupExcelFile.FileName)) Then
                    IO.File.Delete(Server.MapPath("docs/" & fupExcelFile.FileName))
                End If
                fupExcelFile.SaveAs(Server.MapPath("docs/" & fupExcelFile.FileName))

            Catch ex As Exception
                Response.Write(ex.Message)
                Response.End()
            End Try
            hdnUploadedFile.Value = fupExcelFile.FileName
            Dim mySheets() As String = GetExcelSheetNames(Server.MapPath("docs/" & fupExcelFile.FileName))
            ddlExcelSheets.Items.Clear()
            If mySheets.Length > 0 Then
                For Each sheet_name As String In mySheets
                    If sheet_name <> "" Then
                        ddlExcelSheets.Items.Add(sheet_name)
                    End If
                Next
            Else
                ddlExcelSheets.Items.Add(mySheets(0))
            End If

        End If
        If Wizard1.ActiveStepIndex = 1 Then
            GetExcelSheetColums(hdnUploadedFile.Value, ddlExcelSheets.SelectedValue)
        End If

        If Wizard1.ActiveStepIndex = 1 Then
            VerifyQuery()
        End If

    End Sub

    Protected Function GetExcelSheetNames(ByVal excelFile As String) As String()

        Dim objConn As OleDbConnection
        Dim dt As System.Data.DataTable

        Try
            ' Connection String. Change the excel file to the file you
            ' will search.
            Dim connString As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & excelFile & ";Extended Properties=Excel 8.0;"
            ' Create connection object by using the preceding connection string.
            objConn = New OleDbConnection(connString)
            ' Open connection with the database.
            objConn.Open()
            ' Get the data table containg the schema guid.
            dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)

            If dt Is Nothing Then
                Return Nothing
            End If

            Dim excelSheets(dt.Rows.Count) As String '= New String(dt.Rows.Count)
            Dim i As Integer = 0

            ' Add the sheet name to the string array.
            For Each row As DataRow In dt.Rows
                excelSheets(i) = row("TABLE_NAME").ToString()
                i += 1
            Next

            'Loop through all of the sheets if you want too...
            For j As Integer = 0 To excelSheets.Length
                ' Query each excel sheet.
            Next
            Return excelSheets
        Catch ex As Exception

            Return Nothing

        Finally

            ' Clean up.

        End Try
        If objConn IsNot Nothing Then

            objConn.Close()
            objConn.Dispose()
        End If
        If dt IsNot Nothing Then
            dt.Dispose()
        End If
    End Function


    Public Sub GetExcelSheetColums(ByVal filename As String, ByVal sheet As String)


        Dim myConn As New OleDbConnection
        Dim XlsConn As String = _
            "Provider=Microsoft.ACE.OLEDB.12.0;Data " & _
            "Source=" & Server.MapPath("docs/" & filename) & ";" & _
            "Extended Properties=Excel 8.0"
        '
        ' Open an ADO connection to the Excel file.
        '
        myConn.ConnectionString = XlsConn
        myConn.Open()



        Dim commandText As String = "Select * From [" + sheet + "]"
        Dim selecter As OleDbCommand = New OleDbCommand(commandText, myConn)
        Dim reader As OleDbDataReader = selecter.ExecuteReader()

        'Get the name and type of the first column
        Dim sheetSchema As DataTable = reader.GetSchemaTable()
        'Dim firstColumnName As String = sheetSchema.Rows(1).Item("ColumnName")


    End Sub

    Protected Sub gvColumns_DataBinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvColumns.DataBinding
        Dim myConn As New SqlClient.SqlConnection
        Dim XlsConn As String = System.Configuration.ConfigurationManager.ConnectionStrings("estomes_dataConnectionString").ConnectionString

        ' Open an ADO connection to the Excel file.
        '
        myConn.ConnectionString = XlsConn
        myConn.Open()


        'Dim dt2 As New DataTable
        'Dim cmd As String = "select top 1 * from dbo.JB_Staging"
        'Dim cmd_reply As New SqlDataAdapter(cmd, myConn)
        'cmd_reply.Fill(dt2)

        Dim commandText As String = "Select top 1 * From JB_Staging"
        Dim selecter As SqlCommand = New SqlCommand(commandText, myConn)
        Dim reader As SqlDataReader = selecter.ExecuteReader()

        'Get the name and type of the first column
        Dim sheetSchema As DataTable = reader.GetSchemaTable()
        Dim dt As New DataTable
        dt.Columns.Add("ColumnName")
        dt.Columns.Add("ColumnDesc")
        Dim dr3 As DataRow = dt.Rows.Add()
        dr3("ColumnName") = "*****(ignore)*****"
        dr3("ColumnDesc") = "*****(ignore)*****"
        Dim firstColumnName As String = sheetSchema.Rows(0).Item("ColumnName")
        For Each dr As DataRow In sheetSchema.Rows

            'If dt2.Columns.Contains(dr("ColumnName").ToString) Then
            Dim dr2 As DataRow
            dr2 = dt.Rows.Add
            dr2("ColumnName") = dr("ColumnName")
            'dr2("ColumnDesc") = dr("ColumnName").ToString & "(" & dr("DataType").ToString & "-" & dr("ColumnSize").ToString & ")"
            dr2("ColumnDesc") = dr("ColumnName") & "(" & dr("DataType").ToString & "-" & dr("ColumnSize").ToString & ")"
            'End If
        Next
        dt.DefaultView.Sort = "ColumnName"
        Session("dt") = dt
    End Sub


    Protected Sub gvColumns_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvColumns.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ddl As DropDownList = e.Row.Cells(3).FindControl("ddlDestinationColumn")

            ddl.DataTextField = "ColumnDesc"
            ddl.DataValueField = "ColumnName"
            ddl.DataSource = Session("dt")
            ddl.DataBind()
            If ddl.Items.Contains(New ListItem(e.Row.Cells(0).Text)) Or ddl.Items.Contains(New ListItem(e.Row.Cells(0).Text.Replace(" ", "_"))) Then
                ddl.SelectedValue = e.Row.Cells(0).Text
            End If

            'Dim map_dt As DataTable = Session("map_dt")
            'For Each dr As DataRow In map_dt.Rows
            '    If e.Row.Cells(0).Text = dr("map_source_field") Then
            '        If ddl.Items.Contains(New ListItem(dr("map_destination_field"))) Then
            '            ddl.SelectedValue = dr("map_destination_field")
            '        End If
            '    End If

            'Next


        End If

    End Sub

    Protected Sub VerifyQuery()
        Dim final_map As String = "<br>"

        Dim i As Integer
        Dim myConn As New OleDbConnection
        Dim XlsConn As String = _
            "Provider=Microsoft.ACE.OLEDB.12.0;Data " & _
            "Source=" & Server.MapPath("docs/" & hdnUploadedFile.Value) & ";" & _
            "Extended Properties=Excel 8.0"
        '
        ' Open an ADO connection to the Excel file.
        '
        myConn.ConnectionString = XlsConn
        myConn.Open()



        Dim commandText As String = "Select * From [" + ddlExcelSheets.SelectedValue + "]"
        Dim selecter As OleDbCommand = New OleDbCommand(commandText, myConn)
        Dim reader As OleDbDataReader = selecter.ExecuteReader()

        'Get the name and type of the first column
        Dim sheetSchema As DataTable = reader.GetSchemaTable()



        'Dim dt_speeds As New DataTable
        'dt_speeds.Columns.Add("Column Name")

        'For Each dr As DataRow In sheetSchema.Rows
        '    Dim dr_speeds As DataRow = dt_speeds.Rows.Add
        '    dr_speeds("Column Name") = dr("ColumnName")
        'Next

        gvColumns.DataSource = sheetSchema 'dt_speeds
        gvColumns.DataBind()


    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Dim myConn As New OleDbConnection
        Dim XlsConn As String = _
            "Provider=Microsoft.ACE.OLEDB.12.0;Data " & _
            "Source=" & Server.MapPath("docs/" & hdnUploadedFile.Value) & ";" & _
            "Extended Properties=Excel 8.0"
        '
        ' Open an ADO connection to the Excel file.
        '
        myConn.ConnectionString = XlsConn
        myConn.Open()

        Dim myConn2 As New SqlConnection
        Dim XlsConn2 As String = System.Configuration.ConfigurationManager.ConnectionStrings("estomes_dataConnectionString").ConnectionString  ' "Provider=sqloledb;Data Source=mmeashcssdb06;Initial Catalog=CSS_BMAT;User ID=svc-web-bmat;Password=Bm@tW3b"

        ' Open an ADO connection to the Excel file.
        '
        myConn2.ConnectionString = XlsConn2
        myConn2.Open()


        Dim agent_id As String = ""

        Dim sql As String = "select "
        For Each dr As GridViewRow In gvColumns.Rows
            Dim ddl As DropDownList = dr.Cells(3).Controls(1)
            If ddl.SelectedValue <> "*****(ignore)*****" Then
                sql &= "[" & dr.Cells(0).Text & "], "
            End If
            If ddl.SelectedValue = "" Then
                agent_id = dr.Cells(0).Text
            End If

        Next
        'response.write(agent_id)
        'response.end()

        sql = Left(sql, Len(sql) - 2) & " from [" & ddlExcelSheets.SelectedValue & "]"




        Dim da As OleDbDataAdapter = New OleDbDataAdapter(sql, myConn)
        Dim excel_dt As New DataTable
        da.Fill(excel_dt)

        'Response.Write(sql)
        'Response.End()

        sql = "insert into JB_Staging ("
        For Each dr As GridViewRow In gvColumns.Rows
            Dim ddl As DropDownList = dr.Cells(3).Controls(1)
            If ddl.SelectedValue <> "*****(ignore)*****" Then
                sql &= "[" & ddl.SelectedValue & "], "
            End If
        Next

        sql &= "added) values ("



        Dim already_added As String = ""
        For Each dr As DataRow In excel_dt.Rows
            Dim insertitems As String = ""
            For x As Integer = 0 To excel_dt.Columns.Count - 1
                insertitems &= "'" & dr.Item(x).ToString.Replace("'", "''") & "',"
            Next

            insertitems &= "GetDate())"
           
         
            'If ok_to_add Then
            Dim reply2 As New SqlCommand 

            reply2 = New SqlCommand(sql & insertitems, myConn2)
            Try
                reply2.ExecuteNonQuery()
            Catch ex As Exception
                response.write(ex.message)
            End Try

            'End If
        Next
        Dim reply3 As New SqlCommand
        reply3 = New SqlCommand("JBExport", myConn2)
        reply3.CommandTimeout = 240
        Try
            reply3.ExecuteNonQuery()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        lblAlreadyAdded.Text = "(Load Complete)"

        If IO.File.Exists(Server.MapPath("JB_Dupes_out.csv")) Then
            Dim hl As New HyperLink
            hl.NavigateUrl = "JB_Dupes_out.csv"
            hl.Text = "JB_Dupes_out.csv"
            Panel1.Controls.Add(hl)
        End If
        If IO.File.Exists(Server.MapPath("Non_JB_Dupes_out.csv")) Then
            Dim hl As New HyperLink
            hl.NavigateUrl = "Non_JB_Dupes_out.csv"
            hl.Text = "Non_JB_Dupes_out.csv"
            Panel1.Controls.Add(hl)
        End If



        myConn.Close()
        myConn2.Close()

    End Sub


End Class

