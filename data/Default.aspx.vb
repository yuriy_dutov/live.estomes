﻿Imports System.Data.SqlClient

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub tnUpload_Click(sender As Object, e As System.EventArgs) Handles tnUpload.Click
        FileUpload1.SaveAs(Server.MapPath("docs/" & FileUpload1.FileName))
        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("estomes_dataConnectionString").ConnectionString)
        cn.Open()

        'response.write("truncate table JB_Staging; bulk insert estomes_data.dbo.JB_Staging FROM '" & Server.MapPath("docs/" & FileUpload1.FileName) & "' with ( formatfile = 'C:\WWWRoot\posting.leadringers.com\data\JB.fmt', FIELDTERMINATOR =',', ROWTERMINATOR ='\n'  )")
        'response.end()
        Try
            Dim reply As New SqlCommand("truncate table JB_Staging; bulk insert estomes_data.dbo.JB_Staging FROM '" & Server.MapPath("docs/" & FileUpload1.FileName) & "' with ( formatfile = 'C:\WWWRoot\posting.leadringers.com\data\JB.fmt', FIELDTERMINATOR =',', ROWTERMINATOR ='\n'  )", cn)
            reply.ExecuteNonQuery()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try



      
        IO.File.Delete(Server.MapPath("JB_Dupes_out.csv"))
        IO.File.Delete(Server.MapPath("Non_JB_Dupes_out.csv"))
        Dim reply2 As New SqlCommand("JBExport", cn)
        reply2.CommandType = Data.CommandType.StoredProcedure
        reply2.CommandTimeout = 120
        reply2.ExecuteNonQuery()
        HyperLink1.Text = "JB_Dupes_out.csv"
        HyperLink1.NavigateUrl = "JB_Dupes_out.csv"
        HyperLink2.Text = "Non_JB_Dupes_out.csv"
        HyperLink2.NavigateUrl = "Non_JB_Dupes_out.csv"

       

        HyperLink1.Text = ""



    End Sub
End Class
