﻿Imports System.Net
Imports System.IO
Imports System.Data.SqlClient
Imports System.Data
Imports Common

Partial Class _Default
    Inherits System.Web.UI.Page

    Dim isDupe As Boolean = False

    
    Protected Function try_submit(routine As String, Optional isDupe As Boolean = False, Optional first_pass As Boolean = False) As Boolean
        If Not isDupe Then
            Select Case routine
                Case "Post_leadresell"
                    If Post_leadresell(True, first_pass) Then Return True
                Case "Post_leadresell2"
                    If Post_leadresell2(True, first_pass) Then Return True
                Case "Post_leadresell3"
                    If Post_leadresell3(True, first_pass) Then Return True
                Case "Post_leadresell4"
                    If Post_leadresell4(True, first_pass) Then Return True
                Case "Post_LR_Data"
                    If Post_LR_Data(True, first_pass) Then Return True
                Case "Post_ed_soup"
                    If post_ed_soup(True, first_pass) Then Return True
                Case "post_PerfectPitch"
                    If post_PerfectPitch(True, first_pass) Then Return True
                Case "Post_adspire"
                    If post_adspire(True, first_pass) Then Return True
                Case "post_axiom"
                    If post_axiom(True, first_pass) Then Return True
                Case "post_1NW"
                    If post_1NW(True, first_pass) Then Return True
                Case "post_market"
                    If post_market(True, first_pass) Then Return True
                Case "post_Accelerex"
                    If post_Accelerex(True, first_pass) Then Return True

                Case "post_Duplicate"
                    If Post_Duplicate(True, first_pass) Then Return True

                Case "post_edsoup_mob"
                    If post_edsoup_mob(True, first_pass) Then Return True
                Case "post_OBWT"
                    If post_OBWT(True, first_pass) Then Return True

            End Select
        Else
            Select Case routine

                Case "post_Duplicate"
                    If Post_Duplicate(True, first_pass) Then Return True

            End Select
        End If
        Return False
    End Function

    Protected Sub Post_Data()
        Exit Sub

        Dim centerID As String = ""
        If Request("centerID") IsNot Nothing Then
            If Request("centerID") = "2" Then
                'Repost_Data()
                'Exit Sub
            End If
        End If

        Dim this_uri As Uri

        this_uri = New Uri("http://ps.myfootpath.com/v2/transmission.php")

        Dim data As String
        If Request("vendor_id") IsNot Nothing Then
            Select Case Request("vendor_id")
                Case "1" 'Estomes
                    data = "afid=167&afkey=3136bc30e8b4be820e16abace7c28f1e&campaign_id=169&request_type_id=16"
                Case "2" 'Tymax 
                    data = "afid=115&afkey=7ac92e12a4330e9f62ab7b0694005161&campaign_id=124&request_type_id=16"
                Case Else
                    data = "afid=167&afkey=3136bc30e8b4be820e16abace7c28f1e&campaign_id=169&request_type_id=16"
            End Select
        Else
            data = "afid=167&afkey=3136bc30e8b4be820e16abace7c28f1e&campaign_id=169&request_type_id=16"
        End If

        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&primary_phone=" & txtPrimaryPhone.Value
        data &= "&citizen=" & txtCitizen.Value
        data &= "&field_of_interest=" & txtInterest.Value
        data &= "&has_hs_or_ged=" & txtGraduated.Value
        data &= "&source_id=" & txtSourceID.Value

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        ' Response.Write(tmp)



        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)


        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp <> "success" Then
            ' Repost_Data()
        End If

        Response.End()
    End Sub


    Protected Function Post_LR_Data(show_result As Boolean, first_pass As Boolean) As Boolean

        Dim this_uri As Uri
        this_uri = New Uri("http://live.estomes.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        data = "list_id=" & txtSourceID.Value

        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&number1=" & txtPrimaryPhone.Value
        data &= "&number2=&number3=&street=&city=&state=&zip=&ip=&datetime="

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        'cn.Open()
        'Dim reply As New SqlCommand("AddFSO", cn)
        'reply.CommandType = System.Data.CommandType.StoredProcedure
        'reply.Parameters.AddWithValue("first_name", txtFirstName.value)
        'reply.Parameters.AddWithValue("last_name", txtLastName.value)
        'reply.Parameters.AddWithValue("email", txtEmail.value)
        'reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.value)
        'reply.Parameters.AddWithValue("field_of_interest", txtInterest.value)
        'reply.Parameters.AddWithValue("zip", txtzip.value)
        'reply.Parameters.AddWithValue("submitted_data", data)
        'reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        'reply.Parameters.AddWithValue("result", tmp)
        'reply.ExecuteNonQuery()
        'cn.Close()

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp = "SUCCESS" Then
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        Else
            reply.Parameters.AddWithValue("post_result", "FAIL")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_LR_Data")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()


        If tmp = "SUCCESS" Then
            Return True
        Else
            Return False
        End If


    End Function


    Protected Function Post_Duplicate(show_result As Boolean, first_pass As Boolean) As Boolean

        Dim this_uri As String = "Duplicate"

        Dim data As String = ""

        Dim tmp As String = "Duplicate"


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp = "SUCCESS" Then
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        Else
            reply.Parameters.AddWithValue("post_result", "FAIL")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("url", txtURL.Value)

        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()


        If tmp = "SUCCESS" Then
            Return True
        Else
            Return False
        End If


    End Function


    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Dim num_loaded As Integer = 0
        txtSourceID.Value = txtSubID.Text
        FileUpload1.SaveAs(Server.MapPath("docs/" & FileUpload1.FileName))
        Dim objStreamReader As StreamReader
        objStreamReader = File.OpenText(Server.MapPath("docs/" & FileUpload1.FileName))
        'Dim contents As String = objStreamReader.ReadLine()

        Dim someString As String

        While objStreamReader.Peek() <> -1
            someString = objStreamReader.ReadLine()
            '... do whatever else you need to do ...
            Dim line_info() As String = someString.Split(",")
            If line_info.Length = 13 Then
                txtFirstName.Value = line_info(0)
                txtLastName.Value = line_info(1)
                txtEmail.Value = line_info(2)
                txtAddress.Value = line_info(3)
                txtCity.Value = line_info(4)
                txtState.Value = line_info(5)
                txtZip.Value = line_info(6)
                txtPrimaryPhone.Value = line_info(7)
                'txtFirstNa.Value = line_info(8)
                'txtFirstName.Value = line_info(9)
                txtOptInDate.Value = line_info(10)
                txtOptInTime.Value = line_info(11)
                txtURL.Value = txtSubID.Text
                try_submit(DropDownList1.SelectedValue, False, False)
                num_loaded += 1
            End If


        End While
        Response.Write("Loaded: " & num_loaded)
    End Sub

    Protected Sub Repost_Data()

        Dim data As String

        data = "userpass=bw430fhww5"

        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email_address=" & txtEmail.Value
        data &= "&phone_num=" & txtPrimaryPhone.Value
        data &= "&address_one=&address_two=&address_three=&city=&state=&postal_code=&auto_leadid=&comments="
        data &= "&vendor_id=" & txtSourceID.Value

        Dim this_uri As Uri
        this_uri = New Uri("http://dialer205.jdmphone.com/estromes.php?" & data)




        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        'web_request.ContentLength = data.Length
        'web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        Response.Write("<br>Repost:" & tmp)

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()
        Response.End()
    End Sub


    Protected Function Post_leadresell(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=25705"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&ip=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&gender="
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function Post_leadresell2(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=26834"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&ip=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&gender="
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell2")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function Post_leadresell3(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=28932"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&ip=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&gender="
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell3")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function


    Protected Function Post_leadresell4(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=25759"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&ip=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&gender="
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell3")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function



    Protected Function post_adspire(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://www.adspirenetwork.com/api/api_edu.php")

        Dim data As String

        data = "method=pushLead&apiKey=I1oHO7[CVt:u}OXkJ=5tttq59d0BtEy!{QtLyj5&buyerId=3327&campaignId=107&eduLevel=&bestTimeCall=&startTime=&gender=&employer="
        'data &= "&campaign_name=" & txtSourceID.value
        data &= "&firstName=" & txtFirstName.Value
        data &= "&lastName=" & txtLastName.Value
        data &= "&emailAddress=" & txtEmail.Value
        data &= "&phoneNumberHome=" & txtPrimaryPhone.Value
        data &= "&zipCode=" & txtZip.Value
        data &= "&dob=&phoneNumberAlternative="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        'data &= "&url=" & txtSourceID.value
        data &= "&ipAddress=" & txtIP.Value
        data &= "&eduProgram=" & txtInterest.Value
        data &= "&graduationYear=" & txtGraduated.Value
        'data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Success") = -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_adspire")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Success") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function
    Protected Function post_Accelerex(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("https://app.leadconduit.com/v2/PostLeadAction")

        Dim data As String

        data = "xxCampaignId=0530xohqo&xxAccountId=053ekni7p"
        data &= "&lead_source_id=" & txtSourceID.Value
        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone_home=" & txtPrimaryPhone.Value
        data &= "&ip_address=" & txtIP.Value
        data &= "&address1=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&postal_code=" & txtZip.Value
        data &= "&placement=accx14&subid=" & txtSourceID.Value


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("<result>success</result>") = -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_Accelerex")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("<result>success</result>") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function



    Protected Function post_axiom(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://feeds.leads-junction.com/leadpost.php ")

        Dim data As String

        data = "clientid=409&campaigncode=8"
        data &= "&campaign_name=" & txtSourceID.Value
        data &= "&firstname=" & txtFirstName.Value
        data &= "&lastname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&zip=" & txtZip.Value
        data &= "&homephone=" & txtPrimaryPhone.Value
        data &= "&ip=" & txtIP.Value
        data &= "&url=" & txtSourceID.Value
        data &= "&country=&dateofbirth=&workphone=&mobilephone=&gender=&language=&comments="




        data &= "&program=" & txtInterest.Value
        data &= "&gradyear=" & txtGraduated.Value
        data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Accepted") = -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_axiom")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Accepted") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function
    Protected Function post_1NW(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("https://api.five9.com/web2campaign/AddToList")

        Dim data As String

        data = "F9domain=1NWContact&F9list=EDUAD.NET_livefeed_112612"
        'data &= "&campaign_name=" & txtSourceID.value
        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        'data &= "&email=" & txtEmail.value
        data &= "&number1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&number2="
        data &= "&street=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        'data &= "&url=" & txtSourceID.value
        'data &= "&ip_address=" & txtIP.value
        'data &= "&program=" & txtInterest.value
        'data &= "&gradyear=" & txtGraduated.value
        'data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")

        ' campaign_name,url,ip_address,gradyear,email,program,regdate,dob

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim check_string As String = "name=" & Chr(34) & "F9errCode" & Chr(34) & " value=" & Chr(34) & "0" & Chr(34) & " "

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf(check_string) = -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_1NW")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf(check_string) = -1 Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function

    Protected Function post_market(show_result As Boolean, first_pass As Boolean) As Boolean

        'https://restapilite.safesoftsolutions.com/leads/insert?
        'phone_number=4242564037&auth_token=cf7a69dcb3ebd0a67cca6706e50127b5b54455b7&list_id=26701
        '&form_data[field_29723]=lawanda&form_data[field_29725]=wells&form_data[field_29727]=332w Regentst.4
        '&form_data[field_29731]=Inglewood&form_data[field_29733]=CA&form_data[field_29735]=90301&form_data[field_29737]=4242564030
        '&form_data[field_29743]=lawandawellsluvconner26@gmail.com&form_data[field_30547]=CEOcheck_dup=1&hopper=1
        '&form_data[field_30543]=98.126.131.89&form_data[field_30545]=08/7/2012 13:59

        Dim this_uri As Uri
        this_uri = New Uri("https://restapilite.safesoftsolutions.com/leads/insert")

        Dim data As String

        data = "&auth_token=cf7a69dcb3ebd0a67cca6706e50127b5b54455b7&list_id=26701"
        data &= "&form_data[field_29723]=" & txtFirstName.Value
        data &= "&form_data[field_29725]=" & txtLastName.Value
        data &= "&form_data[field_29743]=" & txtEmail.Value
        data &= "&phone_number=" & txtPrimaryPhone.Value
        data &= "&form_data[field_29737]=" & txtPrimaryPhone.Value
        data &= "&form_data[field_29735]=" & txtZip.Value
        data &= "&form_data[field_29727]=" & txtAddress.Value
        data &= "&form_data[field_29731]=" & txtCity.Value
        data &= "&form_data[field_29733]=" & txtState.Value
        data &= "&form_data[field_30543]=" & txtIP.Value
        data &= "&form_data[field_30545]=" & Format(Now, "yyyy-mm-dd hh:mm_ss")

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)

        If tmp.IndexOf("success") = -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_market")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("success") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function

    Protected Function post_edsoup_mob(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://www.edsoup.com/dhein/transfer.php")

        Dim data As String

        data = "key=130e377e7808b4d36ee5c2d570d66b4c&source_id=209"
        data &= "&campaign_name=" & txtSourceID.Value
        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&number1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&number2="
        data &= "&street=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&url=" & txtURL.Value
        data &= "&ip_address=" & txtIP.Value
        data &= "&program=" & txtInterest.Value
        data &= "&gradyear=" & txtGraduated.Value
        data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")
        data &= "&referringsite=FinishSchoolOnline.com"
        data &= "&leadid=" & txtLeadID.Value

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If Left(tmp, 4) = "fail" Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_edsoup_mob")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        reply.Parameters.AddWithValue("lead_id", txtLeadID.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If Left(tmp, 4) = "fail" Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function


    Protected Function post_PerfectPitch(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://www.edsoup.com/dhein/transfer.php")

        Dim data As String

        data = "key=130e377e7808b4d36ee5c2d570d66b4c&source_id=221"
        data &= "&campaign_name=" & txtSourceID.Value
        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&number1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&number2="
        data &= "&street=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&url=" & txtSourceID.Value
        data &= "&ip_address=" & txtIP.Value
        data &= "&program=" & txtInterest.Value
        data &= "&gradyear=" & txtGraduated.Value
        data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")
        data &= "&referringsite=123freetravel.com/HBB/"

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If Left(tmp, 4) = "fail" Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_PerfectPitch")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If Left(tmp, 4) = "fail" Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function


    Protected Function post_OBWT(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://www.edsoup.com/dhein/transfer.php")

        Dim data As String

        data = "key=130e377e7808b4d36ee5c2d570d66b4c&source_id=226"
        data &= "&campaign_name=" & txtSourceID.Value
        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&number1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&number2="
        data &= "&street=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&url=" & txtSourceID.Value
        data &= "&ip_address=" & txtIP.Value
        data &= "&program=" & txtInterest.Value
        data &= "&gradyear=" & txtGraduated.Value
        data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")
        data &= "&referringsite=123freetravel.com/HBB/"

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If Left(tmp, 4) = "fail" Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_OBWT")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If Left(tmp, 4) = "fail" Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function




    Protected Function post_ed_soup(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://www.edsoup.com/dhein/transfer.php")

        Dim data As String

        data = "key=130e377e7808b4d36ee5c2d570d66b4c&source_id=196"
        data &= "&campaign_name=" & txtSourceID.Value
        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&number1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&number2="
        data &= "&street=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&url=" & txtSourceID.Value
        data &= "&ip_address=" & txtIP.Value
        data &= "&program=" & txtInterest.Value
        data &= "&gradyear=" & txtGraduated.Value
        data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")
        data &= "&referringsite=123freetravel.com/HBB/"

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If Left(tmp, 4) = "fail" Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_ed_soup")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If Left(tmp, 4) = "fail" Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function
    Protected Sub Repost_Live()
        Dim this_uri As Uri

        this_uri = New Uri("http://live.estomes.com")



        Dim data As String = "vendor_id=2&sub_id=LR_PNIL"

        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&primary_phone=" & txtPrimaryPhone.Value
        data &= "&citizen=" & txtCitizen.Value
        data &= "&field_of_interest=" & txtInterest.Value
        data &= "&has_hs_or_ged=" & txtGraduated.Value
        ' data &= "&source_id=" & Request("source_id").ToString


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()

    End Sub



    Protected Sub Page_PreRenderComplete(sender As Object, e As System.EventArgs) Handles Me.PreRenderComplete

    End Sub
End Class
