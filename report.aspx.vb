﻿Imports System.Data

Partial Class report
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            StartDate.Text = Month(Today) & "/1/" & Year(Today)
            EndDate.Text = Month(Today) & "/" & Day(Today) & "/" & Year(Today)

            Dim dt As DataTable = Common.GetSchoolTable("SELECT distinct sub_id FROM  MyFootpathAffiliates with (nolock)  WHERE (submitted_date BETWEEN '" & StartDate.Text & "' AND '" & EndDate.Text & " 11:59pm' ) order by sub_id")
            ddlSource.DataSource = dt
            ddlSource.DataBind()

        End If
        
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
        'Confirms that an HtmlForm control is rendered for the specified ASP.NET   
        'server control at run time.  
    End Sub


    Protected Sub GridView1_DataBound() 'ByVal sender As Object, ByVal e As System.EventArgs) ' Handles GridView1.DataBound

        Response.Clear()
        Response.AddHeader("content-disposition", String.Format("attachment;filename=excel_report.xls"))
        Response.Charset = ""

        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        'Response.ContentType = "application/vnd.xls"
        Response.ContentType = "application/vnd.ms-excel"
        Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        GridView1.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.End()
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        dsExportList.DataBind()
        GridView1.DataBind()
        GridView1_DataBound()
    End Sub

    Protected Sub btnGo_Click(sender As Object, e As System.EventArgs) Handles btnGo.Click
        hdnStartDate.Value = StartDate.Text
        hdnEndDate.Value = EndDate.Text


        If ddlSource.SelectedValue <> "" Then
            dsExportList.FilterExpression = "Sub_id = '" & ddlSource.SelectedValue & "'"
        Else
            dsExportList.FilterExpression = ""
        End If

        dsExportList.DataBind()
        GridView1.DataBind()
    End Sub
End Class
