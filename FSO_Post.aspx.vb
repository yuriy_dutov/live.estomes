﻿Imports System.Net
Imports System.IO
Imports System.Data.SqlClient

Partial Class FSO_Post
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request("first_name") IsNot Nothing Then
                If Request("first_name") IsNot Nothing Then
                    txtFirstName.Text = Request("first_name")
                End If
                If Request("last_name") IsNot Nothing Then
                    txtLastName.Text = Request("last_name")
                End If
                If Request("email") IsNot Nothing Then
                    txtEmail.Text = Request("email")
                End If
                If Request("primary_phone") IsNot Nothing Then
                    txtPrimaryPhone.Text = Request("primary_phone")
                End If
                If Request("field_of_interest") IsNot Nothing Then
                    txtInterest.Text = Request("field_of_interest")
                End If
                If Request("postal") IsNot Nothing Then
                    txtZip.Text = Request("postal")
                End If
                If Request("test") IsNot Nothing Then
                    ddlTest.SelectedValue = "Yes"
                Else
                    ddlTest.SelectedValue = "No"
                End If

                Post_Data()
            End If
        End If
    End Sub

    Protected Sub Post_Data()


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddFSO", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)
        reply.ExecuteNonQuery()
        cn.Close()

        Response.Redirect(Request("redirect_url") & "&first_name=" & txtFirstName.Text & "&last_name=" & txtLastName.Text & "&phone=" & txtPrimaryPhone.Text & "&email=" & txtEmail.Text & "&zipCode=" & txtZip.Text)
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Post_Data()

    End Sub

End Class

