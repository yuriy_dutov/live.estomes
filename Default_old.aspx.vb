﻿Option Compare Text

Imports System.Net
Imports System.IO
Imports System.Data.SqlClient
Imports System.Data
Imports Common

Partial Class _Default
    Inherits System.Web.UI.Page

    Dim isDupe As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request("first_name") IsNot Nothing Then
                If Request("first_name") IsNot Nothing Then
                    txtFirstName.Text = Request("first_name")
                End If
                If Request("last_name") IsNot Nothing Then
                    txtLastName.Text = Request("last_name")
                End If
                If Request("email") IsNot Nothing Then
                    txtEmail.Text = Request("email")
                End If
                If Request("primary_phone") IsNot Nothing Then
                    txtPrimaryPhone.Text = Request("primary_phone")
                End If
                If Request("citizen") IsNot Nothing Then
                    txtCitizen.Text = Request("citizen")
                Else
                    txtCitizen.Text = "Y"
                End If
                If Request("field_of_interest") IsNot Nothing Then
                    txtInterest.Text = Request("field_of_interest")
                End If
                If Request("has_hs_or_ged") IsNot Nothing Then
                    txtGraduated.Text = Request("has_hs_or_ged")
                End If
                If Request("sub_id") IsNot Nothing Then
                    txtSourceID.Text = Request("sub_id")
                End If

                If Request("address") IsNot Nothing Then
                    txtAddress.Text = Request("address")
                End If
                If Request("city") IsNot Nothing Then
                    txtCity.Text = Request("city")
                End If
                If Request("state") IsNot Nothing Then
                    txtState.Text = Request("state")
                End If
                If Request("zip") IsNot Nothing Then
                    txtZip.Text = Request("zip")
                End If
                If Request("optindate") IsNot Nothing Then
                    txtOptInDate.Text = Request("optindate")
                End If


                If Request("optintime") IsNot Nothing Then
                    txtOptInTime.Text = Request("optintime")
                End If

                If Request("lead_id") IsNot Nothing Then
                    txtLeadID.Text = Request("lead_id")
                End If

                If Request("ipaddress") IsNot Nothing Then
                    txtIP.Text = Request("ipaddress")
                End If

                If Request("url") IsNot Nothing Then
                    txtURL.Text = Request("url")
                End If

                If Request("test") IsNot Nothing Then
                    ddlTest.SelectedValue = "Yes"
                Else
                    ddlTest.SelectedValue = "No"
                End If


                Dim check_dt As New DataTable


                Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("EstomesConnectionString").ConnectionString)
                cn.Open()

                If Request("sub_id") = "PNIL" Then

                    Dim reply3 As New SqlDataAdapter("select * from unique_email_phone with (nolock) where (phone = @phone or email = @email) and tablename in ('OY2','NIP') ", cn)
                    reply3.SelectCommand.Parameters.AddWithValue("phone", txtPrimaryPhone.Text)
                    reply3.SelectCommand.Parameters.AddWithValue("email", txtEmail.Text)
                    reply3.Fill(check_dt)

                    If check_dt.Rows.Count > 0 Then
                        isDupe = True

                    End If

                    Dim ins As New SqlCommand("insert into unique_email_phone (phone, email, tablename, opt_in_date, uploaded) select @phone, @email, 'NIP', GetDate(), Getdate()", cn)
                    ins.Parameters.AddWithValue("phone", txtPrimaryPhone.Text)
                    ins.Parameters.AddWithValue("email", txtEmail.Text)
                    ins.ExecuteNonQuery()

                End If

                If Request("sub_id") = "CMS" Then

                    Dim reply3 As New SqlDataAdapter("select * from cms_email_phone with (nolock) where (phone = @phone or email = @email)", cn)
                    reply3.SelectCommand.Parameters.AddWithValue("phone", txtPrimaryPhone.Text)
                    reply3.SelectCommand.Parameters.AddWithValue("email", txtEmail.Text)
                    reply3.Fill(check_dt)

                    If check_dt.Rows.Count > 0 Then
                        isDupe = True

                    End If

                    Dim ins As New SqlCommand("insert into cms_email_phone (phone, email, tablename, opt_in_date, uploaded) select @phone, @email, 'NA', GetDate(), Getdate()", cn)
                    ins.Parameters.AddWithValue("phone", txtPrimaryPhone.Text)
                    ins.Parameters.AddWithValue("email", txtEmail.Text)
                    ins.ExecuteNonQuery()

                End If


                If Request("vendor_id") IsNot Nothing Then

                    Dim dt As New DataTable

                    Dim reply As New SqlDataAdapter("select * from Vendor_Routing with(nolock) where vendor_id = @vendor_id", cn)
                    reply.SelectCommand.Parameters.AddWithValue("vendor_id", Request("vendor_id"))

                    reply.Fill(dt)
                    If Not try_submit(dt.Rows(0).Item("send_to"), isDupe, True) Then
                        Response.Write("FAIL - Duplicate Lead")
                        txtSourceID.Text = dt.Rows(0).Item("prefix").ToString & "_" & txtSourceID.Text
                        try_submit(dt.Rows(0).Item("fail_to").ToString)
                    Else
                        Response.Write("Successful Post")
                    End If

                    cn.Close()
                    cn.Dispose()
                    Response.End()


                End If

                If Request("sub_id") IsNot Nothing Then

                    Dim dt As New DataTable

                    Dim reply As New SqlDataAdapter("select * from Routing with(nolock) where sub_id = @sub_id", cn)
                    reply.SelectCommand.Parameters.AddWithValue("sub_id", Request("sub_id"))

                    reply.Fill(dt)

                    'Get daily cap
                    'select count(*) from MyFootpathAffiliates where sub_id = 'PNIL' and submitted_date > convert(varchar(10),GetDate(),101)

                    Dim cn2 As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
                    cn2.Open()
                    Dim daily_dt As New DataTable
                    Dim reply2 As New SqlDataAdapter("select count(*) from MyFootpathAffiliates where sub_id = @sub_id and submitted_date > convert(varchar(10),GetDate(),101)", cn2)
                    reply2.SelectCommand.Parameters.AddWithValue("sub_id", Request("sub_id"))
                    reply2.Fill(daily_dt)
                    cn2.Close()
                    cn2.Dispose()


                    If isDupe Then
                        try_submit(dt.Rows(0).Item("fail_to").ToString)
                        Response.Write("FAIL - Duplicate Lead")
                        cn.Close()
                        cn.Dispose()
                        Response.End()
                    End If


                    Dim daily_count As Integer = 0
                    If daily_dt.Rows.Count > 0 Then
                        daily_count = daily_dt.Rows(0).Item(0)
                    End If



                    If dt.Rows.Count > 0 Then

                        If ((dt.Rows(0).Item("post_email").ToString = "True") Or (dt.Rows(0).Item("daily_roll_to").ToString = "True")) Then
                            'If dt.Rows(0).Item("post_email").ToString = "True" Then
                            Post_Email(False, False)
                        End If

                        If daily_count <> 0 And (daily_count < dt.Rows(0).Item("daily_cap") And daily_count Mod 2 = 0) Then
                            If Not try_submit(dt.Rows(0).Item("daily_roll_to").ToString, isDupe, True) Then
                                Response.Write("FAIL - Duplicate Lead")
                                If Not try_submit(dt.Rows(0).Item("send_to").ToString, isDupe) Then
                                    txtSourceID.Text = dt.Rows(0).Item("prefix").ToString & "_" & txtSourceID.Text
                                    If Not try_submit(dt.Rows(0).Item("fail_to").ToString) Then
                                        txtSourceID.Text = dt.Rows(0).Item("prefix").ToString & "_" & txtSourceID.Text
                                        If try_submit(dt.Rows(0).Item("second_fail").ToString) Then
                                            cn.Close()
                                            cn.Dispose()
                                            Response.End()
                                        End If
                                    Else
                                        cn.Close()
                                        cn.Dispose()
                                        Response.End()
                                    End If

                                End If
                            Else
                                cn.Close()
                                cn.Dispose()
                                Response.Write("Successful Post")
                                Response.End()
                            End If
                        Else
                            If Not try_submit(dt.Rows(0).Item("send_to"), isDupe, True) Then
                                Response.Write("FAIL - Duplicate Lead")
                                If Not try_submit(dt.Rows(0).Item("daily_roll_to").ToString, isDupe) Then
                                    txtSourceID.Text = dt.Rows(0).Item("prefix").ToString & "_" & txtSourceID.Text
                                    If Not try_submit(dt.Rows(0).Item("fail_to").ToString) Then
                                        txtSourceID.Text = dt.Rows(0).Item("prefix").ToString & "_" & txtSourceID.Text
                                        If try_submit(dt.Rows(0).Item("second_fail").ToString) Then
                                            cn.Close()
                                            cn.Dispose()
                                            Response.End()
                                        End If
                                    Else
                                        cn.Close()
                                        cn.Dispose()
                                        Response.End()
                                    End If

                                End If
                            Else
                                cn.Close()
                                cn.Dispose()
                                Response.Write("Successful Post")
                                Response.End()
                            End If
                        End If



                    Else
                        Select Case Request("vendor_id")
                            Case "1"
                                Post_leadresell(True, True)
                            Case "2"
                                Post_LR_Data(True, True)
                            Case "3"
                                post_ed_soup(True, True)
                        End Select

                    End If

                End If


                cn.Close()
                cn.Dispose()
                Response.End()


            End If
        End If
    End Sub

    Protected Function try_submit(routine As String, Optional isDupe As Boolean = False, Optional first_pass As Boolean = False) As Boolean
        If Not isDupe Then
            Select Case routine
                Case "Post_leadresell"
                    If Post_leadresell(True, first_pass) Then Return True
                Case "Post_leadresell2"
                    If Post_leadresell2(True, first_pass) Then Return True
                Case "Post_leadresell3"
                    If Post_leadresell3(True, first_pass) Then Return True

                Case "Post_leadreselljg"
                    If Post_leadreselljg(True, first_pass) Then Return True

                Case "Post_leadresell4"
                    If Post_leadresell4(True, first_pass) Then Return True
                Case "Post_leadresell5"
                    If Post_leadresell5(True, first_pass) Then Return True

                Case "post_leadresell_SPHN"
                    If Post_leadresell_SPHN(True, first_pass) Then Return True
                Case "post_leadresell_FSA"
                    If Post_leadresell_FSA(True, first_pass) Then Return True
                Case "post_leadresell_MAU"
                    If Post_leadresell_MAU(True, first_pass) Then Return True
                Case "post_leadresell_CWU"
                    If Post_leadresell_CWU(True, first_pass) Then Return True

                Case "Post_leadsresell_YCAR"
                    If Post_leadsresell_YCAR(True, first_pass) Then Return True
                Case "Post_leadsresell_LJSR"
                    If Post_leadsresell_LJSR(True, first_pass) Then Return True
                Case "Post_leadsresell_EDS2"
                    If Post_leadsresell_EDS2(True, first_pass) Then Return True



                Case "post_leadresell_PR"
                    If post_leadresell_PR(True, first_pass) Then Return True
                Case "post_leadresell_UA"
                    If post_leadresell_UA(True, first_pass) Then Return True
                Case "post_NW"
                    If post_NW(True, first_pass) Then Return True
                Case "Post_LR_Data"
                    If Post_LR_Data(True, first_pass) Then Return True
                Case "Post_ed_soup"
                    If post_ed_soup(True, first_pass) Then Return True
                Case "post_PerfectPitch"
                    If post_PerfectPitch(True, first_pass) Then Return True
                Case "Post_adspire"
                    If post_adspire(True, first_pass) Then Return True
                Case "post_axiom"
                    If post_axiom(True, first_pass) Then Return True
                Case "post_1NW"
                    If post_1NW(True, first_pass) Then Return True
                Case "post_market"
                    If post_market(True, first_pass) Then Return True
                Case "post_Accelerex"
                    If post_Accelerex(True, first_pass) Then Return True

                Case "post_Duplicate"
                    If Post_Duplicate(True, first_pass) Then Return True

                Case "post_edsoup_mob"
                    If post_edsoup_mob(True, first_pass) Then Return True
                Case "post_OBWT"
                    If post_OBWT(True, first_pass) Then Return True
                Case "post_h2h"
                    If post_h2h(True, first_pass) Then Return True
                Case "post_SMS_A"
                    If post_SMS_A(True, first_pass) Then Return True
                Case "post_SMS_B"
                    If post_SMS_B(True, first_pass) Then Return True

                Case "post_SMS_MIB"
                    If post_SMS_MIB(True, first_pass) Then Return True
                Case "post_SMS_HIM"
                    If post_SMS_HIM(True, first_pass) Then Return True
                Case "post_SMS_FHN"
                    If post_SMS_FHN(True, first_pass) Then Return True
                Case "post_SMS_UFR"
                    If post_SMS_UFR(True, first_pass) Then Return True

                Case "Post_ES"
                    If Post_ES(True, first_pass) Then Return True
                Case "Post_OAM"
                    If Post_OAM(True, first_pass) Then Return True
                Case "Post_OAM2"
                    If Post_OAM2(True, first_pass) Then Return True
                Case "Post_leadresellHFAF"
                    If Post_leadresellHFAF(True, first_pass) Then Return True

                Case "Post_leadresell_YCA"
                    If Post_leadresell_YCA(True, first_pass) Then Return True

                Case "Post_database"
                    If Post_database(True, first_pass) Then Return True
                Case "Post_Email"
                    If Post_Email(True, first_pass) Then Return True
                Case "post_USN_FFR"
                    If post_USN_FFR(True, first_pass) Then Return True

                Case "post_USN_FHN"
                    If post_USN_FHN(True, first_pass) Then Return True


            End Select
        Else
            Select Case routine

                Case "post_Duplicate"
                    If Post_Duplicate(True, first_pass) Then Return True

            End Select
        End If
        Return False
    End Function

    Protected Sub Post_Data()
        Exit Sub

        Dim centerID As String = ""
        If Request("centerID") IsNot Nothing Then
            If Request("centerID") = "2" Then
                'Repost_Data()
                'Exit Sub
            End If
        End If

        Dim this_uri As Uri
        If ddlTest.SelectedValue = "Yes" Then
            this_uri = New Uri("http://test-ps.myfootpath.com/v2/transmission.php")
        Else
            this_uri = New Uri("http://ps.myfootpath.com/v2/transmission.php")
        End If

        Dim data As String
        If Request("vendor_id") IsNot Nothing Then
            Select Case Request("vendor_id")
                Case "1" 'Estomes
                    data = "afid=167&afkey=3136bc30e8b4be820e16abace7c28f1e&campaign_id=169&request_type_id=16"
                Case "2" 'Tymax 
                    data = "afid=115&afkey=7ac92e12a4330e9f62ab7b0694005161&campaign_id=124&request_type_id=16"
                Case Else
                    data = "afid=167&afkey=3136bc30e8b4be820e16abace7c28f1e&campaign_id=169&request_type_id=16"
            End Select
        Else
            data = "afid=167&afkey=3136bc30e8b4be820e16abace7c28f1e&campaign_id=169&request_type_id=16"
        End If

        data &= "&first_name=" & txtFirstName.Text
        data &= "&last_name=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&primary_phone=" & txtPrimaryPhone.Text
        data &= "&citizen=" & txtCitizen.Text
        data &= "&field_of_interest=" & txtInterest.Text
        data &= "&has_hs_or_ged=" & txtGraduated.Text
        data &= "&source_id=" & txtSourceID.Text

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        ' Response.Write(tmp)



        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)


        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp <> "success" Then
            ' Repost_Data()
        End If

        Response.End()
    End Sub


    Protected Function Post_LR_Data(show_result As Boolean, first_pass As Boolean) As Boolean

        Dim this_uri As Uri
        this_uri = New Uri("http://live.estomes.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        data = "list_id=" & txtSourceID.Text

        data &= "&first_name=" & txtFirstName.Text
        data &= "&last_name=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&number1=" & txtPrimaryPhone.Text
        data &= "&number2=&number3=&street=&city=&state=&zip=&ip=&datetime="

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        'cn.Open()
        'Dim reply As New SqlCommand("AddFSO", cn)
        'reply.CommandType = System.Data.CommandType.StoredProcedure
        'reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        'reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        'reply.Parameters.AddWithValue("email", txtEmail.Text)
        'reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        'reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        'reply.Parameters.AddWithValue("zip", txtzip.Text)
        'reply.Parameters.AddWithValue("submitted_data", data)
        'reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        'reply.Parameters.AddWithValue("result", tmp)
        'reply.ExecuteNonQuery()
        'cn.Close()

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp = "SUCCESS" Then
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        Else
            reply.Parameters.AddWithValue("post_result", "FAIL")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_LR_Data")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()


        If tmp = "SUCCESS" Then
            Return True
        Else
            Return False
        End If


    End Function


    Protected Function Post_Duplicate(show_result As Boolean, first_pass As Boolean) As Boolean

        Dim this_uri As String = "Duplicate"

        Dim data As String = ""

        Dim tmp As String = "Duplicate"


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp = "SUCCESS" Then
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        Else
            reply.Parameters.AddWithValue("post_result", "FAIL")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("url", txtURL.Text)

        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()


        If tmp = "SUCCESS" Then
            Return True
        Else
            Return False
        End If


    End Function


    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Post_Data()

    End Sub

    Protected Sub Repost_Data()

        Dim data As String

        data = "userpass=bw430fhww5"

        data &= "&first_name=" & txtFirstName.Text
        data &= "&last_name=" & txtLastName.Text
        data &= "&email_address=" & txtEmail.Text
        data &= "&phone_num=" & txtPrimaryPhone.Text
        data &= "&address_one=&address_two=&address_three=&city=&state=&postal_code=&auto_leadid=&comments="
        data &= "&vendor_id=" & txtSourceID.Text

        Dim this_uri As Uri
        this_uri = New Uri("http://dialer205.jdmphone.com/estromes.php?" & data)




        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        'web_request.ContentLength = data.Length
        'web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        Response.Write("<br>Repost:" & tmp)

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()
        Response.End()
    End Sub


    Protected Function Post_leadresell(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=25705"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&gender=&ip=" & txtIP.Text
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function post_NW(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri

        Dim data As String


        this_uri = New Uri("https://api.five9.com/web2campaign/AddToList")
        data = "F9domain=backtolearn&F9list=NextWave&affiliate=NextWave&F9CallASAP=1"
        data &= "&number1=" & txtPrimaryPhone.Text
        data &= "&first_name=" & txtFirstName.Text
        data &= "&last_name=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&street=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&zip=" & txtZip.Text
        data &= "&ipAddress=" & txtIP.Text

        'data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="

        'data &= "&gender="
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Try
            Dim writer As New StreamWriter(web_request.GetRequestStream)
            writer.Write(data)
            writer.Close()

        Catch ex As Exception
            Response.Write(ex.Message & "<br>")
            Response.Write(this_uri.AbsoluteUri & "?" & data)
            Return True
            'Response.End()
        End Try


        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.Replace(Chr(34), "").IndexOf("name=F9errCode value=0") = -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_NW")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()
        cn.Dispose()

        If tmp.Replace(Chr(34), "").IndexOf("name=F9errCode value=0") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function



    Protected Function Post_leadresell5(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=32386"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&gender=&ip=" & txtIP.Text
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell5")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function post_leadresell_UA(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=34173"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&gender=&ip=" & txtIP.Text
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell_UA")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function



    Protected Function Post_leadresell2(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=26834"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&gender=&ip=" & txtIP.Text
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell2")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function Post_leadresell3(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=34442"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&gender=&ip=" & txtIP.Text
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell3")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function Post_leadreselljg2(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=34442"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&gender=&ip=" & txtIP.Text
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_leadreselljg2")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function


    Protected Function Post_leadresell4(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=25759"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&gender=&ip=" & txtIP.Text
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell4")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function



    Protected Function post_adspire(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://www.adspirenetwork.com/api/api_edu.php")

        Dim data As String

        data = "method=pushLead&apiKey=I1oHO7[CVt:u}OXkJ=5tttq59d0BtEy!{QtLyj5&buyerId=3327&campaignId=107&eduLevel=&bestTimeCall=&startTime=&gender=&employer="
        'data &= "&campaign_name=" & txtSourceID.Text
        data &= "&firstName=" & txtFirstName.Text
        data &= "&lastName=" & txtLastName.Text
        data &= "&emailAddress=" & txtEmail.Text
        data &= "&phoneNumberHome=" & txtPrimaryPhone.Text
        data &= "&zipCode=" & txtZip.Text
        data &= "&dob=&phoneNumberAlternative="
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        'data &= "&url=" & txtSourceID.Text
        data &= "&ipAddress=" & txtIP.Text
        data &= "&eduProgram=" & txtInterest.Text
        data &= "&graduationYear=" & txtGraduated.Text
        'data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Success") = -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_adspire")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Success") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function
    Protected Function post_Accelerex(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("https://app.leadconduit.com/v2/PostLeadAction")

        Dim data As String

        data = "xxCampaignId=0530xohqo&xxAccountId=053ekni7p"
        data &= "&lead_source_id=" & txtSourceID.Text
        data &= "&first_name=" & txtFirstName.Text
        data &= "&last_name=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone_home=" & txtPrimaryPhone.Text
        data &= "&ip_address=" & txtIP.Text
        data &= "&address1=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&postal_code=" & txtZip.Text
        data &= "&placement=accx14&subid=" & txtSourceID.Text


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("<result>success</result>") = -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_Accelerex")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("<result>success</result>") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function



    Protected Function post_axiom(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://feeds.leads-junction.com/leadpost.php ")

        Dim data As String

        data = "clientid=409&campaigncode=8"
        data &= "&campaign_name=" & txtSourceID.Text
        data &= "&firstname=" & txtFirstName.Text
        data &= "&lastname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&zip=" & txtZip.Text
        data &= "&homephone=" & txtPrimaryPhone.Text
        data &= "&ip=" & txtIP.Text
        data &= "&url=" & txtSourceID.Text
        data &= "&country=&dateofbirth=&workphone=&mobilephone=&gender=&language=&comments="




        data &= "&program=" & txtInterest.Text
        data &= "&gradyear=" & txtGraduated.Text
        data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Accepted") = -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_axiom")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Accepted") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function
    Protected Function post_1NW(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("https://api.five9.com/web2campaign/AddToList")

        Dim data As String

        data = "F9domain=1NWContact&F9list=EDUAD.NET_livefeed_112612"
        'data &= "&campaign_name=" & txtSourceID.Text
        data &= "&first_name=" & txtFirstName.Text
        data &= "&last_name=" & txtLastName.Text
        'data &= "&email=" & txtEmail.Text
        data &= "&number1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&number2="
        data &= "&street=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        'data &= "&url=" & txtSourceID.Text
        'data &= "&ip_address=" & txtIP.Text
        'data &= "&program=" & txtInterest.Text
        'data &= "&gradyear=" & txtGraduated.Text
        'data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")

        ' campaign_name,url,ip_address,gradyear,email,program,regdate,dob

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim check_string As String = "name=" & Chr(34) & "F9errCode" & Chr(34) & " value=" & Chr(34) & "0" & Chr(34) & " "

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)

        If tmp.Replace(Chr(34), "").IndexOf("name=F9errCode value=0") = -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_1NW")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.Replace(Chr(34), "").IndexOf("name=F9errCode value=0") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function

    Protected Function post_market(show_result As Boolean, first_pass As Boolean) As Boolean

        'https://restapilite.safesoftsolutions.com/leads/insert?
        'phone_number=4242564037&auth_token=cf7a69dcb3ebd0a67cca6706e50127b5b54455b7&list_id=26701
        '&form_data[field_29723]=lawanda&form_data[field_29725]=wells&form_data[field_29727]=332w Regentst.4
        '&form_data[field_29731]=Inglewood&form_data[field_29733]=CA&form_data[field_29735]=90301&form_data[field_29737]=4242564030
        '&form_data[field_29743]=lawandawellsluvconner26@gmail.com&form_data[field_30547]=CEOcheck_dup=1&hopper=1
        '&form_data[field_30543]=98.126.131.89&form_data[field_30545]=08/7/2012 13:59

        Dim this_uri As Uri
        this_uri = New Uri("https://restapilite.safesoftsolutions.com/leads/insert")

        Dim data As String

        data = "&auth_token=cf7a69dcb3ebd0a67cca6706e50127b5b54455b7&list_id=26701"
        data &= "&form_data[field_29723]=" & txtFirstName.Text
        data &= "&form_data[field_29725]=" & txtLastName.Text
        data &= "&form_data[field_29743]=" & txtEmail.Text
        data &= "&phone_number=" & txtPrimaryPhone.Text
        data &= "&form_data[field_29737]=" & txtPrimaryPhone.Text
        data &= "&form_data[field_29735]=" & txtZip.Text
        data &= "&form_data[field_29727]=" & txtAddress.Text
        data &= "&form_data[field_29731]=" & txtCity.Text
        data &= "&form_data[field_29733]=" & txtState.Text
        data &= "&form_data[field_30543]=" & txtIP.Text
        data &= "&form_data[field_30545]=" & Format(Now, "yyyy-mm-dd hh:mm_ss")

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)

        If tmp.IndexOf("success") = -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_market")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("success") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function

    Protected Function post_edsoup_mob(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://www.edsoup.com/dhein/transfer.php")

        Dim data As String

        data = "key=130e377e7808b4d36ee5c2d570d66b4c&source_id=209"
        data &= "&campaign_name=" & txtSourceID.Text
        data &= "&first_name=" & txtFirstName.Text
        data &= "&last_name=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&number1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&number2="
        data &= "&street=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&url=" & txtURL.Text
        data &= "&ip_address=" & txtIP.Text
        data &= "&program=" & txtInterest.Text
        data &= "&gradyear=" & txtGraduated.Text
        data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")
        data &= "&referringsite=FinishSchoolOnline.com"
        data &= "&leadid=" & txtLeadID.Text

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If Left(tmp, 4) = "fail" Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_edsoup_mob")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        reply.Parameters.AddWithValue("lead_id", txtLeadID.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If Left(tmp, 4) = "fail" Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function


    Protected Function post_PerfectPitch(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://www.edsoup.com/dhein/transfer.php")

        Dim data As String

        data = "key=130e377e7808b4d36ee5c2d570d66b4c&source_id=221"
        data &= "&campaign_name=" & txtSourceID.Text
        data &= "&first_name=" & txtFirstName.Text
        data &= "&last_name=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&number1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&number2="
        data &= "&street=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&url=" & txtSourceID.Text
        data &= "&ip_address=" & txtIP.Text
        data &= "&program=" & txtInterest.Text
        data &= "&gradyear=" & txtGraduated.Text
        data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")
        data &= "&referringsite=123freetravel.com/HBB/"

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If Left(tmp, 4) = "fail" Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_PerfectPitch")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If Left(tmp, 4) = "fail" Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function


    Protected Function post_OBWT(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://www.edsoup.com/dhein/transfer.php")

        Dim data As String

        data = "key=130e377e7808b4d36ee5c2d570d66b4c&source_id=226"
        data &= "&campaign_name=" & txtSourceID.Text
        data &= "&first_name=" & txtFirstName.Text
        data &= "&last_name=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&number1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&number2="
        data &= "&street=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&url=" & txtSourceID.Text
        data &= "&ip_address=" & txtIP.Text
        data &= "&program=" & txtInterest.Text
        data &= "&gradyear=" & txtGraduated.Text
        data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")
        data &= "&referringsite=123freetravel.com/HBB/"

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If Left(tmp, 4) = "fail" Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_OBWT")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If Left(tmp, 4) = "fail" Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function




    Protected Function post_ed_soup(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri


        Dim data As String

        data = "key=130e377e7808b4d36ee5c2d570d66b4c&source_id=196"
        data &= "&campaign_name=" & txtSourceID.Text
        data &= "&first_name=" & txtFirstName.Text
        data &= "&last_name=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&number1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&number2="
        data &= "&street=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&url=" & txtSourceID.Text
        data &= "&ip_address=" & txtIP.Text
        data &= "&program=" & txtInterest.Text
        data &= "&gradyear=" & txtGraduated.Text
        data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")
        data &= "&referringsite=123freetravel.com/HBB/"

        ' data &= "&testlead=1"


        this_uri = New Uri("http://www.edsoup.com/dhein/transfer.php?" & data)

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        'web_request.ContentLength = data.Length
        'web_request.ContentType = "application/x-www-form-urlencoded"
        'Dim writer As New StreamWriter(web_request.GetRequestStream)
        'writer.Write(data, 0, data.Length)
        'writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If Left(tmp, 4) = "fail" Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_ed_soup")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If Left(tmp, 4) = "fail" Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function
    Protected Sub Repost_Live()
        Dim this_uri As Uri

        this_uri = New Uri("http://live.estomes.com")



        Dim data As String = "vendor_id=2&sub_id=LR_PNIL"

        data &= "&first_name=" & txtFirstName.Text
        data &= "&last_name=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&primary_phone=" & txtPrimaryPhone.Text
        data &= "&citizen=" & txtCitizen.Text
        data &= "&field_of_interest=" & txtInterest.Text
        data &= "&has_hs_or_ged=" & txtGraduated.Text
        ' data &= "&source_id=" & Request("source_id").ToString


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()

    End Sub


    Protected Function post_h2h(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://h2htrk.com/d.ashx")

        Dim data As String

        data = "ckm_campaign_id=10266&ckm_key=Xbwvw2zHDvg"
        data &= "&ckm_subid=" & txtSourceID.Text
        data &= "&first_name=" & txtFirstName.Text
        data &= "&last_name=" & txtLastName.Text
        data &= "&email_address=" & txtEmail.Text
        data &= "&phone_home=" & txtPrimaryPhone.Text

        'data &= "&zip=" & txtZip.Text
        'data &= "&dob=&number2="
        'data &= "&street=" & txtAddress.Text
        'data &= "&city=" & txtCity.Text
        'data &= "&state=" & txtState.Text
        'data &= "&url=" & txtSourceID.Text
        'data &= "&ip_address=" & txtIP.Text
        'data &= "&program=" & txtInterest.Text
        'data &= "&gradyear=" & txtGraduated.Text
        'data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")
        'data &= "&referringsite=123freetravel.com/HBB/"

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_h2h")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function


    Protected Function post_SMS_A(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://cc.specialeads.com/data_inflow/external_data_loader.php")
        data = "affiliate_id=106&username=estomes&sub=A"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&address1=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&zip=" & txtZip.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&gradyear=" & txtGraduated.Text
        data &= "&origin=" & txtURL.Text
        data &= "&requestdate=" & Today.ToShortDateString

        data &= "&ip=" & txtIP.Text
        data &= "&optin=" & txtOptInDate.Text

        'data &= "&test=Y"

        'data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="


        'data &= "&gender="


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_SMS_A")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function post_SMS_B(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://cc.specialeads.com/data_inflow/external_data_loader.php")
        data = "affiliate_id=106&username=estomes&sub=HFAF"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&address1=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&zip=" & txtZip.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&gradyear=" & txtGraduated.Text
        data &= "&origin=" & txtURL.Text
        data &= "&requestdate=" & Today.ToShortDateString

        data &= "&ip=" & txtIP.Text
        data &= "&optin=" & txtOptInDate.Text

        'data &= "&test=Y"

        'data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="


        'data &= "&gender="


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_SMS_B")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function




    Protected Function post_leadresell_PR(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=34290"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&gender=&ip=" & txtIP.Text
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell_PR")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function



    Protected Function Post_ES(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://www.edutracking.net/education_source_live_post.php")
        data = "Branch=CENTRAL&CampaignId=116037&DataSource=Stomel Media"
        data &= "&FirstName=" & txtFirstName.Text
        data &= "&LastName=" & txtLastName.Text
        data &= "&Email=" & txtEmail.Text
        data &= "&Phone=" & txtPrimaryPhone.Text
        data &= "&Zip=" & txtZip.Text
        data &= "&Address1=" & txtAddress.Text
        data &= "&City=" & txtCity.Text
        data &= "&State=" & txtState.Text
        data &= "&IP=" & txtIP.Text
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_ES")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function Post_leadreselljg(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=34442"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&gender=&ip=" & txtIP.Text
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_leadreselljg")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function


    Protected Function Post_OAM(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://services.higheredgrowth.com/oadata/record")
        data = "SourceId=10240-1" '&CampaignId=eStomes
        data &= "&SubId=" & txtSourceID.Text
        data &= "&FirstName=" & txtFirstName.Text
        data &= "&LastName=" & txtLastName.Text
        data &= "&EmailAddress=" & txtEmail.Text
        data &= "&PhoneNumber=" & txtPrimaryPhone.Text
        data &= "&ZipCode=" & txtZip.Text
        data &= "&StreetAddress=" & txtAddress.Text
        data &= "&IPv4Address=" & txtIP.Text
        data &= "&CampaignId=" & txtURL.Text
        data &= "&IsTest=false"
        data &= "&CallingOnBehalf=" & txtURL.Text
        data &= "&TcpaConsent=true"
        data &= "&TcpaDisclosure=By checking ""yes"", I consent to be contacted regarding educational opportunities at the phone number provided, including mobile number, using an automated telephone dialing system. I may be contacted by Education Ahead, online-edu-help.com, Education First or 1 of the following partners regardless of being on a federal or state do not call list. This is a separate optional offer for education. Consent is not required as a condition of using this service."


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        'Response.Write(this_uri.AbsoluteUri & "?" & data)

        Dim tmp As String
        Try
            Dim web_response As HttpWebResponse = web_request.GetResponse()
            Dim reader As New StreamReader(web_response.GetResponseStream())
            tmp = reader.ReadToEnd()
            web_response.Close()
        Catch ex As WebException
            tmp = ex.Response.Headers.ToString
        End Try






        If show_result Then
            'Response.Write(tmp)
            'Response.End()

        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf(Chr(34) & "IsValid" & Chr(34) & ":true") = -1 Then 'Fail" Then
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_OAM")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf(Chr(34) & "IsValid" & Chr(34) & ":true") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function Post_OAM2(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://services.higheredgrowth.com/oadata/record")
        data = "SourceId=10240-2" '&CampaignId=eStomes
        data &= "&SubId=" & txtSourceID.Text
        data &= "&FirstName=" & txtFirstName.Text
        data &= "&LastName=" & txtLastName.Text
        data &= "&EmailAddress=" & txtEmail.Text
        data &= "&PhoneNumber=" & txtPrimaryPhone.Text
        data &= "&ZipCode=" & txtZip.Text
        data &= "&StreetAddress=" & txtAddress.Text
        data &= "&IPv4Address=" & txtIP.Text
        data &= "&CampaignId=" & txtURL.Text
        data &= "&IsTest=false"
        data &= "&CallingOnBehalf=" & txtURL.Text
        data &= "&TcpaConsent=true"
        data &= "&TcpaDisclosure=By checking ""yes"", I consent to be contacted regarding educational opportunities at the phone number provided, including mobile number, using an automated telephone dialing system. I may be contacted by Education Ahead, online-edu-help.com, Education First or 1 of the following partners regardless of being on a federal or state do not call list. This is a separate optional offer for education. Consent is not required as a condition of using this service."


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        'Response.Write(this_uri.AbsoluteUri & "?" & data)

        Dim tmp As String
        Try
            Dim web_response As HttpWebResponse = web_request.GetResponse()
            Dim reader As New StreamReader(web_response.GetResponseStream())
            tmp = reader.ReadToEnd()
            web_response.Close()
        Catch ex As WebException
            tmp = ex.Response.Headers.ToString
        End Try






        If show_result Then
            'Response.Write(tmp)
            'Response.End()

        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf(Chr(34) & "IsValid" & Chr(34) & ":true") = -1 Then 'Fail" Then
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_OAM2")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf(Chr(34) & "IsValid" & Chr(34) & ":true") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function


    Protected Function Post_leadresellHFAF(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=35044"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&gender=&ip=" & txtIP.Text
        'data &= "&test=1"

        Dim tmp As String

        Try
            Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
            web_request.Method = WebRequestMethods.Http.Post
            web_request.ContentLength = data.Length
            web_request.ContentType = "application/x-www-form-urlencoded"
            Dim writer As New StreamWriter(web_request.GetRequestStream)
            writer.Write(data)
            writer.Close()

            Dim web_response As HttpWebResponse = web_request.GetResponse()
            Dim reader As New StreamReader(web_response.GetResponseStream())
            tmp = reader.ReadToEnd()
            web_response.Close()
            If show_result Then
                ' Response.Write(tmp)
            End If
        Catch ex As Exception
            tmp = ex.Message
        End Try




        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_leadresellHFAF")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function


    Protected Function Post_leadresell_YCA(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=36086"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&gender=&ip=" & txtIP.Text
        'data &= "&test=1"

        Dim tmp As String

        Try
            Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
            web_request.Method = WebRequestMethods.Http.Post
            web_request.ContentLength = data.Length
            web_request.ContentType = "application/x-www-form-urlencoded"
            Dim writer As New StreamWriter(web_request.GetRequestStream)
            writer.Write(data)
            writer.Close()

            Dim web_response As HttpWebResponse = web_request.GetResponse()
            Dim reader As New StreamReader(web_response.GetResponseStream())
            tmp = reader.ReadToEnd()
            web_response.Close()
            If show_result Then
                ' Response.Write(tmp)
            End If
        Catch ex As Exception
            tmp = ex.Message
        End Try




        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_leadresell_YCA")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function




    Protected Function Post_database(show_result As Boolean, first_pass As Boolean) As Boolean

        Dim data As String = ""




        Dim tmp As String = ""


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)


        reply.Parameters.AddWithValue("show_result", show_result)
        reply.Parameters.AddWithValue("post_result", "SUCCESS")

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", "N/A")
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_database")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf(Chr(34) & "IsValid" & Chr(34) & ":true") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function



    Protected Function Post_Email(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri

        Dim data As String

        'http://submission.lvlivefeed2.com/c2c_dpost.php?from=<email>&first_name=<firstname>&last_name=<lastname>&zip=<zip>&ipaddress=<ipaddress>&sitename=<site>&listname=<listname>


        data = "listname=estomes" '&CampaignId=eStomes
        data &= "&from=" & txtEmail.Text
        data &= "&first_name=" & txtFirstName.Text
        data &= "&last_name=" & txtLastName.Text
        data &= "&zip=" & txtZip.Text
        data &= "&ipaddress=" & txtIP.Text
        data &= "&sitename=" & txtURL.Text

        this_uri = New Uri("http://submission.lvlivefeed2.com/c2c_dpost.php?" & data)
        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        'web_request.ContentLength = data.Length
        'web_request.ContentType = "application/x-www-form-urlencoded"
        'Dim writer As New StreamWriter(web_request.GetRequestStream)
        'writer.Write(data.ToString)
        'writer.Close()

        'Response.Write(this_uri.AbsoluteUri & "?" & data)

        Dim tmp As String
        Try
            Dim web_response As HttpWebResponse = web_request.GetResponse()
            Dim reader As New StreamReader(web_response.GetResponseStream())
            tmp = reader.ReadToEnd()
            web_response.Close()
        Catch ex As WebException
            tmp = ex.Response.Headers.ToString
        End Try






        If show_result Then
            'Response.Write(tmp)
            'Response.End()

        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("1") = -1 Then 'Fail" Then
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_Email")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("1") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function





    Protected Function Post_leadresell_SPHN(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        Dim data As String

        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=35692"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&gender=&ip=" & txtIP.Text
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell_SPHN")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function


    Protected Function Post_leadresell_MAU(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=35746"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&gender=&ip=" & txtIP.Text
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_leadresell_MAU")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function
    Protected Function Post_leadresell_FSA(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=35800"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&gender=&ip=" & txtIP.Text
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_leadresell_FSA")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function
    Protected Function Post_leadresell_CWU(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=35854"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&gender=&ip=" & txtIP.Text
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_leadresell_CWU")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function Post_leadsresell_YCAR(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=36122"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&gender=&ip=" & txtIP.Text
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_leadsresell_YCAR")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function



    Protected Function Post_leadsresell_LJSR(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=36098"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&gender=&ip=" & txtIP.Text
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_leadsresell_LJSR")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function Post_leadsresell_EDS2(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=36110"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&gender=&ip=" & txtIP.Text
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_leadsresell_EDS2")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function






    Protected Function post_USN_FFR(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://www.usnewsuniversitydirectory.com/services/leads/vendor.svc/submitlead")
        data = "MCID=54886&campaignid=9991200&VendorID=15&nurture=1&v_ismobile=Y&disclosureName=USNUC-eStomes-FindFamilyResources&consentlanguage=By checking ""yes"", I consent to be contacted regarding educational and other opportunities at the phone number provided, including mobile number, using an automated telephone dialing system. I may be contacted by Education Ahead, online-edu-help.com, U.S. News University Connection, LLC or 1 of the following partners regardless of being on a federal or state do not call list. This is a separate optional offer for education. Consent is not required as a condition of using this service."
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&state=" & txtState.Text
        data &= "&optin=" & Today.ToShortDateString
        data &= "&postdate=" & Today.ToShortDateString
        data &= "&ip=" & txtIP.Text
        data &= "&consenturl=" & txtURL.Text
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("failure") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_USN_FFR")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("failure") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function


    Protected Function post_USN_FHN(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://www.usnewsuniversitydirectory.com/services/leads/vendor.svc/submitlead")
        data = "MCID=54887&campaignid=9991210&VendorID=15&nurture=1&v_ismobile=Y&disclosureName=USNUC-eStomes-FindFamilyResources&consentlanguage=By checking ""yes"", I consent to be contacted regarding educational and other opportunities at the phone number provided, including mobile number, using an automated telephone dialing system. I may be contacted by Education Ahead, online-edu-help.com, U.S. News University Connection, LLC or 1 of the following partners regardless of being on a federal or state do not call list. This is a separate optional offer for education. Consent is not required as a condition of using this service."
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone=" & txtPrimaryPhone.Text
        data &= "&zip=" & txtZip.Text
        data &= "&state=" & txtState.Text
        data &= "&optin=" & Today.ToShortDateString
        data &= "&postdate=" & Today.ToShortDateString
        data &= "&ip=" & txtIP.Text
        data &= "&consenturl=" & txtURL.Text
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("failure") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_USN_FFR")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("failure") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function



    Protected Function post_SMS_MIB(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://cc.specialeads.com/data_inflow/external_data_loader.php")
        data = "affiliate_id=106&username=estomes&sub=MIB"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&address1=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&zip=" & txtZip.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&gradyear=" & txtGraduated.Text
        data &= "&origin=" & txtURL.Text
        data &= "&requestdate=" & Today.ToShortDateString

        data &= "&ip=" & txtIP.Text
        data &= "&optin=" & txtOptInDate.Text

        'data &= "&test=Y"

        'data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="


        'data &= "&gender="


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_SMS_MIB")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function


    Protected Function post_SMS_HIM(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://cc.specialeads.com/data_inflow/external_data_loader.php")
        data = "affiliate_id=106&username=estomes&sub=HIM"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&address1=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&zip=" & txtZip.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&gradyear=" & txtGraduated.Text
        data &= "&origin=" & txtURL.Text
        data &= "&requestdate=" & Today.ToShortDateString

        data &= "&ip=" & txtIP.Text
        data &= "&optin=" & txtOptInDate.Text

        'data &= "&test=Y"

        'data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="


        'data &= "&gender="


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_SMS_HIM")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function




    Protected Function post_SMS_FHN(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://cc.specialeads.com/data_inflow/external_data_loader.php")
        data = "affiliate_id=106&username=estomes&sub=FHN"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&address1=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&zip=" & txtZip.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&gradyear=" & txtGraduated.Text
        data &= "&origin=" & txtURL.Text
        data &= "&requestdate=" & Today.ToShortDateString

        data &= "&ip=" & txtIP.Text
        data &= "&optin=" & txtOptInDate.Text

        'data &= "&test=Y"

        'data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="


        'data &= "&gender="


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_SMS_FHN")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function


    Protected Function post_SMS_UFR(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://cc.specialeads.com/data_inflow/external_data_loader.php")
        data = "affiliate_id=106&username=estomes&sub=UFR"
        data &= "&fname=" & txtFirstName.Text
        data &= "&lname=" & txtLastName.Text
        data &= "&address1=" & txtAddress.Text
        data &= "&city=" & txtCity.Text
        data &= "&state=" & txtState.Text
        data &= "&zip=" & txtZip.Text
        data &= "&email=" & txtEmail.Text
        data &= "&phone1=" & txtPrimaryPhone.Text
        data &= "&gradyear=" & txtGraduated.Text
        data &= "&origin=" & txtURL.Text
        data &= "&requestdate=" & Today.ToShortDateString

        data &= "&ip=" & txtIP.Text
        data &= "&optin=" & txtOptInDate.Text

        'data &= "&test=Y"

        'data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="


        'data &= "&gender="


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Text)
        reply.Parameters.AddWithValue("last_name", txtLastName.Text)
        reply.Parameters.AddWithValue("email", txtEmail.Text)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Text)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Text)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Text)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Text)

        reply.Parameters.AddWithValue("address", txtAddress.Text)
        reply.Parameters.AddWithValue("city", txtCity.Text)
        reply.Parameters.AddWithValue("state", txtState.Text)
        reply.Parameters.AddWithValue("zip", txtZip.Text)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Text)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Text)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Text)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Text)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_SMS_UFR")
        reply.Parameters.AddWithValue("url", txtURL.Text)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function





    Protected Sub Page_PreRenderComplete(sender As Object, e As System.EventArgs) Handles Me.PreRenderComplete

    End Sub
End Class


