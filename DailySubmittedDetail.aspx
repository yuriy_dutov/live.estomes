﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DailySubmittedDetail.aspx.vb" Inherits="DailySubmittedDetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td>Start Date:</td>
                    <td>
                        <asp:Calendar ID="Calendar1" runat="server"></asp:Calendar>
                    </td>

                    <td>End Date:</td>
                    <td>
                        <asp:Calendar ID="Calendar2" runat="server"></asp:Calendar>
                    </td>
                </tr>
            </table>




            <asp:Button ID="btnGo" runat="server" Text="Go" />
            <asp:GridView ID="gvDailySubmittedDetail" runat="server" DataSourceID="dsDailySubmittedDetail"></asp:GridView>
            <asp:SqlDataSource ID="dsDailySubmittedDetail" runat="server" ConnectionString="<%$ ConnectionStrings:SchoolConnectionString3 %>" SelectCommand="DetailByDayWithCounts" SelectCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:ControlParameter Name="startday" ControlID="calendar1" PropertyName="SelectedDate" />
                    <asp:ControlParameter Name="endday" ControlID="calendar2" PropertyName="SelectedDate" />
                </SelectParameters>

            </asp:SqlDataSource>
        </div>
    </form>
</body>
</html>
