﻿
Partial Class DailySubmittedDetail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Calendar1.SelectedDate = DateAdd(DateInterval.Day, -1, Today)
            Calendar2.SelectedDate = Today

        End If
    End Sub
End Class
