﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="function_edit.aspx.vb" ValidateRequest="false"
    Inherits="function_edit" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        input {
            width: 300px;
            padding: 1%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <a href="function_designer.aspx">Back to Designer</a>
            <asp:FormView ID="FormView1" DataSourceID="dsPost" runat="server" DataKeyNames="id"
                DefaultMode="Edit" CellPadding="4" ForeColor="#333333">
                <EditItemTemplate>
                    <table>
                        <tr>
                            <td>id:
                            </td>
                            <td>
                                <asp:Label ID="idLabel1" runat="server" Text='<%# Eval("id") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td>post_name:
                            </td>
                            <td>
                                <asp:TextBox ID="post_nameTextBox" runat="server" Text='<%# Bind("post_name") %>' />
                            </td>
                            <td>active:
                            </td>
                            <td>
                                <asp:CheckBox ID="activeCheckBox" runat="server" Checked='<%# Bind("active") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td>FirstName:
                            </td>
                            <td>
                                <asp:TextBox ID="FirstNameTextBox" runat="server" Text='<%# Bind("FirstName") %>' />
                            </td>
                            <td>LastName:
                            </td>
                            <td>
                                <asp:TextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("LastName") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td>Email:
                            </td>
                            <td>
                                <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' />
                            </td>
                            <td>Phone:
                            </td>
                            <td>
                                <asp:TextBox ID="PhoneTextBox" runat="server" Text='<%# Bind("Phone") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td>Zip:
                            </td>
                            <td>
                                <asp:TextBox ID="ZipTextBox" runat="server" Text='<%# Bind("Zip") %>' />
                            </td>
                            <td>Address:
                            </td>
                            <td>
                                <asp:TextBox ID="AddressTextBox" runat="server" Text='<%# Bind("Address") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td>City:
                            </td>
                            <td>
                                <asp:TextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>' />
                            </td>
                            <td>State:
                            </td>
                            <td>
                                <asp:TextBox ID="StateTextBox" runat="server" Text='<%# Bind("State") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td>IP:
                            </td>
                            <td>
                                <asp:TextBox ID="IPTextBox" runat="server" Text='<%# Bind("IP") %>' />
                            </td>
                            <td>Citizen:
                            </td>
                            <td>
                                <asp:TextBox ID="CitizenTextBox" runat="server" Text='<%# Bind("Citizen") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td>Graduated:
                            </td>
                            <td>
                                <asp:TextBox ID="GraduatedTextBox" runat="server" Text='<%# Bind("Graduated") %>' />
                            </td>
                            <td>Interest:
                            </td>
                            <td>
                                <asp:TextBox ID="InterestTextBox" runat="server" Text='<%# Bind("Interest") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td>LeadID:
                            </td>
                            <td>
                                <asp:TextBox ID="LeadIDTextBox" runat="server" Text='<%# Bind("LeadID") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td>OptInDate:
                            </td>
                            <td>
                                <asp:TextBox ID="OptInDateTextBox" runat="server" Text='<%# Bind("OptInDate") %>' />
                            </td>
                            <td>OptInTime:
                            </td>
                            <td>
                                <asp:TextBox ID="OptInTimeTextBox" runat="server" Text='<%# Bind("OptInTime") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td>SourceID:
                            </td>
                            <td>
                                <asp:TextBox ID="SourceIDTextBox" runat="server" Text='<%# Bind("SourceID") %>' />
                            </td>
                            <td>URL:
                            </td>
                            <td>
                                <asp:TextBox ID="URLTextBox" runat="server" Text='<%# Bind("URL")%>' />
                            </td>
                        </tr>
                        <tr>
                            <td>post_URL:
                            </td>
                            <td>
                                <asp:TextBox ID="post_URLTextBox" runat="server" Text='<%# Bind("post_URL") %>' />
                            </td>
                            <td>Post Method:</td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlMethod" SelectedValue='<%# Bind("method")%>'>
                                    <asp:ListItem Text="(Select)" Value=""></asp:ListItem>
                                    <asp:ListItem>post</asp:ListItem>
                                    <asp:ListItem>get</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>Rejected String
                            </td>
                            <td>
                                <asp:TextBox ID="txtRejected" runat="server" Text='<%# Bind("rejected") %>' />
                            </td>
                            <td>Today - i.e. &date_sent=9/15/2010 - enter date_sent
                            </td>
                            <td>
                                <asp:TextBox ID="txtToday" runat="server" Text='<%# Bind("today") %>' />
                            </td>
                        </tr>

                        <tr>
                            <td>Rejectable
                            </td>
                            <td>
                                <asp:CheckBox ID="chkReject" runat="server" checked='<%# Bind("reject")%>' />
                            </td>
                            <td>While rejecting, rate of rejects(0-100%)
                            </td>
                            <td>
                                <asp:TextBox ID="txtRejectRate" runat="server" Text='<%# Bind("reject_rate")%>' />
                            </td>
                        </tr>


                        <tr><td>Cost</td>
                            <td><asp:TextBox ID="txtCost" runat="server" Text='<%# Bind("cost")%>' /></td>
                            <td>Revenue</td>
                            <td><asp:TextBox ID="txtRevenue" runat="server" Text='<%# Bind("revenue")%>' /></td>
                        </tr>

                        <tr>
                            <td>fixed_string:
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="fixed_stringTextBox" TextMode="MultiLine" Columns="90" runat="server"
                                    Text='<%# Bind("fixed_string") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td>Notes:
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="notesTextBox" TextMode="MultiLine" Rows="10" Columns="90" runat="server"
                                    Text='<%# Bind("notes") %>' />
                            </td>
                        </tr>
                    </table>
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update"
                        Text="Update" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False"
                        CommandName="Cancel" Text="Cancel" />
                </EditItemTemplate>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <InsertItemTemplate>
                    FirstName:
                <asp:TextBox ID="FirstNameTextBox" runat="server" Text='<%# Bind("FirstName") %>' />
                    <br />
                    LastName:
                <asp:TextBox ID="LastNameTextBox" runat="server" Text='<%# Bind("LastName") %>' />
                    <br />
                    Email:
                <asp:TextBox ID="EmailTextBox" runat="server" Text='<%# Bind("Email") %>' />
                    <br />
                    Phone:
                <asp:TextBox ID="PhoneTextBox" runat="server" Text='<%# Bind("Phone") %>' />
                    <br />
                    Zip:
                <asp:TextBox ID="ZipTextBox" runat="server" Text='<%# Bind("Zip") %>' />
                    <br />
                    Address:
                <asp:TextBox ID="AddressTextBox" runat="server" Text='<%# Bind("Address") %>' />
                    <br />
                    City:
                <asp:TextBox ID="CityTextBox" runat="server" Text='<%# Bind("City") %>' />
                    <br />
                    State:
                <asp:TextBox ID="StateTextBox" runat="server" Text='<%# Bind("State") %>' />
                    <br />
                    IP:
                <asp:TextBox ID="IPTextBox" runat="server" Text='<%# Bind("IP") %>' />
                    <br />
                    Citizen:
                <asp:TextBox ID="CitizenTextBox" runat="server" Text='<%# Bind("Citizen") %>' />
                    <br />
                    Graduated:
                <asp:TextBox ID="GraduatedTextBox" runat="server" Text='<%# Bind("Graduated") %>' />
                    <br />
                    Interest:
                <asp:TextBox ID="InterestTextBox" runat="server" Text='<%# Bind("Interest") %>' />
                    <br />
                    LeadID:
                <asp:TextBox ID="LeadIDTextBox" runat="server" Text='<%# Bind("LeadID") %>' />
                    <br />
                    OptInDate:
                <asp:TextBox ID="OptInDateTextBox" runat="server" Text='<%# Bind("OptInDate") %>' />
                    <br />
                    OptInTime:
                <asp:TextBox ID="OptInTimeTextBox" runat="server" Text='<%# Bind("OptInTime") %>' />
                    <br />
                    SourceID:
                <asp:TextBox ID="SourceIDTextBox" runat="server" Text='<%# Bind("SourceID") %>' />
                    <br />
                    post_URL:
                <asp:TextBox ID="post_URLTextBox" runat="server" Text='<%# Bind("post_URL") %>' />
                    <br />
                    fixed_string:
                <asp:TextBox ID="fixed_stringTextBox" runat="server" Text='<%# Bind("fixed_string") %>' />
                    <br />
                    post_name:
                <asp:TextBox ID="post_nameTextBox" runat="server" Text='<%# Bind("post_name") %>' />
                    <br />
                    active:
                <asp:CheckBox ID="activeCheckBox" runat="server" Checked='<%# Bind("active") %>' />
                    <br />
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                        Text="Insert" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False"
                        CommandName="Cancel" Text="Cancel" />
                </InsertItemTemplate>
                <ItemTemplate>
                    id:
                <asp:Label ID="idLabel" runat="server" Text='<%# Eval("id") %>' />
                    <br />
                    FirstName:
                <asp:Label ID="FirstNameLabel" runat="server" Text='<%# Bind("FirstName") %>' />
                    <br />
                    LastName:
                <asp:Label ID="LastNameLabel" runat="server" Text='<%# Bind("LastName") %>' />
                    <br />
                    Email:
                <asp:Label ID="EmailLabel" runat="server" Text='<%# Bind("Email") %>' />
                    <br />
                    Phone:
                <asp:Label ID="PhoneLabel" runat="server" Text='<%# Bind("Phone") %>' />
                    <br />
                    Zip:
                <asp:Label ID="ZipLabel" runat="server" Text='<%# Bind("Zip") %>' />
                    <br />
                    Address:
                <asp:Label ID="AddressLabel" runat="server" Text='<%# Bind("Address") %>' />
                    <br />
                    City:
                <asp:Label ID="CityLabel" runat="server" Text='<%# Bind("City") %>' />
                    <br />
                    State:
                <asp:Label ID="StateLabel" runat="server" Text='<%# Bind("State") %>' />
                    <br />
                    IP:
                <asp:Label ID="IPLabel" runat="server" Text='<%# Bind("IP") %>' />
                    <br />
                    Citizen:
                <asp:Label ID="CitizenLabel" runat="server" Text='<%# Bind("Citizen") %>' />
                    <br />
                    Graduated:
                <asp:Label ID="GraduatedLabel" runat="server" Text='<%# Bind("Graduated") %>' />
                    <br />
                    Interest:
                <asp:Label ID="InterestLabel" runat="server" Text='<%# Bind("Interest") %>' />
                    <br />
                    LeadID:
                <asp:Label ID="LeadIDLabel" runat="server" Text='<%# Bind("LeadID") %>' />
                    <br />
                    OptInDate:
                <asp:Label ID="OptInDateLabel" runat="server" Text='<%# Bind("OptInDate") %>' />
                    <br />
                    OptInTime:
                <asp:Label ID="OptInTimeLabel" runat="server" Text='<%# Bind("OptInTime") %>' />
                    <br />
                    SourceID:
                <asp:Label ID="SourceIDLabel" runat="server" Text='<%# Bind("SourceID") %>' />
                    <br />
                    post_URL:
                <asp:Label ID="post_URLLabel" runat="server" Text='<%# Bind("post_URL") %>' />
                    <br />
                    fixed_string:
                <asp:Label ID="fixed_stringLabel" runat="server" Text='<%# Bind("fixed_string") %>' />
                    <br />
                    post_name:
                <asp:Label ID="post_nameLabel" runat="server" Text='<%# Bind("post_name") %>' />
                    <br />
                    active:
                <asp:CheckBox ID="activeCheckBox" runat="server" Checked='<%# Bind("active") %>'
                    Enabled="false" />
                    <br />
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit"
                        Text="Edit" />
                    &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete"
                        Text="Delete" />
                    &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New"
                        Text="New" />
                </ItemTemplate>
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            </asp:FormView>
            <asp:SqlDataSource ID="dsPost" ConnectionString="<%$ ConnectionStrings:SchoolConnectionString3 %>"
                runat="server" SelectCommand="SELECT * FROM [post_info] where id=@id" UpdateCommand="UPDATE [post_info] SET [FirstName] = @FirstName, [LastName] = @LastName, [Email] = @Email, [Phone] = @Phone, 
            [Zip] = @Zip, [Address] = @Address, [City] = @City, [State] = @State, [IP] = @IP, [Citizen] = @Citizen, [Graduated] = @Graduated, 
            [Interest] = @Interest, [LeadID] = @LeadID, [OptInDate] = @OptInDate, [OptInTime] = @OptInTime, [SourceID] = @SourceID, [post_URL] = @post_URL, 
            [fixed_string] = @fixed_string, [post_name] = @post_name, [active] = @active, URL=@URL, notes=@notes, rejected=@rejected, today=@today, method=@method,
                reject=@reject, reject_rate=@reject_rate, cost=@cost, revenue=@revenue WHERE [id] = @id">
                <SelectParameters>
                    <asp:QueryStringParameter QueryStringField="id" Name="id" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="FirstName" Type="String" />
                    <asp:Parameter Name="LastName" Type="String" />
                    <asp:Parameter Name="Email" Type="String" />
                    <asp:Parameter Name="Phone" Type="String" />
                    <asp:Parameter Name="Zip" Type="String" />
                    <asp:Parameter Name="Address" Type="String" />
                    <asp:Parameter Name="City" Type="String" />
                    <asp:Parameter Name="State" Type="String" />
                    <asp:Parameter Name="IP" Type="String" />
                    <asp:Parameter Name="Citizen" Type="String" />
                    <asp:Parameter Name="Graduated" Type="String" />
                    <asp:Parameter Name="Interest" Type="String" />
                    <asp:Parameter Name="LeadID" Type="String" />
                    <asp:Parameter Name="OptInDate" Type="String" />
                    <asp:Parameter Name="OptInTime" Type="String" />
                    <asp:Parameter Name="SourceID" Type="String" />
                    <asp:Parameter Name="post_URL" Type="String" />
                    <asp:Parameter Name="fixed_string" Type="String" />
                    <asp:Parameter Name="post_name" Type="String" />
                    <asp:Parameter Name="notes" Type="String" />
                    <asp:Parameter Name="rejected" Type="String" />
                    <asp:Parameter Name="today" Type="String" />
                    <asp:Parameter Name="method" Type="String" />
                    <asp:Parameter Name="active" Type="Boolean" />
                    <asp:Parameter Name="reject" Type="Boolean" />
                    <asp:Parameter Name="reject_rate" Type="String" />
                    <asp:Parameter Name="revenue" Type="String" />
                    <asp:Parameter Name="cost" Type="String" />
                    <asp:Parameter Name="id" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
            Sample Posting string:
        <asp:HyperLink ID="hlSample" Font-Size="Smaller" runat="server"></asp:HyperLink>
        </div>
    </form>
</body>
</html>
