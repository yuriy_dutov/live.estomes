﻿Option Compare Text

Imports System.Net
Imports System.IO
Imports System.Data.SqlClient
Imports System.Data
Imports Common

Partial Class _Default
    Inherits System.Web.UI.Page

    Dim isDupe As Boolean = False

    Protected Function try_submit(routine As String, Optional isDupe As Boolean = False, Optional first_pass As Boolean = False) As Boolean


        'If routine = "" Then
        '    Return False
        'End If

        'new try_submit
        If Not isDupe Then
            Dim post_dt As DataTable = GetSchoolTable("select * from post_info where post_name = '" & routine & "'")
            If post_dt.Rows.Count > 0 Then



                Dim this_uri As Uri
                Dim data As String = ""


                this_uri = New Uri(post_dt.Rows(0).Item("post_URL"))
                If post_dt.Rows(0).Item("fixed_string").ToString <> "" Then
                    data &= post_dt.Rows(0).Item("fixed_string")
                End If
                If post_dt.Rows(0).Item("FirstName").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("FirstName") & "=" & txtFirstName.Value
                End If
                If post_dt.Rows(0).Item("LastName").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("LastName") & "=" & txtLastName.Value
                End If
                If post_dt.Rows(0).Item("Email").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Email") & "=" & txtEmail.Value
                End If
                If post_dt.Rows(0).Item("Phone").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Phone") & "=" & txtPrimaryPhone.Value
                End If
                If post_dt.Rows(0).Item("Zip").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Zip") & "=" & txtZip.Value
                End If
                If post_dt.Rows(0).Item("Address").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Address") & "=" & txtAddress.Value
                End If
                If post_dt.Rows(0).Item("City").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("City") & "=" & txtCity.Value
                End If
                If post_dt.Rows(0).Item("State").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("State") & "=" & txtState.Value
                End If
                If post_dt.Rows(0).Item("IP").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("IP") & "=" & txtIP.Value
                End If
                If post_dt.Rows(0).Item("Citizen").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Citizen") & "=" & txtCitizen.Value
                End If
                If post_dt.Rows(0).Item("Graduated").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Graduated") & "=" & txtGraduated.Value
                End If
                If post_dt.Rows(0).Item("Interest").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("Interest") & "=" & txtInterest.Value
                End If
                If post_dt.Rows(0).Item("LeadID").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("LeadID") & "=" & txtLeadID.Value
                End If
                If post_dt.Rows(0).Item("OptinDate").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("OptinDate") & "=" & txtOptInDate.Value
                End If
                If post_dt.Rows(0).Item("OptInTime").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("OptInTime") & "=" & txtOptInTime.Value
                End If
                If post_dt.Rows(0).Item("SourceID").ToString <> "" Then

                    Dim sources() As String = post_dt.Rows(0).Item("SourceID").ToString.Split("|")
                    For Each source In sources
                        data &= "&" & source & "=" & txtSourceID.Value
                    Next

                    'data &= "&" & post_dt.Rows(0).Item("SourceID") & "=" & txtSourceID.value
                End If
                If post_dt.Rows(0).Item("URL").ToString <> "" Then
                    data &= "&" & post_dt.Rows(0).Item("URL") & "=" & txtURL.Value
                End If
                If post_dt.Rows(0).Item("Today").ToString <> "" Then

                    Dim sources() As String = post_dt.Rows(0).Item("Today").ToString.Split("|")
                    For Each source In sources
                        data &= "&" & source & "=" & Today.ToShortDateString
                    Next


                    'data &= "&" & post_dt.Rows(0).Item("Today") & "=" & Today.ToShortDateString
                End If



                Dim web_request As HttpWebRequest

                If post_dt.Rows(0).Item("method") = "post" Then

                    web_request = HttpWebRequest.Create(this_uri)
                    web_request.Method = WebRequestMethods.Http.Post
                    web_request.ContentLength = System.Text.Encoding.UTF8.GetByteCount(data)
                    web_request.ContentType = "application/x-www-form-urlencoded"
                    Dim writer As New StreamWriter(web_request.GetRequestStream)
                    writer.Write(data, 0, System.Text.Encoding.UTF8.GetByteCount(data))
                    writer.Close()
                Else
                    this_uri = New Uri(post_dt.Rows(0).Item("post_URL") & "?" & data)
                    web_request = HttpWebRequest.Create(this_uri)
                    web_request.Method = WebRequestMethods.Http.Get

                End If



                'web_request.ContentLength = data.Length



                Dim web_response As HttpWebResponse = web_request.GetResponse()
                Dim reader As New StreamReader(web_response.GetResponseStream())
                Dim tmp As String = reader.ReadToEnd()
                web_response.Close()



                Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
                cn.Open()
                Dim reply As New SqlCommand("AddMFP", cn)
                reply.CommandType = System.Data.CommandType.StoredProcedure
                reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
                reply.Parameters.AddWithValue("last_name", txtLastName.Value)
                reply.Parameters.AddWithValue("email", txtEmail.Value)
                reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
                reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
                reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
                reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

                reply.Parameters.AddWithValue("address", txtAddress.Value)
                reply.Parameters.AddWithValue("city", txtCity.Value)
                reply.Parameters.AddWithValue("state", txtState.Value)
                reply.Parameters.AddWithValue("zip", txtZip.Value)

                reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
                reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
                reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

                reply.Parameters.AddWithValue("show_result", True)
                If tmp.IndexOf(post_dt.Rows(0).Item("rejected").ToString) > -1 Then 'Fail
                    reply.Parameters.AddWithValue("post_result", "FAIL")
                Else
                    reply.Parameters.AddWithValue("post_result", "SUCCESS")
                End If

                reply.Parameters.AddWithValue("lead_id", txtLeadID.Value)

                reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
                reply.Parameters.AddWithValue("submitted_data", data)
                reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
                reply.Parameters.AddWithValue("result", tmp)
                reply.Parameters.AddWithValue("isDupe", isDupe)
                reply.Parameters.AddWithValue("first_pass", first_pass)
                reply.Parameters.AddWithValue("post_function", post_dt.Rows(0).Item("post_name").ToString)
                reply.Parameters.AddWithValue("url", txtURL.Value)
                If Request("vendor_id") IsNot Nothing Then
                    reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
                Else
                    reply.Parameters.AddWithValue("vendor_id", "0")
                End If


                reply.Parameters.AddWithValue("query_string", Request.Url.Query)

                'If routine = "Post_leadsresell_YCA" Then
                '    'email_error(cn.State.ToString)
                'End If

                reply.ExecuteNonQuery()
                cn.Close()

                If tmp.IndexOf(post_dt.Rows(0).Item("rejected").ToString) > -1 Then 'Fail
                    Return False
                Else
                    Return True
                End If
            Else
                Select Case routine
                    Case "post_Duplicate"
                        If Post_Duplicate(True, first_pass) Then Return True
                    Case "Post_database"
                        If Post_database(True, first_pass) Then Return True
                    Case "Post_Email"
                        If Post_Email(True, first_pass) Then Return True
                End Select
                Return False
            End If
        Else
            Select Case routine

                Case "post_Duplicate"
                    If Post_Duplicate(True, first_pass) Then Return True

            End Select
        End If


        Return False
    End Function


    Protected Function try_submit2(routine As String, Optional isDupe As Boolean = False, Optional first_pass As Boolean = False) As Boolean
        Select Case routine
            Case "Post_leadresell"
                If Post_leadresell(True, first_pass) Then Return True
            Case "Post_leadresell2"
                If Post_leadresell2(True, first_pass) Then Return True
            Case "Post_leadresell3"
                If Post_leadresell3(True, first_pass) Then Return True
            Case "Post_leadresell3"
                If Post_leadresell3(True, first_pass) Then Return True
            Case "Post_leadreselljg"
                If Post_leadreselljg(True, first_pass) Then Return True
            Case "Post_leadresell4"
                If Post_leadresell4(True, first_pass) Then Return True
            Case "Post_leadresell5"
                If Post_leadresell5(True, first_pass) Then Return True

            Case "post_leadresell_SPHN"
                If Post_leadresell_SPHN(True, first_pass) Then Return True
            Case "Post_leadresell_FSA"
                If Post_leadresell_FSA(True, first_pass) Then Return True
            Case "Post_leadresell_MAU"
                If Post_leadresell_MAU(True, first_pass) Then Return True
            Case "Post_leadresell_CWU"
                If Post_leadresell_CWU(True, first_pass) Then Return True

            Case "Post_leadsresell_YCAR"
                If Post_leadsresell_YCAR(True, first_pass) Then Return True
            Case "Post_leadsresell_LJSR"
                If Post_leadsresell_LJSR(True, first_pass) Then Return True
            Case "Post_leadsresell_EDS2"
                If Post_leadsresell_EDS2(True, first_pass) Then Return True





            Case "post_leadresell_PR"
                If post_leadresell_PR(True, first_pass) Then Return True
            Case "post_leadresell_UA"
                If post_leadresell_UA(True, first_pass) Then Return True



            Case "post_NW"
                If post_NW(True, first_pass) Then Return True
            Case "Post_LR_Data"
                If Post_LR_Data(True, first_pass) Then Return True
            Case "Post_ed_soup"
                If post_ed_soup(True, first_pass) Then Return True
            Case "post_PerfectPitch"
                If post_PerfectPitch(True, first_pass) Then Return True
            Case "Post_adspire"
                If post_adspire(True, first_pass) Then Return True
            Case "post_axiom"
                If post_axiom(True, first_pass) Then Return True
            Case "post_1NW"
                If post_1NW(True, first_pass) Then Return True
            Case "post_market"
                If post_market(True, first_pass) Then Return True
            Case "post_Accelerex"
                If post_Accelerex(True, first_pass) Then Return True

            Case "post_Duplicate"
                If Post_Duplicate(True, first_pass) Then Return True

            Case "post_edsoup_mob"
                If post_edsoup_mob(True, first_pass) Then Return True
            Case "post_OBWT"
                If post_OBWT(True, first_pass) Then Return True
            Case "post_h2h"
                If post_h2h(True, first_pass) Then Return True
            Case "post_SMS_A"
                If post_SMS_A(True, first_pass) Then Return True
            Case "post_SMS_B"
                If post_SMS_B(True, first_pass) Then Return True


            Case "post_SMS_MIB"
                If post_SMS_MIB(True, first_pass) Then Return True
            Case "post_SMS_HIM"
                If post_SMS_HIM(True, first_pass) Then Return True
            Case "post_SMS_FHN"
                If post_SMS_FHN(True, first_pass) Then Return True

            Case "post_SMS_UFR"
                If post_SMS_UFR(True, first_pass) Then Return True

            Case "Post_ES"
                If Post_ES(True, first_pass) Then Return True
            Case "Post_leadresellHFAF"
                If Post_leadresellHFAF(True, first_pass) Then Return True

            Case "Post_leadresell_YCA"
                If Post_leadresell_YCA(True, first_pass) Then Return True


            Case "Post_OAM"
                If Post_OAM(True, first_pass) Then Return True
            Case "Post_OAM2"
                If Post_OAM2(True, first_pass) Then Return True

            Case "Post_database"
                If Post_database(True, first_pass) Then Return True

            Case "Post_Email"
                If Post_Email(True, first_pass) Then Return True
            Case "post_USN_FFR"
                If post_USN_FFR(True, first_pass) Then Return True

            Case "post_USN_FHN"
                If post_USN_FHN(True, first_pass) Then Return True

        End Select

        Return False
    End Function

    Protected Sub Post_Data()
        Exit Sub

        Dim centerID As String = ""
        If Request("centerID") IsNot Nothing Then
            If Request("centerID") = "2" Then
                'Repost_Data()
                'Exit Sub
            End If
        End If

        Dim this_uri As Uri

        this_uri = New Uri("http://ps.myfootpath.com/v2/transmission.php")

        Dim data As String
        If Request("vendor_id") IsNot Nothing Then
            Select Case Request("vendor_id")
                Case "1" 'Estomes
                    data = "afid=167&afkey=3136bc30e8b4be820e16abace7c28f1e&campaign_id=169&request_type_id=16"
                Case "2" 'Tymax 
                    data = "afid=115&afkey=7ac92e12a4330e9f62ab7b0694005161&campaign_id=124&request_type_id=16"
                Case Else
                    data = "afid=167&afkey=3136bc30e8b4be820e16abace7c28f1e&campaign_id=169&request_type_id=16"
            End Select
        Else
            data = "afid=167&afkey=3136bc30e8b4be820e16abace7c28f1e&campaign_id=169&request_type_id=16"
        End If

        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&primary_phone=" & txtPrimaryPhone.Value
        data &= "&citizen=" & txtCitizen.Value
        data &= "&field_of_interest=" & txtInterest.Value
        data &= "&has_hs_or_ged=" & txtGraduated.Value
        data &= "&source_id=" & txtSourceID.Value

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        ' Response.Write(tmp)



        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)


        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp <> "success" Then
            ' Repost_Data()
        End If

        Response.End()
    End Sub

    Protected Function Post_leadresell5(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=32386"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&gender=&ip=" & txtIP.Value
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell5")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function post_leadresell_UA(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=32440"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&gender=&ip=" & txtIP.Value
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell_UA")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function Post_LR_Data(show_result As Boolean, first_pass As Boolean) As Boolean

        Dim this_uri As Uri
        this_uri = New Uri("http://live.estomes.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        data = "list_id=" & txtSourceID.Value

        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&number1=" & txtPrimaryPhone.Value
        data &= "&number2=&number3=&street=&city=&state=&zip=&ip=&datetime="

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        'cn.Open()
        'Dim reply As New SqlCommand("AddFSO", cn)
        'reply.CommandType = System.Data.CommandType.StoredProcedure
        'reply.Parameters.AddWithValue("first_name", txtFirstName.value)
        'reply.Parameters.AddWithValue("last_name", txtLastName.value)
        'reply.Parameters.AddWithValue("email", txtEmail.value)
        'reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.value)
        'reply.Parameters.AddWithValue("field_of_interest", txtInterest.value)
        'reply.Parameters.AddWithValue("zip", txtzip.value)
        'reply.Parameters.AddWithValue("submitted_data", data)
        'reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        'reply.Parameters.AddWithValue("result", tmp)
        'reply.ExecuteNonQuery()
        'cn.Close()

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp = "SUCCESS" Then
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        Else
            reply.Parameters.AddWithValue("post_result", "FAIL")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_LR_Data")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()


        If tmp = "SUCCESS" Then
            Return True
        Else
            Return False
        End If


    End Function


    Protected Function Post_Duplicate(show_result As Boolean, first_pass As Boolean) As Boolean

        Dim this_uri As String = "Duplicate"

        Dim data As String = ""

        Dim tmp As String = "Duplicate"


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp = "SUCCESS" Then
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        Else
            reply.Parameters.AddWithValue("post_result", "FAIL")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("url", txtURL.Value)

        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()


        If tmp = "SUCCESS" Then
            Return True
        Else
            Return False
        End If


    End Function


    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Dim num_loaded As Integer = 0
        txtSourceID.Value = txtSubID.Text
        FileUpload1.SaveAs(Server.MapPath("docs/" & FileUpload1.FileName))
        Dim objStreamReader As StreamReader
        objStreamReader = File.OpenText(Server.MapPath("docs/" & FileUpload1.FileName))
        'Dim contents As String = objStreamReader.ReadLine()
        Dim file_lines() As String = objStreamReader.ReadToEnd.Split(vbCrLf)
        objStreamReader.Close()
        ' Dim someString As String

        'While objStreamReader.Peek() <> -1
        For Each file_line In file_lines
            'Response.Write(file_line & "<br>")
            If file_line <> "" Then
                Dim line_info() As String = file_line.Split(",")
                If line_info.Length > 10 Then
                    txtFirstName.Value = line_info(0)
                    txtLastName.Value = line_info(1)
                    txtEmail.Value = line_info(2)
                    txtAddress.Value = line_info(3)
                    txtCity.Value = line_info(4)
                    txtState.Value = line_info(5)
                    txtZip.Value = line_info(6)
                    txtPrimaryPhone.Value = line_info(7)
                    'txtFirstNa.Value = line_info(8)
                    'txtFirstName.Value = line_info(9)
                    txtOptInDate.Value = line_info(10)
                    txtOptInTime.Value = line_info(11)
                    txtURL.Value = txtSubID.Text
                    Try
                        If try_submit(DropDownList1.SelectedValue, False, False) Then
                            'Response.Write("Posted: " & txtFirstName.Value & " " & txtLastName.Value & "<br>")
                        Else
                            'Response.Write("Posted, but failed: " & txtFirstName.Value & " " & txtLastName.Value & "<br>")
                        End If


                    Catch ex As Exception
                        'Response.Write("Failed: " & txtFirstName.Value & " " & txtLastName.Value & " " & ex.Message & "<br>")
                    End Try

                    num_loaded += 1
                End If
            End If
        Next
        'someString = objStreamReader.ReadLine()
        '... do whatever else you need to do ...



        'End While
        Response.Write("Loaded: " & num_loaded)
    End Sub

    Protected Sub Repost_Data()

        Dim data As String

        data = "userpass=bw430fhww5"

        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email_address=" & txtEmail.Value
        data &= "&phone_num=" & txtPrimaryPhone.Value
        data &= "&address_one=&address_two=&address_three=&city=&state=&postal_code=&auto_leadid=&comments="
        data &= "&vendor_id=" & txtSourceID.Value

        Dim this_uri As Uri
        this_uri = New Uri("http://dialer205.jdmphone.com/estromes.php?" & data)




        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        'web_request.ContentLength = data.Length
        'web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        Response.Write("<br>Repost:" & tmp)

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()
        Response.End()
    End Sub


    Protected Function Post_leadresell(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=25705"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&gender=&ip=" & txtIP.Value
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function Post_leadresell2(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=26834"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&gender=&ip=" & txtIP.Value
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell2")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function Post_leadresell3(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=34442"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&gender=&ip=" & txtIP.Value
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell3")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function


    Protected Function Post_leadreselljg(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=34442"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&gender=&ip=" & txtIP.Value
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_leadreselljg")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function


    Protected Function post_NW(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri

        Dim data As String


        this_uri = New Uri("https://api.five9.com/web2campaign/AddToList")
        data = "F9domain=backtolearn&F9list=NextWave&affiliate=NextWave&F9CallASAP=1"
        data &= "&number1=" & txtPrimaryPhone.Value
        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&street=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&zip=" & txtZip.Value
        data &= "&ipAddress=" & txtIP.Value

        'data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="

        'data &= "&gender="
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Try
            Dim writer As New StreamWriter(web_request.GetRequestStream)
            writer.Write(data)
            writer.Close()

        Catch ex As Exception
            Response.Write(ex.Message & "<br>")
            Response.Write(this_uri.AbsoluteUri & "?" & data)
            Return True
            'Response.End()
        End Try


        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.Replace("""", "").IndexOf("name=F9errCode value=0") = -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_NW")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()
        cn.Dispose()

        If tmp.Replace("""", "").IndexOf("name=F9errCode value=0") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function




    Protected Function Post_leadresell4(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=25759"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&gender=&ip=" & txtIP.Value
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Try
            Dim writer As New StreamWriter(web_request.GetRequestStream)
            writer.Write(data)
            writer.Close()

        Catch ex As Exception
            Response.Write(ex.Message & "<br>")
            Response.Write(this_uri.AbsoluteUri & "?" & data)
            Return True
            'Response.End()
        End Try


        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell4")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()
        cn.Dispose()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function



    Protected Function post_adspire(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://www.adspirenetwork.com/api/api_edu.php")

        Dim data As String

        data = "method=pushLead&apiKey=I1oHO7[CVt:u}OXkJ=5tttq59d0BtEy!{QtLyj5&buyerId=3327&campaignId=107&eduLevel=&bestTimeCall=&startTime=&gender=&employer="
        'data &= "&campaign_name=" & txtSourceID.value
        data &= "&firstName=" & txtFirstName.Value
        data &= "&lastName=" & txtLastName.Value
        data &= "&emailAddress=" & txtEmail.Value
        data &= "&phoneNumberHome=" & txtPrimaryPhone.Value
        data &= "&zipCode=" & txtZip.Value
        data &= "&dob=&phoneNumberAlternative="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        'data &= "&url=" & txtSourceID.value
        data &= "&ipAddress=" & txtIP.Value
        data &= "&eduProgram=" & txtInterest.Value
        data &= "&graduationYear=" & txtGraduated.Value
        'data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Success") = -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_adspire")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Success") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function
    Protected Function post_Accelerex(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("https://app.leadconduit.com/v2/PostLeadAction")

        Dim data As String

        data = "xxCampaignId=0530xohqo&xxAccountId=053ekni7p"
        data &= "&lead_source_id=" & txtSourceID.Value
        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone_home=" & txtPrimaryPhone.Value
        data &= "&ip_address=" & txtIP.Value
        data &= "&address1=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&postal_code=" & txtZip.Value
        data &= "&placement=accx14&subid=" & txtSourceID.Value


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("<result>success</result>") = -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_Accelerex")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("<result>success</result>") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function



    Protected Function post_axiom(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://feeds.leads-junction.com/leadpost.php ")

        Dim data As String

        data = "clientid=409&campaigncode=8"
        data &= "&campaign_name=" & txtSourceID.Value
        data &= "&firstname=" & txtFirstName.Value
        data &= "&lastname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&zip=" & txtZip.Value
        data &= "&homephone=" & txtPrimaryPhone.Value
        data &= "&ip=" & txtIP.Value
        data &= "&url=" & txtSourceID.Value
        data &= "&country=&dateofbirth=&workphone=&mobilephone=&gender=&language=&comments="




        data &= "&program=" & txtInterest.Value
        data &= "&gradyear=" & txtGraduated.Value
        data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Accepted") = -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_axiom")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Accepted") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function
    Protected Function post_1NW(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("https://api.five9.com/web2campaign/AddToList")

        Dim data As String

        data = "F9domain=1NWContact&F9list=EDUAD.NET_livefeed_112612"
        'data &= "&campaign_name=" & txtSourceID.value
        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        'data &= "&email=" & txtEmail.value
        data &= "&number1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&number2="
        data &= "&street=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        'data &= "&url=" & txtSourceID.value
        'data &= "&ip_address=" & txtIP.value
        'data &= "&program=" & txtInterest.value
        'data &= "&gradyear=" & txtGraduated.value
        'data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")

        ' campaign_name,url,ip_address,gradyear,email,program,regdate,dob

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim check_string As String = "name=" & Chr(34) & "F9errCode" & Chr(34) & " value=" & Chr(34) & "0" & Chr(34) & " "

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf(check_string) = -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_1NW")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf(check_string) = -1 Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function

    Protected Function post_market(show_result As Boolean, first_pass As Boolean) As Boolean

        'https://restapilite.safesoftsolutions.com/leads/insert?
        'phone_number=4242564037&auth_token=cf7a69dcb3ebd0a67cca6706e50127b5b54455b7&list_id=26701
        '&form_data[field_29723]=lawanda&form_data[field_29725]=wells&form_data[field_29727]=332w Regentst.4
        '&form_data[field_29731]=Inglewood&form_data[field_29733]=CA&form_data[field_29735]=90301&form_data[field_29737]=4242564030
        '&form_data[field_29743]=lawandawellsluvconner26@gmail.com&form_data[field_30547]=CEOcheck_dup=1&hopper=1
        '&form_data[field_30543]=98.126.131.89&form_data[field_30545]=08/7/2012 13:59

        Dim this_uri As Uri
        this_uri = New Uri("https://restapilite.safesoftsolutions.com/leads/insert")

        Dim data As String

        data = "&auth_token=cf7a69dcb3ebd0a67cca6706e50127b5b54455b7&list_id=26701"
        data &= "&form_data[field_29723]=" & txtFirstName.Value
        data &= "&form_data[field_29725]=" & txtLastName.Value
        data &= "&form_data[field_29743]=" & txtEmail.Value
        data &= "&phone_number=" & txtPrimaryPhone.Value
        data &= "&form_data[field_29737]=" & txtPrimaryPhone.Value
        data &= "&form_data[field_29735]=" & txtZip.Value
        data &= "&form_data[field_29727]=" & txtAddress.Value
        data &= "&form_data[field_29731]=" & txtCity.Value
        data &= "&form_data[field_29733]=" & txtState.Value
        data &= "&form_data[field_30543]=" & txtIP.Value
        data &= "&form_data[field_30545]=" & Format(Now, "yyyy-mm-dd hh:mm_ss")

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)

        If tmp.IndexOf("success") = -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_market")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("success") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function

    Protected Function post_edsoup_mob(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://www.edsoup.com/dhein/transfer.php")

        Dim data As String

        data = "key=130e377e7808b4d36ee5c2d570d66b4c&source_id=209"
        data &= "&campaign_name=" & txtSourceID.Value
        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&number1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&number2="
        data &= "&street=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&url=" & txtURL.Value
        data &= "&ip_address=" & txtIP.Value
        data &= "&program=" & txtInterest.Value
        data &= "&gradyear=" & txtGraduated.Value
        data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")
        data &= "&referringsite=FinishSchoolOnline.com"
        data &= "&leadid=" & txtLeadID.Value

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If Left(tmp, 4) = "fail" Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_edsoup_mob")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        reply.Parameters.AddWithValue("lead_id", txtLeadID.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If Left(tmp, 4) = "fail" Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function


    Protected Function post_PerfectPitch(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://www.edsoup.com/dhein/transfer.php")

        Dim data As String

        data = "key=130e377e7808b4d36ee5c2d570d66b4c&source_id=221"
        data &= "&campaign_name=" & txtSourceID.Value
        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&number1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&number2="
        data &= "&street=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&url=" & txtSourceID.Value
        data &= "&ip_address=" & txtIP.Value
        data &= "&program=" & txtInterest.Value
        data &= "&gradyear=" & txtGraduated.Value
        data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")
        data &= "&referringsite=123freetravel.com/HBB/"

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If Left(tmp, 4) = "fail" Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_PerfectPitch")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If Left(tmp, 4) = "fail" Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function


    Protected Function post_OBWT(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://www.edsoup.com/dhein/transfer.php")

        Dim data As String

        data = "key=130e377e7808b4d36ee5c2d570d66b4c&source_id=226"
        data &= "&campaign_name=" & txtSourceID.Value
        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&number1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&number2="
        data &= "&street=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&url=" & txtSourceID.Value
        data &= "&ip_address=" & txtIP.Value
        data &= "&program=" & txtInterest.Value
        data &= "&gradyear=" & txtGraduated.Value
        data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")
        data &= "&referringsite=123freetravel.com/HBB/"

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"

        Try
            Dim writer As New StreamWriter(web_request.GetRequestStream)
            writer.Write(data)
            writer.Close()

        Catch ex As Exception
            Response.Write(ex.Message & "<br>")
            Response.Write(this_uri.AbsoluteUri & "?" & data)
            Return True
            'Response.End()
        End Try


        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If Left(tmp, 4) = "fail" Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_OBWT")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()
        cn.Dispose()

        If Left(tmp, 4) = "fail" Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function




    Protected Function post_ed_soup(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://www.edsoup.com/dhein/transfer.php")

        Dim data As String

        data = "key=130e377e7808b4d36ee5c2d570d66b4c&source_id=196"
        data &= "&campaign_name=" & txtSourceID.Value
        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&number1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&number2="
        data &= "&street=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&url=" & txtSourceID.Value
        data &= "&ip_address=" & txtIP.Value
        data &= "&program=" & txtInterest.Value
        data &= "&gradyear=" & txtGraduated.Value
        data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")
        data &= "&referringsite=123freetravel.com/HBB/"

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If Left(tmp, 4) = "fail" Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_ed_soup")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If Left(tmp, 4) = "fail" Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function
    Protected Sub Repost_Live()
        Dim this_uri As Uri

        this_uri = New Uri("http://live.estomes.com")



        Dim data As String = "vendor_id=2&sub_id=LR_PNIL"

        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&primary_phone=" & txtPrimaryPhone.Value
        data &= "&citizen=" & txtCitizen.Value
        data &= "&field_of_interest=" & txtInterest.Value
        data &= "&has_hs_or_ged=" & txtGraduated.Value
        ' data &= "&source_id=" & Request("source_id").ToString


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()

    End Sub


    Protected Function post_h2h(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://h2htrk.com/d.ashx")

        Dim data As String

        data = "ckm_campaign_id=10266&ckm_key=Xbwvw2zHDvg"
        data &= "&ckm_subid=" & txtSourceID.Value
        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&email_address=" & txtEmail.Value
        data &= "&phone_home=" & txtPrimaryPhone.Value

        'data &= "&zip=" & txtZip.value
        'data &= "&dob=&number2="
        'data &= "&street=" & txtAddress.value
        'data &= "&city=" & txtCity.value
        'data &= "&state=" & txtState.value
        'data &= "&url=" & txtSourceID.value
        'data &= "&ip_address=" & txtIP.value
        'data &= "&program=" & txtInterest.value
        'data &= "&gradyear=" & txtGraduated.value
        'data &= "&regdate=" & Format(Now, "yyyy-mm-dd hh:mm_ss")
        'data &= "&referringsite=123freetravel.com/HBB/"

        ' data &= "&testlead=1"



        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        'If Left(tmp, 4) = "fail" Then
        '    Repost_Live()

        'End If

        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If


        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_h2h")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If


    End Function



    Protected Function post_SMS_A(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://cc.specialeads.com/data_inflow/external_data_loader.php")
        data = "affiliate_id=106&username=estomes&sub=A"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&address1=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&zip=" & txtZip.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&gradyear=" & txtGraduated.Value
        data &= "&origin=" & txtURL.Value
        data &= "&requestdate=" & Today.ToShortDateString

        data &= "&ip=" & txtIP.Value
        data &= "&optin=" & txtOptInDate.Value


        ' data &= "&test=Y"

        'data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="


        'data &= "&gender="


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_SMS_A")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function post_SMS_B(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://cc.specialeads.com/data_inflow/external_data_loader.php")
        data = "affiliate_id=106&username=estomes&sub=HFAF"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&address1=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&zip=" & txtZip.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&gradyear=" & txtGraduated.Value
        data &= "&origin=" & txtURL.Value
        data &= "&requestdate=" & Today.ToShortDateString

        data &= "&ip=" & txtIP.Value
        data &= "&optin=" & txtOptInDate.Value


        'data &= "&test=Y"

        'data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="


        'data &= "&gender="


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_SMS_B")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function




    Protected Function post_leadresell_PR(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=34290"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&gender=&ip=" & txtIP.Value
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell_PR")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function


    Protected Function Post_ES(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://www.edutracking.net/education_source_live_post.php")
        data = "Branch=CENTRAL&CampaignId=116037&DataSource=Stomel Media"
        data &= "&FirstName=" & txtFirstName.Value
        data &= "&LastName=" & txtLastName.Value
        data &= "&Email=" & txtEmail.Value
        data &= "&Phone=" & txtPrimaryPhone.Value
        data &= "&Zip=" & txtZip.Value
        data &= "&Address1=" & txtAddress.Value
        data &= "&City=" & txtCity.Value
        data &= "&State=" & txtState.Value
        data &= "&IP=" & txtIP.Value
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_ES")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function


    Protected Function Post_leadresellHFAF(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=35044"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&gender=&ip=" & txtIP.Value
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_leadresellHFAF")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function




    Protected Function Post_OAM(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://services.higheredgrowth.com/oadata/record")
        data = "SourceId=10240-1" '&CampaignId=eStomes
        data &= "&SubId=" & txtSourceID.Value
        data &= "&FirstName=" & txtFirstName.Value
        data &= "&LastName=" & txtLastName.Value
        data &= "&EmailAddress=" & txtEmail.Value
        data &= "&PhoneNumber=" & txtPrimaryPhone.Value
        data &= "&ZipCode=" & txtZip.Value
        data &= "&StreetAddress=" & txtAddress.Value
        data &= "&IPv4Address=" & txtIP.Value
        data &= "&CampaignId=" & txtURL.Value
        data &= "&IsTest=false"
        data &= "&CallingOnBehalf=" & txtURL.Value
        data &= "&TcpaConsent=true"
        data &= "&TcpaDisclosure=By checking ""yes"", I consent to be contacted regarding educational opportunities at the phone number provided, including mobile number, using an automated telephone dialing system. I may be contacted by Education Ahead, online-edu-help.com, Education First or 1 of the following partners regardless of being on a federal or state do not call list. This is a separate optional offer for education. Consent is not required as a condition of using this service."


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        'Response.Write(this_uri.AbsoluteUri & "?" & data)

        Dim tmp As String
        Try
            Dim web_response As HttpWebResponse = web_request.GetResponse()
            Dim reader As New StreamReader(web_response.GetResponseStream())
            tmp = reader.ReadToEnd()
            web_response.Close()
        Catch ex As WebException
            tmp = ex.Response.Headers.ToString
        End Try






        If show_result Then
            'Response.Write(tmp)
            'Response.End()

        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf(Chr(34) & "IsValid" & Chr(34) & ":true") = -1 Then 'Fail" Then
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_OAM")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf(Chr(34) & "IsValid" & Chr(34) & ":true") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function


    Protected Function Post_OAM2(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://services.higheredgrowth.com/oadata/record")
        data = "SourceId=10240-2" '&CampaignId=eStomes
        data &= "&SubId=" & txtSourceID.Value
        data &= "&FirstName=" & txtFirstName.Value
        data &= "&LastName=" & txtLastName.Value
        data &= "&EmailAddress=" & txtEmail.Value
        data &= "&PhoneNumber=" & txtPrimaryPhone.Value
        data &= "&ZipCode=" & txtZip.Value
        data &= "&StreetAddress=" & txtAddress.Value
        data &= "&IPv4Address=" & txtIP.Value
        data &= "&CampaignId=" & txtURL.Value
        data &= "&IsTest=false"
        data &= "&CallingOnBehalf=" & txtURL.Value
        data &= "&TcpaConsent=true"
        data &= "&TcpaDisclosure=By checking ""yes"", I consent to be contacted regarding educational opportunities at the phone number provided, including mobile number, using an automated telephone dialing system. I may be contacted by Education Ahead, online-edu-help.com, Education First or 1 of the following partners regardless of being on a federal or state do not call list. This is a separate optional offer for education. Consent is not required as a condition of using this service."


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        'Response.Write(this_uri.AbsoluteUri & "?" & data)

        Dim tmp As String
        Try
            Dim web_response As HttpWebResponse = web_request.GetResponse()
            Dim reader As New StreamReader(web_response.GetResponseStream())
            tmp = reader.ReadToEnd()
            web_response.Close()
        Catch ex As WebException
            tmp = ex.Response.Headers.ToString
        End Try






        If show_result Then
            'Response.Write(tmp)
            'Response.End()

        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf(Chr(34) & "IsValid" & Chr(34) & ":true") = -1 Then 'Fail" Then
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_OAM2")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf(Chr(34) & "IsValid" & Chr(34) & ":true") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function Post_database(show_result As Boolean, first_pass As Boolean) As Boolean

        Dim data As String = ""




        Dim tmp As String = ""


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("show_result", show_result)
        reply.Parameters.AddWithValue("post_result", "SUCCESS")

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", "N/A")
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_database")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf(Chr(34) & "IsValid" & Chr(34) & ":true") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function Post_Email(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri

        Dim data As String

        'http://submission.lvlivefeed2.com/c2c_dpost.php?from=<email>&first_name=<firstname>&last_name=<lastname>&zip=<zip>&ipaddress=<ipaddress>&sitename=<site>&listname=<listname>

        this_uri = New Uri("http://submission.lvlivefeed2.com/c2c_dpost.php")
        data = "listname=estomes" '&CampaignId=eStomes
        data &= "&from=" & txtEmail.Value
        data &= "&first_name=" & txtFirstName.Value
        data &= "&last_name=" & txtLastName.Value
        data &= "&zip=" & txtZip.Value
        data &= "&ipaddress=" & txtIP.Value
        data &= "&sitename=" & txtURL.Value


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        'Response.Write(this_uri.AbsoluteUri & "?" & data)

        Dim tmp As String
        Try
            Dim web_response As HttpWebResponse = web_request.GetResponse()
            Dim reader As New StreamReader(web_response.GetResponseStream())
            tmp = reader.ReadToEnd()
            web_response.Close()
        Catch ex As WebException
            tmp = ex.Response.Headers.ToString
        End Try






        If show_result Then
            'Response.Write(tmp)
            'Response.End()

        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)


        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf(Chr(34) & "IsValid" & Chr(34) & ":true") = -1 Then 'Fail" Then
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_Email")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf(Chr(34) & "IsValid" & Chr(34) & ":true") = -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function



    Protected Function Post_leadresell_SPHN(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=35692"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&gender=&ip=" & txtIP.Value
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_leadresell_SPHN")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function


    Protected Function Post_leadresell_MAU(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=35746"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&gender=&ip=" & txtIP.Value
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_leadresell_MAU")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function
    Protected Function Post_leadresell_FSA(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=35800"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&gender=&ip=" & txtIP.Value
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_leadresell_FSA")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function
    Protected Function Post_leadresell_CWU(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=35854"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&gender=&ip=" & txtIP.Value
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_leadresell_CWU")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function






    Protected Function post_USN_FFR(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://www.usnewsuniversitydirectory.com/services/leads/vendor.svc/submitlead")
        data = "MCID=54886&campaignid=9991200&VendorID=15&nurture=1&v_ismobile=Y&disclosureName=USNUC-eStomes-FindFamilyResources&consentlanguage=By checking ""yes"", I consent to be contacted regarding educational and other opportunities at the phone number provided, including mobile number, using an automated telephone dialing system. I may be contacted by Education Ahead, online-edu-help.com, U.S. News University Connection, LLC or 1 of the following partners regardless of being on a federal or state do not call list. This is a separate optional offer for education. Consent is not required as a condition of using this service."
        data &= "&fname=" & txtFirstName.value
        data &= "&lname=" & txtLastName.value
        data &= "&email=" & txtEmail.value
        data &= "&phone=" & txtPrimaryPhone.value
        data &= "&zip=" & txtZip.value
        data &= "&state=" & txtState.value
        data &= "&optin=" & Today.ToShortDateString
        data &= "&postdate=" & Today.ToShortDateString
        data &= "&ip=" & txtIP.value
        data &= "&consenturl=" & txtURL.value
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.value)
        reply.Parameters.AddWithValue("last_name", txtLastName.value)
        reply.Parameters.AddWithValue("email", txtEmail.value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.value)

        reply.Parameters.AddWithValue("address", txtAddress.value)
        reply.Parameters.AddWithValue("city", txtCity.value)
        reply.Parameters.AddWithValue("state", txtState.value)
        reply.Parameters.AddWithValue("zip", txtZip.value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("failure") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_USN_FFR")
        reply.Parameters.AddWithValue("url", txtURL.value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("failure") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function



    Protected Function post_USN_FHN(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://www.usnewsuniversitydirectory.com/services/leads/vendor.svc/submitlead")
        data = "MCID=54887&campaignid=9991210&VendorID=15&nurture=1&v_ismobile=Y&disclosureName=USNUC-eStomes-FindFamilyResources&consentlanguage=By checking ""yes"", I consent to be contacted regarding educational and other opportunities at the phone number provided, including mobile number, using an automated telephone dialing system. I may be contacted by Education Ahead, online-edu-help.com, U.S. News University Connection, LLC or 1 of the following partners regardless of being on a federal or state do not call list. This is a separate optional offer for education. Consent is not required as a condition of using this service."
        data &= "&fname=" & txtFirstName.value
        data &= "&lname=" & txtLastName.value
        data &= "&email=" & txtEmail.value
        data &= "&phone=" & txtPrimaryPhone.value
        data &= "&zip=" & txtZip.value
        data &= "&state=" & txtState.value
        data &= "&optin=" & Today.ToShortDateString
        data &= "&postdate=" & Today.ToShortDateString
        data &= "&ip=" & txtIP.value
        data &= "&consenturl=" & txtURL.value
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.value)
        reply.Parameters.AddWithValue("last_name", txtLastName.value)
        reply.Parameters.AddWithValue("email", txtEmail.value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.value)

        reply.Parameters.AddWithValue("address", txtAddress.value)
        reply.Parameters.AddWithValue("city", txtCity.value)
        reply.Parameters.AddWithValue("state", txtState.value)
        reply.Parameters.AddWithValue("zip", txtZip.value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("failure") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "post_USN_FFR")
        reply.Parameters.AddWithValue("url", txtURL.value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("failure") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function





    Protected Function Post_leadresell_YCA(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=36086"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&zip=" & txtZip.Value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&gender=&ip=" & txtIP.Value
        'data &= "&test=1"

        Dim tmp As String

        Try
            Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
            web_request.Method = WebRequestMethods.Http.Post
            web_request.ContentLength = data.Length
            web_request.ContentType = "application/x-www-form-urlencoded"
            Dim writer As New StreamWriter(web_request.GetRequestStream)
            writer.Write(data)
            writer.Close()

            Dim web_response As HttpWebResponse = web_request.GetResponse()
            Dim reader As New StreamReader(web_response.GetResponseStream())
            tmp = reader.ReadToEnd()
            web_response.Close()
            If show_result Then
                ' Response.Write(tmp)
            End If
        Catch ex As Exception
            tmp = ex.Message
        End Try




        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_leadresell_YCA")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function



    Protected Function Post_leadsresell_YCAR(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=36122"
        data &= "&fname=" & txtFirstName.value
        data &= "&lname=" & txtLastName.value
        data &= "&email=" & txtEmail.value
        data &= "&phone1=" & txtPrimaryPhone.value
        data &= "&zip=" & txtZip.value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.value
        data &= "&city=" & txtCity.value
        data &= "&state=" & txtState.value
        data &= "&gender=&ip=" & txtIP.value
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.value)
        reply.Parameters.AddWithValue("last_name", txtLastName.value)
        reply.Parameters.AddWithValue("email", txtEmail.value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.value)

        reply.Parameters.AddWithValue("address", txtAddress.value)
        reply.Parameters.AddWithValue("city", txtCity.value)
        reply.Parameters.AddWithValue("state", txtState.value)
        reply.Parameters.AddWithValue("zip", txtZip.value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_leadsresell_YCAR")
        reply.Parameters.AddWithValue("url", txtURL.value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function



    Protected Function Post_leadsresell_LJSR(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=36098"
        data &= "&fname=" & txtFirstName.value
        data &= "&lname=" & txtLastName.value
        data &= "&email=" & txtEmail.value
        data &= "&phone1=" & txtPrimaryPhone.value
        data &= "&zip=" & txtZip.value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.value
        data &= "&city=" & txtCity.value
        data &= "&state=" & txtState.value
        data &= "&gender=&ip=" & txtIP.value
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.value)
        reply.Parameters.AddWithValue("last_name", txtLastName.value)
        reply.Parameters.AddWithValue("email", txtEmail.value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.value)

        reply.Parameters.AddWithValue("address", txtAddress.value)
        reply.Parameters.AddWithValue("city", txtCity.value)
        reply.Parameters.AddWithValue("state", txtState.value)
        reply.Parameters.AddWithValue("zip", txtZip.value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_leadsresell_LJSR")
        reply.Parameters.AddWithValue("url", txtURL.value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function Post_leadsresell_EDS2(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://api.leadsresell.com/post_leads.aspx")
        data = "post_act=import&apikey=6c5fdc96dd8847b0be05d96525108de9&dsID=36110"
        data &= "&fname=" & txtFirstName.value
        data &= "&lname=" & txtLastName.value
        data &= "&email=" & txtEmail.value
        data &= "&phone1=" & txtPrimaryPhone.value
        data &= "&zip=" & txtZip.value
        data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="
        data &= "&address=" & txtAddress.value
        data &= "&city=" & txtCity.value
        data &= "&state=" & txtState.value
        data &= "&gender=&ip=" & txtIP.value
        'data &= "&test=1"

        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.value)
        reply.Parameters.AddWithValue("last_name", txtLastName.value)
        reply.Parameters.AddWithValue("email", txtEmail.value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.value)

        reply.Parameters.AddWithValue("address", txtAddress.value)
        reply.Parameters.AddWithValue("city", txtCity.value)
        reply.Parameters.AddWithValue("state", txtState.value)
        reply.Parameters.AddWithValue("zip", txtZip.value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Error") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_leadsresell_EDS2")
        reply.Parameters.AddWithValue("url", txtURL.value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Error") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function






    Protected Function post_SMS_MIB(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://cc.specialeads.com/data_inflow/external_data_loader.php")
        data = "affiliate_id=106&username=estomes&sub=MIB"
        data &= "&fname=" & txtFirstName.value
        data &= "&lname=" & txtLastName.value
        data &= "&address1=" & txtAddress.value
        data &= "&city=" & txtCity.value
        data &= "&state=" & txtState.value
        data &= "&zip=" & txtZip.value
        data &= "&email=" & txtEmail.value
        data &= "&phone1=" & txtPrimaryPhone.value
        data &= "&gradyear=" & txtGraduated.value
        data &= "&origin=" & txtURL.value
        data &= "&requestdate=" & Today.ToShortDateString

        data &= "&ip=" & txtIP.value
        data &= "&optin=" & txtOptInDate.value

        'data &= "&test=Y"

        'data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="


        'data &= "&gender="


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.value)
        reply.Parameters.AddWithValue("last_name", txtLastName.value)
        reply.Parameters.AddWithValue("email", txtEmail.value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.value)

        reply.Parameters.AddWithValue("address", txtAddress.value)
        reply.Parameters.AddWithValue("city", txtCity.value)
        reply.Parameters.AddWithValue("state", txtState.value)
        reply.Parameters.AddWithValue("zip", txtZip.value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_SMS_MIB")
        reply.Parameters.AddWithValue("url", txtURL.value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function


    Protected Function post_SMS_HIM(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://cc.specialeads.com/data_inflow/external_data_loader.php")
        data = "affiliate_id=106&username=estomes&sub=HIM"
        data &= "&fname=" & txtFirstName.value
        data &= "&lname=" & txtLastName.value
        data &= "&address1=" & txtAddress.value
        data &= "&city=" & txtCity.value
        data &= "&state=" & txtState.value
        data &= "&zip=" & txtZip.value
        data &= "&email=" & txtEmail.value
        data &= "&phone1=" & txtPrimaryPhone.value
        data &= "&gradyear=" & txtGraduated.value
        data &= "&origin=" & txtURL.value
        data &= "&requestdate=" & Today.ToShortDateString

        data &= "&ip=" & txtIP.value
        data &= "&optin=" & txtOptInDate.value

        'data &= "&test=Y"

        'data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="


        'data &= "&gender="


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.value)
        reply.Parameters.AddWithValue("last_name", txtLastName.value)
        reply.Parameters.AddWithValue("email", txtEmail.value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.value)

        reply.Parameters.AddWithValue("address", txtAddress.value)
        reply.Parameters.AddWithValue("city", txtCity.value)
        reply.Parameters.AddWithValue("state", txtState.value)
        reply.Parameters.AddWithValue("zip", txtZip.value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_SMS_HIM")
        reply.Parameters.AddWithValue("url", txtURL.value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function




    Protected Function post_SMS_FHN(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://cc.specialeads.com/data_inflow/external_data_loader.php")
        data = "affiliate_id=106&username=estomes&sub=FHN"
        data &= "&fname=" & txtFirstName.value
        data &= "&lname=" & txtLastName.value
        data &= "&address1=" & txtAddress.value
        data &= "&city=" & txtCity.value
        data &= "&state=" & txtState.value
        data &= "&zip=" & txtZip.value
        data &= "&email=" & txtEmail.value
        data &= "&phone1=" & txtPrimaryPhone.value
        data &= "&gradyear=" & txtGraduated.value
        data &= "&origin=" & txtURL.value
        data &= "&requestdate=" & Today.ToShortDateString

        data &= "&ip=" & txtIP.value
        data &= "&optin=" & txtOptInDate.value

        'data &= "&test=Y"

        'data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="


        'data &= "&gender="


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.value)
        reply.Parameters.AddWithValue("last_name", txtLastName.value)
        reply.Parameters.AddWithValue("email", txtEmail.value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.value)

        reply.Parameters.AddWithValue("address", txtAddress.value)
        reply.Parameters.AddWithValue("city", txtCity.value)
        reply.Parameters.AddWithValue("state", txtState.value)
        reply.Parameters.AddWithValue("zip", txtZip.value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_SMS_FHN")
        reply.Parameters.AddWithValue("url", txtURL.value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function





    Protected Function post_SMS_UFR(show_result As Boolean, first_pass As Boolean) As Boolean
        Dim this_uri As Uri
        this_uri = New Uri("http://Posting.leadringers.com/incoming.aspx")

        Dim data As String

        'http://Posting.leadringers.com/incoming.aspx
        'list_id=eST_BMF&first_name=&last_name=&email=&number1=&number2=&number3=&street=&city=&state=&zip=&ip=&datetime=


        this_uri = New Uri("http://cc.specialeads.com/data_inflow/external_data_loader.php")
        data = "affiliate_id=106&username=estomes&sub=UFR"
        data &= "&fname=" & txtFirstName.Value
        data &= "&lname=" & txtLastName.Value
        data &= "&address1=" & txtAddress.Value
        data &= "&city=" & txtCity.Value
        data &= "&state=" & txtState.Value
        data &= "&zip=" & txtZip.Value
        data &= "&email=" & txtEmail.Value
        data &= "&phone1=" & txtPrimaryPhone.Value
        data &= "&gradyear=" & txtGraduated.Value
        data &= "&origin=" & txtURL.Value
        data &= "&requestdate=" & Today.ToShortDateString

        data &= "&ip=" & txtIP.Value
        data &= "&optin=" & txtOptInDate.Value

        'data &= "&test=Y"

        'data &= "&dob=&phone2=&program=&edulevel=&gradyear=&student=&url=&regdate=&timezone=&remoteid=sub=&is18="


        'data &= "&gender="


        Dim web_request As HttpWebRequest = HttpWebRequest.Create(this_uri)
        web_request.Method = WebRequestMethods.Http.Post
        web_request.ContentLength = data.Length
        web_request.ContentType = "application/x-www-form-urlencoded"
        Dim writer As New StreamWriter(web_request.GetRequestStream)
        writer.Write(data)
        writer.Close()

        Dim web_response As HttpWebResponse = web_request.GetResponse()
        Dim reader As New StreamReader(web_response.GetResponseStream())
        Dim tmp As String = reader.ReadToEnd()
        web_response.Close()
        If show_result Then
            ' Response.Write(tmp)
        End If


        Dim cn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MFPConnectionString").ConnectionString)
        cn.Open()
        Dim reply As New SqlCommand("AddMFP", cn)
        reply.CommandType = System.Data.CommandType.StoredProcedure
        reply.Parameters.AddWithValue("first_name", txtFirstName.Value)
        reply.Parameters.AddWithValue("last_name", txtLastName.Value)
        reply.Parameters.AddWithValue("email", txtEmail.Value)
        reply.Parameters.AddWithValue("primary_phone", txtPrimaryPhone.Value)
        reply.Parameters.AddWithValue("citizen", txtCitizen.Value)
        reply.Parameters.AddWithValue("field_of_interest", txtInterest.Value)
        reply.Parameters.AddWithValue("has_hs_or_ged", txtGraduated.Value)

        reply.Parameters.AddWithValue("address", txtAddress.Value)
        reply.Parameters.AddWithValue("city", txtCity.Value)
        reply.Parameters.AddWithValue("state", txtState.Value)
        reply.Parameters.AddWithValue("zip", txtZip.Value)

        reply.Parameters.AddWithValue("optindate", txtOptInDate.Value)
        reply.Parameters.AddWithValue("optintime", txtOptInTime.Value)
        reply.Parameters.AddWithValue("ipaddress", txtIP.Value)

        reply.Parameters.AddWithValue("show_result", show_result)
        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            reply.Parameters.AddWithValue("post_result", "FAIL")
        Else
            reply.Parameters.AddWithValue("post_result", "SUCCESS")
        End If

        reply.Parameters.AddWithValue("sub_id", txtSourceID.Value)
        reply.Parameters.AddWithValue("submitted_data", data)
        reply.Parameters.AddWithValue("submitted_site", this_uri.AbsoluteUri)
        reply.Parameters.AddWithValue("result", tmp)
        reply.Parameters.AddWithValue("isDupe", isDupe)
        reply.Parameters.AddWithValue("first_pass", first_pass)
        reply.Parameters.AddWithValue("post_function", "Post_SMS_UFR")
        reply.Parameters.AddWithValue("url", txtURL.Value)
        If Request("vendor_id") IsNot Nothing Then
            reply.Parameters.AddWithValue("vendor_id", Request("vendor_id"))
        Else
            reply.Parameters.AddWithValue("vendor_id", "0")
        End If
        reply.ExecuteNonQuery()
        cn.Close()

        If tmp.IndexOf("Rejected") > -1 Then 'Fail
            Return False
        Else
            Return True
        End If

    End Function




    Protected Sub Page_PreRenderComplete(sender As Object, e As System.EventArgs) Handles Me.PreRenderComplete

    End Sub
End Class
